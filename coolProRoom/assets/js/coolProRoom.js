/**

 * Created by grego on 13/06/2017.

 */

var tipoCamara=0;
var tipoProducto=0;
var idProducto=0;
var idProyecto=0;
var potencia=0;
var modific=false;
var guardado= false;
var cond_cam_aire=0.75;
var espe_cam_aire=150;
var temp_ext=$("#tempExt").val();
var altitud= $("#altitud").val();
var hum_ext= $("#humExt").val();
var elementChange;
var datosModificados= {};
var noMostrar= false;

$(document).ready(function() {
    $.fn.wizard.logging = true;
    var wizard = $('#satellite-wizard').wizard({
        keyboard : false,
        contentHeight : 500,
        contentWidth : 1300,
        backdrop: 'static'
    });
    console.log(wizard);

    wizard.on('closed', function() {
        //wizard.reset();
    });

    wizard.on("reset", function() {
        wizard.modal.find(':input').val('').removeAttr('disabled');
        wizard.modal.find('.form-group').removeClass('has-error').removeClass('has-succes');
    });

    wizard.on("submit", function(wizard) {
        var submit = {
            "hostname": $("#new-server-fqdn").val()
        };

        this.Math.log('seralize()');

        this.Math.log(this.serialize());

        this.Math.log('serializeArray()');

        this.Math.log(this.serializeArray());

        setTimeout(function() {

            wizard.trigger("success");

            wizard.hideButtons();

            wizard._submitting = true;

            wizard.showSubmitCard("success");

            wizard.updateProgressBar(0);

        }, 2000);
    });

    wizard.el.find(".wizard-success .im-done").click(function() {
        wizard.hide();

        setTimeout(function() {
            wizard.reset();
        }, 250);
    });

    $(".wizard-next").click(function(){
        if($(".wizard-next").attr("class").includes("btn-success")){
            $(".progress-bar").css("width","100%");
        }
    })

    $('#open-wizard').click(function(e) {
        e.preventDefault();

        modific=true;

        wizard.show();

        //wizard.reset();

        $(".guardar").hide();

        $(".calcular").hide();

        $(".label-info").hide();
    });


    //Cambia datos de provincia
    $("#idProvincia").change(function() {
        option = $("#idProvincia option:selected").val();
        if(option==0){
            $("#tempExt").val("");
            $("#humExt").val("");
            $("#altitud").val("");
            $("#velViento").val("");
            $("#tempSuelo").val("");
        }
        else{
            //rellenarDatosProvincias();
        }
    });

    //Cambiar valor de input hidden al cambiar el select
    $('.wizard-dialog').on('change', '[change-hidden]', function(){
        changeHidden($(this));
    });

    //Controla el evento click en los tipos de cámara
    $(".idCamara").click(function(){
        var elementCamara= $(this);
        if($(this).hasClass( "noDisponible")){
            alertify.warning( '¡Próximamente disponible!');
        }
        else{
            if($(".idCamara.seleccionada").attr("id")){
                $.confirm({
                    icon: 'fa fa-warning',
                    title: 'Cambiar de tipo de cámara',
                    content: '<div class="content-pane" style="transition-duration: 0.5s; transition-timing-function: cubic-bezier(0.36, 1.1, 0.2, 1); height: 20px;"><div class="content" style="clip: rect(0px 430px 40px -100px);">Se perderán todos los cambios realizados en los parámetros de cálculo.</div></div>',
                    confirmButton: 'Ok',
                    cancelButton: 'Cancelar',
                    confirm: function () {
                        datosDefecto();
                        datosDefectoCamara(elementCamara);
                    },
                    cancel: function () {
                    }
                });
            }
            else{
                datosDefecto();
                datosDefectoCamara(elementCamara);
            }
        }
    });

    $("[name=options_secadero]").change(function(){
        tipoCamara= $(this).val();
        $("[name=aislamiento][value='1']").prop('checked', true);
        visibilityFields("aislamiento");
        if(tipoCamara == 8 || tipoCamara == 9 || tipoCamara == 10){
            $("[name=aislamiento][value='2']").prop('checked', true);
            $('.active-aislamiento').addClass('oculto');
        }
    });

    $("#longCam").change(function(){
        calcularVolumen();
    });

    $("#anchCam").change(function(){
        calcularVolumen();
    });

    $("#altCam").change(function(){
        calcularVolumen();
    });

    $("#voluCam").change(function(){
        comprobarMaximoMinimo("volumen_cam");
        if($("#voluCam").val()<=120){
            $("#altCam").val("3");
        }
        else if($("#voluCam").val()<=400)
            $("#altCam").val("4");
        else if($("#voluCam").val()<=1000)
            $("#altCam").val("5");
        else if($("#voluCam").val()<=5000)
            $("#altCam").val("6");
        else
            $("#altCam").val(Math.pow($("#voluCam").val(),(1/3)));

        $("#longCam").val(Math.round(Math.sqrt($("#voluCam").val()/$("#altCam").val())*100)/100);
        $("#anchCam").val(Math.round(Math.sqrt($("#voluCam").val()/$("#altCam").val())*100)/100);
        datosModificados["capacidad_cam"]= 0;
        datosModificados["rotacion"]= 0;
        datosModificados["carga_diaria"]= 0;
        datosModificados["renov_aire_calc"]= 0;
        datosModificados["renov_infiltra"]= 0;
        $(".rotacion_tipo").show();
        sendData("volumen_cam");
    });

    $('.datepicker').datepicker();

    $(".categoriaProducto").click(function(){
        $(".categoriaProducto").removeClass("seleccionada");

        $(this).addClass("seleccionada");

        id=$(this).attr("id");

        tipoProducto=$(this).children('input[name=categoriaProducto]').val();
        //console.log('tipoProducto');
        //console.log(tipoProducto);

        $(this).children('input[type=radio]').prop('checked', true);

        rellenarProductos(tipoProducto);
        $('.seleccionarTipoProducto').hide();

        elementChange = $(this).children('input').attr("name");
    })

    $('body').on('click', '.close-modal-product', function() {
        if(wizard.getActiveCard().validate()){
            $('#modalProducto').modal('hide');
        }
    })

    $('body').on('click', '.close-modal-cerramientos', function() {
        $('#modalCerramientos').modal('hide');
    })

    $("#capAlm").change(function(){
        comprobarMaximoMinimo("capacidad_cam");
        calcularDensAlmac();
        datosModificados["rotacion"]= 0;
        datosModificados["carga_diaria"]= 0;
        $(".rotacion_tipo").show();
        sendData("capacidad_cam");
    });

    $("#densNetAlm").change(function(){
        datosModificados["capacidad_cam"]= 0;
        datosModificados["rotacion"]= 0;
        datosModificados["carga_diaria"]= 0;
        $(".rotacion_tipo").show();
    });

    $(document).on('change', 'input, select', function() {
        var id = $(this).attr('id');
        if($(this).is("select")){
            var elementChange = $(this).attr('id');
        }
        else{
            var elementChange = $(this).attr('name');
        }

        if(elementChange== "idCamara"){
            tipoCamara= $(this).val();
            $('#producto-lacteos').addClass('oculto');
            if(tipoCamara== 8 || tipoCamara== 10){
                $('#producto-lacteos').removeClass('oculto');
            }
            rellenarProductos(1);
        }
        else if(elementChange != "nombre_proyecto" && elementChange != "fecha_proyecto" && elementChange != "nombre_proyectista" && elementChange != "nombre_cliente" && elementChange != "observ_proyecto" && elementChange != "volumen_cam" && elementChange != "capacidad_cam" && elementChange != "carga_diaria"){
            sendData(elementChange);
        }
    });

    $("[name=forma], [name=tipo_ubicacion], [name=cerr_verticales], [name=tipoNumPot], [name=tipoEvap], [name=potVentEst], [name=potResisEst], [name=potConEst], [name=tipo_cubierta], [name=tipo_cerr2], [name=tipo_cerr3], [name=tipo_cerr4]").change(function(){
        var id = $(this).attr('name');
        $("[name="+id+"]").each(function(){
            var id = $(this).attr('id');
            $('.active-'+id).addClass('oculto');
        })
        visibilityFields($(this).attr('id'));
    });

    $('body').on('change', '[name=cerr_ext], [name=ventanas], [name=aislamiento]', function() {
        var id = $(this).attr('name');
        $('.active-'+id).addClass('oculto');
        if ($(this).val() == 1) {
            visibilityFields(id);
        }

    });

    //Controla el evento click al pulsar en editar proyecto
    $(document).on( "click", ".editarProyect", function(){
        wizard.show();

        $(".guardar").show();
        $(".potenciaTotalHeader").show();
        //$(".progress-bar").css("width","100%");
        wizard.updateProgressBar(100);

        $(".calcular").show();

        $(".label-info").show();

        $(".wizard-nav-item").addClass("already-visited");

        idProyecto=$(this).parents("tr").attr("id");
        $('input[name="id_proyecto"]').val(idProyecto);

        $.ajax({
            url: "app/ajax/datosProyecto.php",

            type: "POST",

            data: {idProyecto: idProyecto},

            success: procesoDatosProyecto,

            error: errorAjax,
        });

        modific=false;
    });

    //Controla el evento click al pulsar en borrar proyecto
    $(document).on( "click", ".borrarProyect", function(){
        idProyecto=$(this).parents("tr").attr("id");
        $.confirm({
            title: 'Borrar proyecto',
            content: '<div class="content-pane" style="transition-duration: 0.5s; transition-timing-function: cubic-bezier(0.36, 1.1, 0.2, 1); height: 20px;"><div class="content" style="clip: rect(0px 430px 20px -100px);">¿Deséa borrar el proyecto?</div></div>',
            //animation: 'zoom',
            confirmButton: 'Si',
            confirm: function () {
                $.ajax({

                    url: "app/ajax/borrarProyecto.php",

                    type: "POST",

                    data: {id: idProyecto},

                    success: procesoBorrarProyecto,

                    error: errorAjax,
                });
            }
        });
    });

    //Controla el evento click al pulsar en guardar
    $(".guardar").click(function(){
        if(wizard.getActiveCard().validate()){
            if(idProyecto){
                editarProyecto(idProyecto);
            }
            else{
                guardarProyecto();
            }
        }
    });

    //Controla el evento click al pulsar en calcular
    $(".calcular").click(function(){
        potencia=(($("#voluCam").val()*$("#capAlm").val())/$("#densNetAlm").val())*40;

        $(".label-info").text("Pot. frigorífica: "+potencia+" W");

        datosResultados();
    });

    $(".close").click(function(){
        if(guardado || !wizard.getActiveCard().validate()){
            wizard.hide();
            location.reload();
        }
        else{
            $.confirm({
                title: 'Guardar proyecto',
                content: '<div class="content-pane" style="transition-duration: 0.5s; transition-timing-function: cubic-bezier(0.36, 1.1, 0.2, 1); height: 20px;"><div class="content" style="clip: rect(0px 430px 20px -100px);">¿Quiéres guardar el proyecto antes de salir?</div></div>',
                confirmButton: 'Si',
                confirm: function () {
                    wizard.hide();
                    if(idProyecto){
                        editarProyecto(idProyecto);
                    }
                    else{
                        guardarProyecto();
                    }
                    location.reload();
                },
                cancel: function () {
                    wizard.hide();
                    location.reload();
                },
                somethingElse: {
                    text: 'Something else',
                    btnClass: 'btn-blue',
                    keys: ['enter', 'shift'],
                    action: function(){
                        $.alert('Something else?');
                    }
                }
            });
        }
    });

    

    $("#renov_aire_adiccional").click(function(){
        $("[name=renov_aire_calc").val($("[name=renov_aire_adiccional").val())
        sendData("renov_aire_calc");
    });

    $("[name=rotacion").change(function(){
        comprobarRotacionTipo($(this).val());
        datosModificados["carga_diaria"]= 0;
    });

    $("[name=carga_diaria").change(function(){
        comprobarMaximoMinimo("carga_diaria");
        $("[name=rotacion").val(100*$("[name=carga_diaria").val()/$("[name=capacidad_cam").val());
        datosModificados["rotacion"]= 1;
        comprobarRotacionTipo($("[name=rotacion").val());
        sendData("carga_diaria");
    });

    $("input[type=number]").keyup(function() {
        //console.log($(this).attr("name"));
        if($(this).attr("name")== "temp_cam" || $(this).attr("name")== "temp_final"){
            if(tipoCamara== 1 || tipoCamara== 3 || tipoCamara== 5 || tipoCamara>= 6){
                $(this).attr("min", "0");
                $(this).attr("max", "20");
            }
            else{
                $(this).attr("min", "-60");
                $(this).attr("max", "0");
            }
        }
        else if($(this).attr("name")== "temp_inicial"){
            if(tipoCamara== 1 || tipoCamara== 3 || tipoCamara== 5 || tipoCamara>= 6){
                $(this).attr("min", "0");
                $(this).attr("max", "100");
            }
            else{
                $(this).attr("min", "-60");
                $(this).attr("max", "-10");
            }
        }
        else if($(this).attr("name")== "capacidad_cam"){
            if(tipoCamara== 1 || tipoCamara== 2 || tipoCamara== 3 || tipoCamara== 4 || tipoCamara>= 6){
                $(this).attr("min", "0");
                $(this).attr("max", "2000000");
            }
            else{
                $(this).attr("min", "0");
                $(this).attr("max", "500000");
            }
        }
        else if($(this).attr("name")== "carga_diaria"){
            $(this).attr("max", $("[name=capacidad_cam").val());
        }
        $(this).removeClass("error");
        if(parseFloat($(this).val())< $(this).attr("min") || parseFloat($(this).val())> $(this).attr("max") ){
            $(this).addClass("error");

            /*var max = parseFloat($(this).attr('max'));
            var min = parseFloat($(this).attr('min'))

            if (parseFloat($(this).val()) > max)
            {
                $(this).val(max);
            }
            else if (parseFloat($(this).val()) < min)
            {
                $(this).val(min);
            }*/
        }
    });

    $("input[type=number]").keypress(function(event) { 
        if (event.keyCode === 13) { 
            event.preventDefault();
            $(this).change();
        }
    });

    $(".wizard-nav-link").click(function(e){
        if(!wizard.getActiveCard().validate()){
            return false;
        }
    });

    $(".imprimir").click(function(){
        alertify.warning( 'Generando documento');
        $('.resultados').each(function() {
            id= $(this).attr("id");
            $("[name="+id).val(Number($("[name="+id).val()).toLocaleString());
        });

        var campos = $('input').serialize();
        $.ajax({

            url: "app/ajax/datosDocumento.php",

            type: "POST",

            data: {
                data: campos
            },

            success: procesoInformeResultados,

            error: errorAjax,
        });
    });

    $("input[type='number']").attr("title", "");
});

//Validacion
function validateField(el) {
    var name = el.val();
    var retValue = {};
    if (name == "") {
        retValue.status = false;
        retValue.msg = "Por favor introduce este dato";
    } else {
        retValue.status = true;
    }
    return retValue;
};

//Valida provincias
function validarProvincias(el) {
    var name = el.val();
    var retValue = {};
    if (name == "0") {
        retValue.status = false;
        retValue.msg = "Por favor introduce una provincia";
    } else {
        retValue.status = true;
    }
    return retValue;
};

camarSeleccionada=false;

//Valida tipo de cámara
function validarIdCamara(el){
    var name=false;
    $(".idCamara").each(function(ind, e) {
        name = $(e).attr("class").includes("seleccionada");
        if(name==true) {
            camarSeleccionada = true;
        }
    });

    var retValue = {};
    if (!camarSeleccionada) {
        retValue.status = false;
        retValue.msg = "Por favor introduce una provincia";
        $(".seleccionaCamar").show();
    } else {
        retValue.status = true;
        $(".seleccionaCamar").hide();
    }
    return retValue;
}

//Valida tipo de producto
function validarCategoriaProducto(el){
    camarSeleccionada=false;
    var name=false;
    $(".categoriaProducto").each(function(ind, e) {
        name = $(e).attr("class").includes("seleccionada");
        if(name==true) {
            camarSeleccionada = true;
        }
    });

    var retValue = {};
    if (!camarSeleccionada) {
        retValue.status = false;
        retValue.msg = "Por favor introduce una provincia";
        $(".seleccionarTipoProducto").show();
    } else {
        retValue.status = true;
        $(".seleccionarTipoProducto").hide();
    }
    return retValue;
}


function resultadosCalcular(el){
    datosResultados();
    if(modific && !guardado)
        guardarProyecto();
}

//Rellena datos de provincias
function rellenarDatosProvincias(){
    option = $("#idProvincia option:selected").val();
    $.ajax({
        url: "app/ajax/datosProvincia.php",

        type: "POST",

        data: {id: option},

        success: proceso,

        error: errorAjax,

    })
}

function proceso(datos) {
    $("#tempExt").val($(datos).find('temp_ext').text());

    $("#humExt").val($(datos).find('hum_ext').text());

    $("#altitud").val($(datos).find('altitud').text());

    $("#velViento").val($(datos).find('vel_viento').text());

    tempSuelo=(Number($(datos).find('temp_ext').text())+15)/2;

    $("#tempSuelo").val(tempSuelo.toFixed(2));
}

function rellenarProductos(id){
    //console.log('tipo camara rellenarProductos');
    //console.log(tipoCamara);

    $.ajax({
        url: "app/ajax/datosProductos.php",

        type: "POST",

        data: {idTipoProducto: id, idTipoCamara: tipoCamara},

        success: procesoProductos,

        error: errorAjax,
    })
}

function procesoProductos(datos){
    // console.log(datos + ' select tipo producto');
    $(".productos").html("");
     $("#modal-producto-wrapper").html("");

    productos="<p>Seleccione producto</p><select class='form-control' id='idProducto' change-hidden='productoValue'>";

    $(datos).find("dato").each(function(ind, e){
        if($(e).find("nombre").text().includes("variado") || $(e).find("nombre").text().includes("variadas")){
            productos=productos+"<option value='"+$(e).find("id").text()+"' selected>"+$(e).find("nombre").text()+"</option>";
        }

        else{
            productos=productos+"<option value='"+$(e).find("id").text()+"'>"+$(e).find("nombre").text()+"</option>";
        }
    });

    productos=productos+"</select>";

    $(".productos").append(productos);

    if(tipoCamara>=6 && tipoProducto==1){
        $("#idProducto").val(118);
    }

    var botonModal = '<a class="btn btn-info btn" data-toggle="modal" data-target="#modalProducto" data-backdrop="false">Editar características del producto</a>';
    $("#modal-producto-wrapper").append(botonModal);

    if(idProducto){
        $('#idProducto option[value='+idProducto+']').attr('selected', true);
        idProducto= 0;
    }

    changeHidden($('[change-hidden]'));
    sendData(elementChange);
}

function calcularVolumen(){
    /*$("#voluCam").val($("#longCam").val()*$("#anchCam").val()*$("#altCam").val());
    calcularCapacidadCam();*/
    datosModificados["volumen_cam"]= 0;
    datosModificados["capacidad_cam"]= 0;
    datosModificados["rotacion"]= 0;
    datosModificados["carga_diaria"]= 0;
    datosModificados["renov_aire_calc"]= 0;
    datosModificados["renov_infiltra"]= 0;
    $(".rotacion_tipo").show();
}

function calcularDensAlmac(){
    $("#densNetAlm").val($("#capAlm").val()/$("#voluCam").val());
}

function sendData(elementChange) {
    guardado= false;
    $("[name="+elementChange).parents(".form-group").removeClass('has-error');
    if($("[name="+elementChange).val()== ""){
        $("[name="+elementChange).parents(".form-group").addClass("has-error");
        $("#potenciaTotalHeader").text("--");
        alertify.error( "¡Error de cálculo! Comprueba los datos introducidos");
    }
    else{
        comprobarMaximoMinimo(elementChange);
        if(elementChange!= undefined){
            datosModificados[elementChange]=1;
        }
        //console.log($(datosModificados).serialize());
        $("[name=temp_ext_calc_cerr1").prop('disabled', false);
        var campos = $('input').serialize();
        $("[name=temp_ext_calc_cerr1").prop('disabled', true);
        //console.log(elementChange);
        //console.log("campos "+campos);

        $.ajax({
            url: "app/ajax/changeDatos.php",

            type: "POST",

            dataType: "JSON",

            data: {
                evento: elementChange,
                data: campos,
                datosModificados: datosModificados
            },
            success: procesoChangeDatos,
            error: errorAjax,
        });
        console.log(datosModificados);
    }
}

function procesoChangeDatos(datos){
    console.log(datos[2]);
    var formas= datos[2];
    datos= datos[1];
    var resultados= ['calculoA', 'calculoB1', 'calculoB2', 'calculoC1', 'calculoC2', 'calculoD1', 'calculoD2', 'calculoE', 'calculoF1', 'calculoF2', 'calculoG', 'calculoH', 'calculoI', 'calculoJ', 'carga_sens', 'carga_lat', 'carga_total', 'pot_frig_cam', 'pot_frig_cam_sens', 'pot_frig_cam_lat'];
    var cargasNegativas= false;
    $(".ventilacion_adiccional").hide();
    if (datos) {
        $.each( datos, function(key, value) {
            if(key== "forma"){
                var id= $('input[value='+value+'].forma').attr('id');
                $('input[value='+value+'].forma').prop('checked', true);
                $(".forma").each(function(){
                    var id = $(this).attr('id');
                    $('.active-'+id).addClass('oculto');
                })
                visibilityFields(id);
            }
            else{
                $('input[name='+key+']').val(value);
            }
            // console.log(key);
            // console.log(value);
        });
        $.each( formas, function(key, value) {
            $("#"+key).parent().hide();
            if(value > 0){
                $("#"+key).parent().show();
            }
        });
    }

    $("input[type='number'], input[name='renov_aire_adiccional']").each(function() {
        if(!Number.isInteger(Number($(this).val()))){
            if($(this).val()>= -1 && $(this).val()<= 1 && $(this).hasClass("dec-4")){
                $(this).val(parseFloat(Math.round($(this).val() * 10000) / 10000).toFixed(4));
            }
            else{
                $(this).val(parseFloat(Math.round($(this).val() * 100) / 100).toFixed(2));
            }
        }
    });

    if($("[name=renov_aire_adiccional").val()> 0 && tipoCamara== 1){
        $(".ventilacion_adiccional").show();
        $("#renov_adiccional").text($("[name=renov_aire_adiccional").val());
    }

    $('.resultados').each(function() {
        id= $(this).attr("id");
        if(!Number.isInteger(Number($("[name="+id).val()))){
            if($(this).hasClass("ent")){
                $("[name="+id).val(Math.round($("[name="+id).val()));
            }
            else{
                $("[name="+id).val(parseFloat(Math.round($("[name="+id).val() * 100) / 100).toFixed(2));
            }
        }
        $("#"+id).text(Number($("[name="+id).val()).toLocaleString());
    });

    $("[name=num_lamp").val(Math.round($("[name=num_lamp").val()));
    $("[name=num_personas").val(Math.round($("[name=num_personas").val()));
    $("[name=capacidad_cam").val(Math.round($("[name=capacidad_cam").val()));
    $("[name=carga_diaria").val(Math.round($("[name=carga_diaria").val()));
    $("[name=pot_vent_estima").val(Math.round($("[name=pot_vent_estima").val()));
    $("[name=pot_resist_estima").val(Math.round($("[name=pot_resist_estima").val()));
    $("[name=pot_vent").val(Math.round($("[name=pot_vent").val()));
    $("[name=pot_resist").val(Math.round($("[name=pot_resist").val()));
    $("[name=tiempo_cong").val(Math.round($("[name=tiempo_cong").val()));
    $("[name=tiempo_enf").val(Math.round($("[name=tiempo_enf").val()));
    $("#potenciaTotalHeader").text(Number(Math.round($("[name=pot_frig_cam").val())).toLocaleString());

    $.each( resultados, function(key, value) {
        if($("[name="+value).val()< 0){
            cargasNegativas= true;
        }
    });

    if(cargasNegativas && !noMostrar){
        $.confirm({
            icon: 'fa fa-warning',
            title: 'Cargas térmicas negativas',
            content: '<div class="content-pane" style="transition-duration: 0.5s; transition-timing-function: cubic-bezier(0.36, 1.1, 0.2, 1); height: 20px;"><div class="content" style="clip: rect(0px 430px 40px -100px);">Al realizar el cálculo han resultado cargas térmicas negativas. Revisa los datos introducidos</div></div>',
            confirmButton: 'Ok',
            cancelButton: 'No mostrar más',
            confirm: function () {
            },
            cancel: function () {
                noMostrar= true;
            }
        });
    }
}

function errorAjax(datos){
    console.log(datos);
}

function guardarProyecto(){
    var campos = $('input, textarea').serialize();
    $.ajax({

        url: "app/ajax/guardarProyecto.php",

        type: "POST",

        data: {
            data: campos,
            datosModificados: datosModificados
        },

        success: procesoDatosGuardar,

        error: errorAjax,
    });
}

function editarProyecto(idProyecto){
    var campos = $('input, textarea').serialize();
    $.ajax({
        url: "app/ajax/editarProyecto.php",
        type: "POST",
        data: {
            data: campos,
            datosModificados: datosModificados
        },
        success: procesoDatosGuardar,
        error: errorAjax,
    });
}

function procesoDatosGuardar(datos){
    if(datos[0]== 1){
        guardado= true;
        alertify.success( 'Los datos se han guardado correctamente');
        if(datos[1]){
            idProyecto= datos[1];
        }
    }
}

function procesoDatosProyecto(datos){
    datosModificados= JSON.parse(datos["datosModificados"]);
    if(datosModificados["rotacion"]== 1){
        $(".rotacion_tipo").hide();
    }
    $.each( datos, function(key, value) {
        console.log($('#'+key).prop('nodeName'));
        if ($('#'+key).prop('nodeName') == 'SELECT') {
            if(key== "idCamara"){
                if(value >= 6){
                    $(".secaderoValue").val(value);
                }
                $('input[value='+value+'][name='+key+']').parent().addClass('seleccionada');
                $('input[name='+key+'][value='+value+']').prop('checked', true);
                tipoCamara=value;
            }
            $('#'+key+' option[value='+value+']').attr('selected', true);
        } else if ($('input[name='+key).attr('type') == 'radio') {
            if(key == "categoriaProducto"){
                $('input[value='+value+'][name='+key+']').parent().addClass('seleccionada');
                tipoProducto= value;
            }
            else if($('input[name='+key+'][value='+value+']').attr('id')){
                var id = $('input[name='+key+'][value='+value+']').attr('id');
                visibilityFields(id);
            }
            $('input[name='+key+'][value='+value+']').prop('checked', true);
        } else if (key== "observ_proyecto") {
            $('[name='+key).val(value);
        } else {
            if(key== "idProducto"){
                idProducto= value;
                rellenarProductos(tipoProducto);
            }
            $('input[name='+key).val(value);
        }
    });
    changeHidden($('[change-hidden]'));

    $('.seleccionada').each(function() {
        var id = $(this).attr('id');
         $(this).children('input[type=radio]').prop('checked', true);
        // console.log(id);
        visibilityFields(id);
    });
}

function procesoBorrarProyecto(datos){
    $("#"+idProyecto).remove();
}

function datosResultados(){
    potencia=(($("#voluCam").val()*$("#capAlm").val())/$("#densNetAlm").val())*40;

    $("#horasDiarias").html(7*$("#numRenov").val());

    $("#factorSeguridad").html(10);

    $("#potenciaFrigor").html(potencia);

//$("#tiempoCong").html($(datos).find("potencia").text());

    $("#tempCamara").html(40);

    $("#hrCamara").html(30);
}

function visibilityFields(id) {
    //console.log('.active-' + id);
    $('.active-' + id).removeClass('oculto');
}

function changeHidden(elements) {
    $(elements).each(function(i, el) {
        var changeHiddenName = $(el).attr('change-hidden');
        var value = $(el).val();
        $('.'+changeHiddenName).val(value);
    });
}

function comprobarRotacionTipo(rotacion) {
    /*if(rotacion>= 42){
        $("[name=rotacion_tipo][value='1']").prop('checked', true);
    }
    else if(rotacion< 42 && rotacion >= 26){
        $("[name=rotacion_tipo][value='2']").prop('checked', true);
    }
    else{
        $("[name=rotacion_tipo][value='3']").prop('checked', true);
    }*/
    $(".rotacion_tipo").hide();
}

function datosDefecto() {
    var defecto= {};
    defecto["long_cam"]= 2;
    defecto["ancho_cam"]= 2;
    defecto["alto_cam"]= 2;
    defecto["rotacion_tipo"]= 2;
    defecto["doble_puerta"]= 2;
    defecto["tipo_ubicacion"]= 1;
    defecto["cerr_ext"]= 2;
    defecto["orientacion_cerr1"]= 1;
    defecto["tipo_cubierta"]= 1;
    defecto["color_cubierta"]= 1;
    defecto["tipo_cerr2"]= 3;
    defecto["color_cerr2"]= 1;
    defecto["tipo_cerr3"]= 3;
    defecto["color_cerr3"]= 1;
    defecto["tipo_cerr4"]= 3;
    defecto["color_cerr4"]= 1;
    defecto["cerr_verticales"]= 1;
    defecto["material_cerramiento_1"]= 2;
    defecto["material_cerramiento_2"]= 2;
    defecto["material_cerramiento_3"]= 2;
    defecto["material_cerramiento_4"]= 2;
    defecto["material_techo"]= 2;
    defecto["material_puertas"]= 2;
    defecto["ventanas"]= 2;
    defecto["material_solera"]= 21;
    defecto["aislamiento"]= 1;
    defecto["material_aislamiento"]= 3;
    defecto["camara_de_aire"]= 2;
    defecto["tipoNumPot"]= 2;
    defecto["tipoEvap"]= 2;
    defecto["potVentEst"]= 1;
    defecto["potResisEst"]= 1;
    defecto["potConEst"]= 1;
    datosModificados= {};
    $(".rotacion_tipo").show();
    $.each( defecto, function(key, value) {
        if($('input[name='+key).attr('type') == 'radio'){
            $('input[name='+key+'][value='+value+']').prop('checked', true);
            $("[name="+key+"]").each(function(){
                var id = $(this).attr('id');
                $('.active-'+id).addClass('oculto');
            });
            visibilityFields($('input[name='+key+'][value='+value+']').attr('id'));
        }
        else if($('#'+key).prop('nodeName') == 'SELECT'){
            $('#'+key+' option').attr('selected', false);
            $('#'+key+' option[value='+value+']').attr('selected', true);
            $('#'+key).val(value);
        }
        else{
            $('input[name='+key).val(value);
        }
    });
    //changeHidden($('[change-hidden]'));
}

function procesoInformeResultados(datos){
    window.location= "descargarWord.php";
}

function datosDefectoCamara(elementCamara) {
    $(".idCamara").removeClass("seleccionada");

    id=$(elementCamara).attr("id");

    tipoCamara=$(elementCamara).children("input[name=idCamara]").val();

    $(elementCamara).addClass("seleccionada");

    $(".productos").html("");

    $(elementCamara).children('input[type=radio]').prop('checked', true);

    $(elementCamara).siblings('.idCamara').each(function(){
        var id = $(this).attr('id');
        $('.active-'+id).addClass('oculto');
        //console.log(id+' ocultados');
    })
    visibilityFields($(elementCamara).attr('id'));

    $('input[name=categoriaProducto]:checked').prop('checked', false);
    $(".categoriaProducto").removeClass("seleccionada");
    elementChange = $(elementCamara).children('input').attr("name");
    if(tipoCamara < 6){
        $("#produc-variado").addClass("seleccionada");
        if(tipoCamara == 3 || tipoCamara == 4){
            $(".categoriaProducto").removeClass("seleccionada");
            $("#otros-productos").addClass("seleccionada");
            $('input[name=categoriaProducto][value=6]').prop('checked', true);
            $("[name=calculoF").val(0);
            $("[name=calculoF1").val(0);
            $("[name=calculoF2").val(0);
            $("[name=porcentaje_calculoF").val(0);
            $("[name=calculoG").val(0);
            $("[name=porcentaje_calculoG").val(0);
            if(tipoCamara== 4){
                $("[name=camara_de_aire][value='1']").prop('checked', true);
            }
            rellenarProductos(6);
        }
        else{
            if(tipoCamara == 5){
                $("[name=aislamiento][value='2']").prop('checked', true);
                $('.active-aislamiento').addClass('oculto');
            }
            if(tipoCamara== 2){
                $("[name=camara_de_aire][value='1']").prop('checked', true);
            }
            $('input[name=categoriaProducto][value=7]').prop('checked', true);
            rellenarProductos(7);
        }
        //$('input[name=idProducto]').val(701);
    }
    else{
        $("#producto-carnes").addClass("seleccionada");
        $('input[name=categoriaProducto][value=1]').prop('checked', true);
        rellenarProductos(1);
        //$('input[name=idProducto]').val(119);
    }

    $(".potenciaTotalHeader").show();
    $(".guardar").show();
}

function comprobarMaximoMinimo(elementChange) {
    $("[name="+elementChange).removeClass("error");
    if(parseFloat($("[name="+elementChange).val())< $("[name="+elementChange).attr("min") || parseFloat($("[name="+elementChange).val())> $("[name="+elementChange).attr("max") ){
        $("[name="+elementChange).addClass("error");

        var max = parseFloat($("[name="+elementChange).attr('max'));
        var min = parseFloat($("[name="+elementChange).attr('min'))

        if (parseFloat($("[name="+elementChange).val()) > max)
        {
            $("[name="+elementChange).val(max);
        }
        else if (parseFloat($("[name="+elementChange).val()) < min)
        {
            $("[name="+elementChange).val(min);
        } 
    }
}