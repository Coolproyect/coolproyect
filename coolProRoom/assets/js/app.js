$(document).ready(function() {
    $("#login").validate({
        //debug: true,
        rules: {
            password: {
                required: true,
            },
            username: {
                required: true,
                email: true
            },
        },
        messages: {
            password:{
              required: "Por favor, introduzca su contraseña"
             },
            username: "Por favor, introduzca su dirección de correo electrónico",
        },
        submitHandler: submitFormLogin
    });

    if($(window).width() < 1280){
        location.href = "https://coolproyect.es/no-disponible/";
    }
});

function submitFormLogin() {
    var data = $("#login").serialize();
    $.ajax({
        type : 'POST',
        url  : 'loginLogout.php?action=login',
        data : data,
        beforeSend: function(){
            $("#error").fadeOut();
        },
        success : function(response){
            console.log(response);
            var data = $.parseJSON(response);

            if($.trim(data[0]) === "1"){

                localStorage.setItem( 'token-GB', data[1] );

                $("#login_button").html('Entrando ...');
                setTimeout(' window.location.href = "index.php"; ',200);
            } else {
                $("#error").fadeIn(1000, function(){
                    $("#error").html(data[0]).show();
                });
            }
        }
    });
    return false;
}