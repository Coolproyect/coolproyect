<?php

class BD {
	protected $servername;
	protected $username;
	protected $password;
	protected $dbname;
	protected $conn;
	protected $result;
	protected $row;
	protected $rows;


	function __construct( $servername, $username, $password, $dbname) {
		$this->servername = $servername;
		$this->username = $username;
		$this->password = $password;
		$this->dbname= $dbname;
		// Create connection
		$this->conn = new mysqli($servername, $username, $password, $dbname);
		mysqli_set_charset($this->conn, "utf8");
		// Check connection
		if ($this->conn->connect_error) {
		    die("Connection failed: " . $conn->connect_error);
		}
	}


	function insert( $nombreTabla, $data ) {
		$row = array();
	    $nilai = array();
	    foreach ( $data as $kolom =>$value )
	           {
	        $row[] = $kolom;
	        $nilai[] = "'".$value."'";
	    }

	    $this->result = $this->conn->query("INSERT INTO ". $nombreTabla ."(". implode(',' ,$row) .")
	                    VALUES (". implode(',' , $nilai) .")");
	}

	function update($nombreTabla , $data , $where)
	  {
	    foreach ( $data as $kolom => $row )
	    {
	        $set[]= $kolom."='".$row."'" ;
	    }
	    $set = implode(',',$set);
	    $query = "UPDATE ".$nombreTabla." SET ".$set." WHERE ".$where ;
	    $this->conn->query($query);
	}

	function delete($nombreTabla , $where)
	{
	    $this->conn->query("DELETE FROM ".$nombreTabla." WHERE ".$where);
	}

	function get($nombreTabla, $where = '1')
	{
	  $this->result = $this->conn->query("SELECT * FROM ".$nombreTabla." WHERE ".$where);

	}

	function getColumn($column, $nombreTabla, $where = '1')
	{
	  $this->result = $this->conn->query("SELECT $column FROM ".$nombreTabla." WHERE ".$where);

	}

	function getColumnOrderBy($column, $nombreTabla, $where = '1', $orderBy)
	{
	  $this->result = $this->conn->query("SELECT $column FROM ".$nombreTabla." WHERE ".$where." ORDER BY ".$orderBy);

	}

	public function getRowSelect()
	{
	  $this->row = mysqli_fetch_array($this->result, MYSQLI_ASSOC);
	  return $this->row;
	}

	public function getRowsSelect()
	{
		$this->rows= array();
		while($this->row=mysqli_fetch_array($this->result, MYSQLI_ASSOC))
        {
        	$this->rows[] =$this->row;
        }

	  	return $this->rows;
	}

	public function getConn() {
		return $this->conn;
	}


}
