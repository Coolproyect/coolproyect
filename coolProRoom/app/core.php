<?php

if (strlen(session_id()) < 1) {
    session_start();
}

include 'models/BD.php';
include_once 'language.php';
//var_dump(file_exists('../language.php'));
define('DB_HOST', 'localhost');
define('DB_NAME', 'kxqavady_coolproyect');
define('DB_USER', 'root');
define('DB_PASSWORD', '');

define('CHAR_KEY_OPEN',         '{');
define('CHAR_KEY_CLOSE',        '}');
define('NAME_ARRAY_FORMULAS',   'formulas');
define('NAME_ARRAY_FORMULAS_TUNEL',   'formulas_tunel');
define('NAME_ARRAY_FORMULAS_SECADERO',   'formulas_secadero');

include 'formulasParaArray.php';
include 'function.php' ;
