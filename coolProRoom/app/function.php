<?php

//Se simplifica el array que llega del formulario a un array asociativo
function getCleanInputForm( $inputFields ) {

	$dataForm = array();
	for ( $i = 0; $i < count( $inputFields ); $i++ ) {
		$dataForm[ $inputFields[ $i ][ 'name' ] ] = $inputFields[ $i ][ 'value' ];
	}

	return $dataForm;
}


/**
 * [getCalculatedFormulas description]
 * @param  [array] $dataForm   [key y valor de los campos del formulario]
 * @param  [array] $dataChange [valor o valores del formulario o base de datos
 * con su clave correspondiente]
 * @return [type]             [Devuelve un array con las fórmulas ya calculadas]
 *
 * Se recorre el array con las FÓRMULAS y se compara con con los valores de $dataChange.
 * Si no coinciden las keys de los respectivos arrays asociativos, se van añadiendo
 * los valores que vienen del formulario a FÓRMULAS, en caso contrario, cambiamos
 * el valor del array FÓRMULAS por el de $dataChange, y a la próxima vuelta del
 * foreach, se empiezan a calcular las FÓRMULAS
 */
function getCalculatedFormulas( $dataForm, $dataChange ) {
	include 'formulasParaArray.php';

	if($dataForm["idCamara"]== 1 || $dataForm["idCamara"] == 2 || $dataForm["idCamara"] == 5){
		$formulasSystem = ${NAME_ARRAY_FORMULAS};
	}
	else if($dataForm["idCamara"]== 3 || $dataForm["idCamara"]== 4){
		$formulasSystem = ${NAME_ARRAY_FORMULAS_TUNEL};
	}
	else{
		$formulasSystem = ${NAME_ARRAY_FORMULAS_SECADERO};
	}
	$flag = false;
	$isEqualName = false;
	$valores_cerr_camara["camara1"]= array("80", "100", "120", "150", "180", "200", "220", "250");
	$valores_cerr_camara["camara2"]= array("100", "120", "150", "180", "200", "220", "250");
	$valores_cerr_camara["camara3"]= array("80", "100", "120", "150", "180", "200", "220", "250");
	$valores_cerr_camara["camara4"]= array("100", "120", "150", "180", "200", "220", "250");
	$valores_cerr_camara["camara5"]= array("40", "60", "80", "100", "120", "150", "180", "200", "220", "250");
	$valores_cerr_camara["camara6"]= array("80", "100", "120", "150", "180", "200", "220", "250");
	$valores_cerr_camara["camara7"]= array("80", "100", "120", "150", "180", "200", "220", "250");
	$valores_cerr_camara["camara8"]= array("40", "60", "80", "100", "120", "150", "180", "200", "220", "250");
	$valores_cerr_camara["camara9"]= array("40", "60", "80", "100", "120", "150", "180", "200", "220", "250");
	$valores_cerr_camara["camara10"]= array("40", "60", "80", "100", "120", "150", "180", "200", "220", "250");
	$valores_aisla_suelo= array("10", "20", "30", "40", "50", "60", "70", "80", "90", "100", "120", "140", "150", "160", "180", "200", "220", "250");

	foreach ( $formulasSystem as $key => $formula ) {

		if ( count( $dataChange ) > 0 ) {

			foreach ($dataChange as $name => $value) {
				# Cambia el array de fórmulas con los valores
				# cambiantes del dataForm
				if ( $key == $name ) {
					$formulasSystem[ $key ] = $value;

					unset( $dataChange[ $key ] );

					$isEqualName = true;
					$flag = false;

				//se añade el valor de los campos del formulario hasta que
				//coincida el nombre del campo con el de las fórmulas
				} elseif ( !$isEqualName ) {
					$formulasSystem[ $key ] = $dataForm[ $key ];
				}
			}
		}

		/**
		 * [if description]
		 * @var [type]
		 * Se empiezan a calcular fórmulas en la siguiente vuelta del foreach
		 * en caso de que flag esté a true
		 */
		if ( $flag ) {
			//comprueba si hay condicional en la key
			$isOperation= false;
			if ( isOperationInclude( $formula ) ) {
				$formula = getOperationTrue( $formula, $formulasSystem );
				$isOperation= true;
			}

			//Si el valor en las fórmulas es 0, se usará el valor del
			//campo del formulario, si no, se calculará la fórmula
			if ($formula == '0' && !$isOperation) {
				$formulasSystem[ $key ] = $dataForm[ $key ];

			} else {
				$formula = getFormulaTransformed( $formula, $formulasSystem );
				$formulasSystem[ $key ] = calculateExpresion( $formula );
			}

			if($key == "espe_cerr1_calc" || $key == "espe_cerr2_calc" || $key == "espe_cerr3_calc" || $key == "espe_cerr4_calc" || $key == "espe_techo_calc" || $key == "espe_puerta"){
				$idCamara= $dataForm["idCamara"];
				$flag = false;
				foreach ( $valores_cerr_camara["camara".$idCamara] as $valor) {
					if($formulasSystem[ $key ] <= $valor && !$flag){
						$formulasSystem[ $key ]= $valor;
						$flag = true;
					}
				}

				if(!$flag){
					$formulasSystem[ $key ]= 250;
				}
			}
			else if($key == "espe_aisla_suelo" && $formulasSystem[ $key ]){
				$flag = false;
				foreach ( $valores_aisla_suelo as $valor) {
					if($formulasSystem[ $key ] <= $valor && !$flag){
						$formulasSystem[ $key ]= $valor;
						$flag = true;
					}
				}
			}


		}

		$flag = $isEqualName;

	}

	return $formulasSystem;
}

function isOperationInclude( $mathFormula ) {
	$flag = false;
	if ( count( $mathFormula ) > 1 ) {
		$flag = true;
	}
	return $flag;
}

function getOperationTrue( $operators, $formulas ) {
	$formulaResult = "";
	foreach ( $operators as $operation => $mathFormula ) {
		$expression = getFormulaTransformed( $operation, $formulas);

		if ( calculateExpresion( $expression ) ){
			$formulaResult = $mathFormula;
		}
	}
	return $formulaResult;
}

function getFormulaTransformed( $mathFormulaString, $formulasSystem ) {

	$claves = extractKey( $mathFormulaString );

	for ( $i=0; $i < count( $claves ); $i++ ) {

		if ( strpos( $mathFormulaString, $claves[ $i ] ) ) {
			$varKey = CHAR_KEY_OPEN . $claves[ $i ] . CHAR_KEY_CLOSE;
			if(!$formulasSystem[ $claves[ $i ] ]){
				$formulasSystem[ $claves[ $i ] ]= 0;
			}
			$mathFormulaKey = $formulasSystem[ $claves[ $i ] ];
			$mathFormulaString = str_replace( $varKey, $mathFormulaKey, $mathFormulaString );
		}
	}


	return $mathFormulaString;
}

function extractKey( $cadena, $charApertura = CHAR_KEY_OPEN, $charCierre = CHAR_KEY_CLOSE ) {
	$posicion = array();

	for ( $i=0; $i < strlen($cadena); $i++ ) {
		if ( $charApertura == $cadena{ $i } || $charCierre == $cadena{ $i } ) {
			$posicion[] = $i;
		}
	}

	$result = array();
	for ($i=0; $i < count( $posicion ) ; $i += 2 ) {
		$inicio = $posicion[ $i ] + 1;
		$final = $posicion[ $i + 1 ];
		$logitud = $final - $inicio;
		$result[] = substr( $cadena, $inicio, $logitud );
	}

	return $result;
}

function calculateExpresion( $expresion ) {
	$result = eval('return ' . $expresion . ';');
	return $result;
}

function extractChanges( $dataForm, $formulas ) {
	$result = array_diff_assoc( $dataForm, $formulas );
	return $result;
}

function is_page( $page_name ) {
    $current_file = basename( $_SERVER['PHP_SELF'] );

    $bool = false;
    if ( $current_file == $page_name ) {
        $bool = true;
    }
    return $bool;
}

if ( !is_page('login.php')){
	/* Si no hay una sesión creada, redireccionar al index. */
	if(empty($_SESSION['username'])) { // Recuerda usar corchetes.
		header('Location: login.php');
	} // Recuerda usar corchetes
}
else {
	if(!empty($_SESSION['username'])) { // Recuerda usar corchetes.
		header('Location: index.php');
	} // Recuerda usar corchetes
}


 ?>
