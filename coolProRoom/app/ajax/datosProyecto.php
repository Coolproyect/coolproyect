<?php

header("Content-type: application/json");

include '../core.php';

$bd = new BD(DB_HOST, DB_USER, DB_PASSWORD, DB_NAME);

$id= $_REQUEST['idProyecto'];

try {
	$bd->get('proyecto', 'id= '.$id);
	$datos = $bd->getRowSelect();
	echo json_encode( $datos);
} catch (Exception $e) {
	echo json_encode(['error - getDataProyecto' => 'Al obtener los datos']);
}