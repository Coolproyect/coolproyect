<?php

header("Content-type: application/json");

include '../core.php';

$bd = new BD(DB_HOST, DB_USER, DB_PASSWORD, DB_NAME);

$data = array();
parse_str($_REQUEST['data'], $data);
$dataTrim = array_map('trim', $data);
$idProyecto= $dataTrim["id_proyecto"];
$dataTrim["datosModificados"]= json_encode($_REQUEST["datosModificados"]);
unset($dataTrim["id_proyecto"]);
$datos= array();

try {
	$bd->update('proyecto', $dataTrim, "id= ".$idProyecto);
	$datos[0]= 1;
	echo json_encode($datos);
} catch (Exception $e) {
	echo json_encode(['error - editarProyecto' => 'Al editar proyecto']);
}