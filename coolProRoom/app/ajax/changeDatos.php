<?php

header("Content-type: text/xml");

include '../core.php';

error_reporting(0);

$bd = new BD(DB_HOST, DB_USER, DB_PASSWORD, DB_NAME);

$data = array();
parse_str($_REQUEST['data'], $data);
$dataTrim = array_map('trim', $data);
$dataInput= array();
$dataProyecto= array();
$formas= array();

foreach ($_REQUEST['datosModificados'] as $key => $value) {
	if($value== 1){
		$dataInput[$key]= $dataTrim[$key];
	}
}
switch ($_REQUEST['evento']) {

	case 'idProvincia':
	try {
			$dataInput= datosProvincia($bd, $dataTrim);
			//echo json_encode($datos);

		} catch (Exception $e) {
			echo json_encode(['error - getDataCar' => 'Al obtener los datos del vehículo']);
		}
		break;

	case 'idCamara':
	try {
			$dataInput["idCamara"]= $dataTrim["idCamara"];
			$dataInput= $dataInput+ datosCamara($bd, $dataTrim);
			$dataInput= $dataInput+ datosProducto($bd, $dataTrim);

			$datos= datosCerramiento($bd, $dataTrim, $dataTrim["material_cerramiento_1"]);
			$dataInput["cond_cerr1"]= $datos["cond"];
			$datos= datosCerramiento($bd, $dataTrim, $dataTrim["material_cerramiento_2"]);
			$dataInput["cond_cerr2"]= $datos["cond"];
			$datos= datosCerramiento($bd, $dataTrim, $dataTrim["material_cerramiento_3"]);
			$dataInput["cond_cerr3"]= $datos["cond"];
			$datos= datosCerramiento($bd, $dataTrim, $dataTrim["material_cerramiento_4"]);
			$dataInput["cond_cerr4"]= $datos["cond"];
			$datos= datosCerramiento($bd, $dataTrim, $dataTrim["material_techo"]);
			$dataInput["cond_techo"]= $datos["cond"];
			$datos= datosCerramiento($bd, $dataTrim, $dataTrim["material_puertas"]);
			$dataInput["cond_puerta"]= $datos["cond"];
			$datos= datosCerramiento($bd, $dataTrim, $dataTrim["material_ventanas"]);
			$dataInput["material_ventanas"]= $dataTrim["material_ventanas"];
			$dataInput["cond_ventana"]= $datos["cond"];
			$datos= datosCerramiento($bd, $dataTrim, $dataTrim["material_solera"]);
			$dataInput["cond_suelo"]= $datos["cond"];
			$datos= datosCerramiento($bd, $dataTrim, $dataTrim["material_aislamiento"]);
			$dataInput["cond_aisla_suelo"]= $datos["cond"];
			//echo json_encode($dataInput);

		} catch (Exception $e) {
			echo json_encode(['error - getDataCar' => 'Al obtener los datos del vehículo']);
		}
		break;

	case 'categoriaProducto':
	try {
			$dataInput= $dataInput+ datosCamara($bd, $dataTrim);
			$dataInput= $dataInput+ datosProducto($bd, $dataTrim);
			//echo json_encode($dataInput);

		} catch (Exception $e) {
			echo json_encode(['error - getDataCar' => 'Al obtener los datos del vehículo']);
		}
		break;

	case 'idProducto':
	try {
			$dataInput= datosCamara($bd, $dataTrim);
			$dataInput= $dataInput+ datosProducto($bd, $dataTrim);
			//echo json_encode($dataInput);

		} catch (Exception $e) {
			echo json_encode(['error - getDataCar' => 'Al obtener los datos del vehículo']);
		}
		break;

	case 'id_forma':
	try {
			$dataInput= datosForma($bd, $dataTrim);
			//echo json_encode($dataInput);

		} catch (Exception $e) {
			echo json_encode(['error - getDataCar' => 'Al obtener los datos del vehículo']);
		}
		break;
	case 'material_cerramiento_1':
	try {
			$datos= datosCerramiento($bd, $dataTrim, $dataTrim["material_cerramiento_1"]);
			$dataInput["cond_cerr1"]= $datos["cond"];
			//echo json_encode($dataInput);

		} catch (Exception $e) {
			echo json_encode(['error - getDataCar' => 'Al obtener los datos del vehículo']);
		}
		break;
	case 'material_cerramiento_2':
	try {
			$datos= datosCerramiento($bd, $dataTrim, $dataTrim["material_cerramiento_2"]);
			$dataInput["cond_cerr2"]= $datos["cond"];
			//echo json_encode($dataInput);

		} catch (Exception $e) {
			echo json_encode(['error - getDataCar' => 'Al obtener los datos del vehículo']);
		}
		break;
	case 'material_cerramiento_3':
	try {
			$datos= datosCerramiento($bd, $dataTrim, $dataTrim["material_cerramiento_3"]);
			$dataInput["cond_cerr3"]= $datos["cond"];
			//echo json_encode($dataInput);

		} catch (Exception $e) {
			echo json_encode(['error - getDataCar' => 'Al obtener los datos del vehículo']);
		}
		break;
	case 'material_cerramiento_4':
	try {
			$datos= datosCerramiento($bd, $dataTrim, $dataTrim["material_cerramiento_4"]);
			$dataInput["cond_cerr4"]= $datos["cond"];
			//echo json_encode($dataInput);

		} catch (Exception $e) {
			echo json_encode(['error - getDataCar' => 'Al obtener los datos del vehículo']);
		}
		break;
	case 'material_techo':
	try {
			$datos= datosCerramiento($bd, $dataTrim, $dataTrim["material_techo"]);
			$dataInput["cond_techo"]= $datos["cond"];
			//echo json_encode($dataInput);

		} catch (Exception $e) {
			echo json_encode(['error - getDataCar' => 'Al obtener los datos del vehículo']);
		}
		break;
	case 'material_puertas':
	try {
			$datos= datosCerramiento($bd, $dataTrim, $dataTrim["material_puertas"]);
			$dataInput["cond_puerta"]= $datos["cond"];
			//echo json_encode($dataInput);

		} catch (Exception $e) {
			echo json_encode(['error - getDataCar' => 'Al obtener los datos del vehículo']);
		}
		break;
	case 'material_ventanas':
	try {
			$datos= datosCerramiento($bd, $dataTrim, $dataTrim["material_ventanas"]);
			$dataInput["material_ventanas"]= $dataTrim["material_ventanas"];
			$dataInput["cond_ventana"]= $datos["cond"];
			//echo json_encode($dataInput);

		} catch (Exception $e) {
			echo json_encode(['error - getDataCar' => 'Al obtener los datos del vehículo']);
		}
		break;
	case 'material_solera':
	try {
			$datos= datosCerramiento($bd, $dataTrim, $dataTrim["material_solera"]);
			$dataInput["cond_suelo"]= $datos["cond"];
			//echo json_encode($dataInput);

		} catch (Exception $e) {
			echo json_encode(['error - getDataCar' => 'Al obtener los datos del vehículo']);
		}
		break;
	case 'material_aislamiento':
	try {
			$datos= datosCerramiento($bd, $dataTrim, $dataTrim["material_aislamiento"]);
			$dataInput["cond_aisla_suelo"]= $datos["cond"];
			//echo json_encode($dataInput);

		} catch (Exception $e) {
			echo json_encode(['error - getDataCar' => 'Al obtener los datos del vehículo']);
		}
		break;
}
$eventoChange= $_REQUEST['evento'];
$dataInput[$eventoChange]= $dataTrim[$eventoChange];
//echo json_encode($dataInput);

$formulasResult = getCalculatedFormulas( $dataTrim, $dataInput);
//devuelve aquellos elementos que han cambiado
$result = extractChanges( $formulasResult, $dataTrim);
$formas["esfera"]= $formulasResult["diam_esfera"];
$formas["cilindro"]= $formulasResult["diam_cilindro"];
$formas["rodaja"]= $formulasResult["diam_rodaja"];
$formas["filete_fino"]= $formulasResult["espe_filete"];
$formas["prisma"]= $formulasResult["long_prisma"];
$formas["ovalo"]= $formulasResult["long_oval"];
$comprobaciones["0"]= $formulasResult;
$comprobaciones["1"]= $result;
$comprobaciones["2"]= $formas;

echo json_encode( $comprobaciones, JSON_PARTIAL_OUTPUT_ON_ERROR);

function datosProvincia($bd, $dataTrim){
	$bd->get('provincia', 'id= '.$dataTrim['idProvincia']);
	$datos = $bd->getRowSelect();
	return $datos;
}

function datosCamara($bd, $dataTrim){
	if($dataTrim['idCamara']< 6){
		$bd->get('productocamara', 'idCamara= '.$dataTrim['idCamara']. ' AND idProducto= '.$dataTrim['idProducto']);
		$datos = $bd->getRowSelect();
	}
	else{
		$bd->get('productosecadero', 'idCamara= '.$dataTrim['idCamara']. ' AND idProducto= '.$dataTrim['idProducto']);
		$datos = $bd->getRowSelect();
	}
	unset($datos['id'], $datos['idProducto']);
	return $datos;
}

function datosProducto($bd, $dataTrim){
	if($dataTrim['idCamara']< 6){
		$bd->get('producto', 'id= '.$dataTrim['idProducto']);
		$datos = $bd->getRowSelect();
	}
	else{
		$bd->get('productoseco', 'id= '.$dataTrim['idProducto']);
		$datos = $bd->getRowSelect();
	}
	unset($datos['id'], $datos['id_categor_prod'], $datos['nombre'], $datos['name']);
	return $datos;
}

function datosForma($bd, $dataTrim){
	$bd->getColumn('diam_esfera, diam_cilindro, long_cilindro, diam_rodaja, espe_rodaja, espe_filete, long_prisma, ancho_prisma, espe_prisma, long_oval, ancho_oval, espe_oval', 'producto', 'id= '.$dataTrim['idProducto']);
	$datos = $bd->getRowSelect();
	return $datos;
}

function datosCerramiento($bd, $dataTrim, $cerramiento){
	$bd->getColumn('cond', 'cerramientos', 'id= '.$cerramiento);
	$datos = $bd->getRowSelect();
	return $datos;
}