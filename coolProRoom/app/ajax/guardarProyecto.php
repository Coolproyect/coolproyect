<?php

header("Content-type: application/json");

include '../core.php';

$bd = new BD(DB_HOST, DB_USER, DB_PASSWORD, DB_NAME);

$data = array();
parse_str($_REQUEST['data'], $data);
$dataTrim = array_map('trim', $data);
$dataTrim["idUsuario"]= $_SESSION['username'];
$dataTrim["datosModificados"]= json_encode($_REQUEST["datosModificados"]);
unset($dataTrim["id_proyecto"]);
$datos= array();

try {
	$bd->insert('proyecto', $dataTrim);
	$datos[0]= 1;
	$datos[1]= $bd->getConn()->insert_id;
	echo json_encode($datos);
} catch (Exception $e) {
	echo json_encode(['error - guardarProyecto' => 'Al guardar proyecto']);
}