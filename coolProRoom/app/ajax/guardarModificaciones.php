<?php

header("Content-type: application/json");

include '../core.php';

$bd = new BD(DB_HOST, DB_USER, DB_PASSWORD, DB_NAME);

$dataProyecto["id_proyecto"]= $_REQUEST['idProyecto'];
$datos= array();
$datosModificados= json_encode($_REQUEST['datosModificados']);

try {
	$dataProyecto["dato"]= $datosModificados;
	$bd->insert('proyectomodific', $dataProyecto);
	$datos[0]= 1;
	echo json_encode($datos);
} catch (Exception $e) {
	echo json_encode(['error - guardarProyecto' => 'Al guardar modificaciones']);
}