<?php
${NAME_ARRAY_FORMULAS} = array(
    'temp_ext'=>'0',
    'hum_ext'=>'0',
    'temp_suelo'=>'({temp_ext}+15)/2',
    'altitud'=>'0',
    'vel_viento'=>'0',
    'temp_cam'=>'0',
    'hum_cam'=>'0',
    'idCamara'=>'0',
    'presion'=>'101325*pow((1-0.0065/(15+273.15)*{altitud}),(9.807/0.2883419/0.0065/1000))',
    'p_sat_agua_cam'    =>  array(
            '{temp_cam} < 0'    =>  'exp(-5674.5359*pow(({temp_cam}+273.15),-1)+6.3925247-0.009677843*({temp_cam}+273.15)+0.00000062215701*pow(({temp_cam}+273.15),2)+0.0000000020747825*pow(({temp_cam}+273.15),3)-0.0000000000009484024*pow(({temp_cam}+273.15),4)+4.1635019*log({temp_cam}+273.15))',
            '{temp_cam} >= 0'   =>  'exp(-5800.2206*pow(({temp_cam}+273.15),-1)+1.3914993-0.048640239*({temp_cam}+273.15)+0.000041764768*pow(({temp_cam}+273.15),2)-0.000000014452093*pow(({temp_cam}+273.15),3)+6.5459673*log({temp_cam}+273.15))'
    ),
    'hum_absol_cam'=>'1000*0.62198*({hum_cam}/100)*{p_sat_agua_cam}/({presion}-({hum_cam}/100)*{p_sat_agua_cam})',
    'p_parcial_agua_cam'=>'(({presion}/1000)*{hum_absol_cam}/1000)/(0.2883419/0.46191511+{hum_absol_cam}/1000)',
    'temp_rocio_cam'    =>  array(
            '{temp_cam} < 0'    =>  '5.994+12.41*log({p_parcial_agua_cam})+0.4273*pow(log({p_parcial_agua_cam}),2)',
            '{temp_cam} >= 0'   =>  '6.983+14.38*log({p_parcial_agua_cam})+1.079*pow(log({p_parcial_agua_cam}),2)'
    ),
    'entalpia_cam'=>'(1.006*{temp_cam}+({hum_absol_cam}/1000)*(2501.3+1.89*{temp_cam}))*1000',
    'entalpia_cam_sens'=>'(1.006*{temp_cam}+({hum_absol_cam}/1000)*(1.89*{temp_cam}))*1000',
    'entalpia_cam_lat'=>'({hum_absol_cam}/1000)*(2501.3)*1000',
    'volumen_espe_cam'=>'0.2870551882*({temp_cam}+273.15)*(1+1.6078*{hum_absol_cam}/1000)/101.32503',
    'dt_vent'    =>  array(
            '{idCamara} == 1 || {idCamara} == 2'    =>  '-0.4267*{hum_cam}+42.502',
            '{idCamara} == 5'   =>  '-0.4493*{hum_cam}+46.158',
    ),
    'dt_est'=>'-0.472*{hum_cam}+49.815',

    'tipoEvap'=>'0',
    'temp_evap'    =>  array(
            '{tipoEvap} == 2'    =>  '{temp_cam}-({dt_vent})',
            '{tipoEvap} == 1'   =>  '{temp_cam}-({dt_est})'
    ),
    'p_sat_agua_evap'    =>  array(
            '{temp_evap} < 0'    =>  'exp(-5674.5359*pow(({temp_evap}+273.15),-1)+6.3925247-0.009677843*({temp_evap}+273.15)+0.00000062215701*pow(({temp_evap}+273.15),2)+0.0000000020747825*pow(({temp_evap}+273.15),3)-0.0000000000009484024*pow(({temp_evap}+273.15),4)+4.1635019*log({temp_evap}+273.15))',
            '{temp_evap} >= 0'   =>  'exp(-5800.2206*pow(({temp_evap}+273.15),-1)+1.3914993-0.048640239*({temp_evap}+273.15)+0.000041764768*pow(({temp_evap}+273.15),2)-0.000000014452093*pow(({temp_evap}+273.15),3)+6.5459673*log({temp_evap}+273.15))'
    ),
    'hum_absol_evap'=>'1000*0.62198*(100/100)*{p_sat_agua_evap}/({presion}-(100/100)*{p_sat_agua_evap})',
    'p_parcial_agua_evap'=>'(({presion}/1000)*{hum_absol_evap}/1000)/(0.2883419/0.46191511+{hum_absol_evap}/1000)',
    'temp_rocio_evap'    =>  array(
            '{temp_evap} < 0'    =>  '5.994+12.41*log({p_parcial_agua_evap})+0.4273*pow(log({p_parcial_agua_evap}),2)',
            '{temp_evap} >= 0'   =>  '6.983+14.38*log({p_parcial_agua_evap})+1.079*pow(log({p_parcial_agua_evap}),2)'
    ),
    'entalpia_evap'=>'(1.006*{temp_evap}+({hum_absol_evap}/1000)*(2501.3+1.89*{temp_evap}))*1000',
    'entalpia_evap_sens'=>'(1.006*{temp_evap}+({hum_absol_evap}/1000)*(1.89*{temp_evap}))*1000',
    'entalpia_evap_lat'=>'({hum_absol_evap}/1000)*(2501.3)*1000',
    'volumen_espe_evap'=>'0.2870551882*({temp_evap}+273.15)*(1+1.6078*{hum_absol_evap}/1000)/101.32503',
    'd_entalpia_evap_sens'=>'({entalpia_cam_sens}-({entalpia_evap_sens}))/({entalpia_cam}-({entalpia_evap}))',
    'd_entalpia_evap_lat'=>'({entalpia_cam_lat}-({entalpia_evap_lat}))/({entalpia_cam}-({entalpia_evap}))',
    'vel_aire_cam'    =>  array(
            '{idCamara} == 1 || {idCamara} == 2 || {idCamara} == 5'    =>  '1',
            '{idCamara} == 3 || {idCamara} == 4'   =>  '7',
            '{idCamara} >= 6'   =>  '0.5'
    ),
    'h_cam'=>'9+3.7*{vel_aire_cam}',

    'categoriaProducto'=>'0',
    'temp_inicial'    =>  array(
            '({idCamara} == 1 || {idCamara} == 5) && {categoriaProducto} == 2'    =>  '15',
            '({idCamara} == 1 || {idCamara} == 5) && {categoriaProducto} != 2'   =>  '25',
            '{idCamara} == 2'   =>  '-10'
    ),
    'temp_final'=>'{temp_cam}',
    'agua'=>'0',
    'proteinas'=>'0',
    'grasas'=>'0',
    'carbohidratos'=>'0',
    'fibras'=>'0',
    'cenizas'=>'0',
    'temp_cong'=>'0',
    'calor_esp_fres'=>'(({agua}/100)*(4.217-0.000090864*{temp_inicial}+0.0000054731*pow({temp_inicial},2))+({proteinas}/100)*(2.0082+0.0012089*{temp_inicial}-0.0000013129*pow({temp_inicial},2))+({grasas}/100)*(1.9842+0.0014733*{temp_inicial}-0.0000048008*pow({temp_inicial},2))+({carbohidratos}/100)*(1.5488+0.0019625*{temp_inicial}-0.0000059399*pow({temp_inicial},2))+({fibras}/100)*(1.8459+0.0018306*{temp_inicial}-0.0000046509*pow({temp_inicial},2))+({cenizas}/100)*(1.0926+0.0018896*{temp_inicial}-0.0000036817*pow({temp_inicial},2)))*1000',
    'calor_esp_cong'=>'0',
    'calor_esp_cong_calc'=>'{calor_esp_cong}*1000+0.0031*(40+{temp_final})',
    'densi_fres'=>'({agua}/100)*(997.18+0.0031439*{temp_inicial}-0.0037574*pow({temp_inicial},2))+({proteinas}/100)*(1329.9-0.5184*{temp_inicial})+({grasas}/100)*(925.59-0.41757*{temp_inicial})+({carbohidratos}/100)*(1599.1-0.3104*{temp_inicial})+({fibras}/100)*(1311.5-0.36589*{temp_inicial})+({cenizas}/100)*(2423.8-0.28063*{temp_inicial})',
    'densi_cong'=>'(1.105*({agua}/100)/(1+0.7138/log({temp_cong}-({temp_final})+1)))*(916.89-0.13071*{temp_final})+({agua}/100-1.105*({agua}/100)/(1+0.7138/log({temp_cong}-({temp_final})+1)))*(997.18+0.0031439*{temp_final}-0.0037574*pow({temp_final},2))+({proteinas}/100)*(1329.9-0.5184*{temp_final})+({grasas}/100)*(925.59-0.41757*{temp_final})+({carbohidratos}/100)*(1599.1-0.31046*{temp_final})+({fibras}/100)*(1311.5-0.36589*{temp_final})+({cenizas}/100)*(2423.8-0.28063*{temp_final})',

    'A'=>'0',

    'B'=>'0',
    'pot_respi_tfinal'    =>  array(
            '{A} == 0 && {B} == 0'    =>  '0',
            '{A} != 0 || {B} != 0'   =>  '(exp({A}+{B}*{temp_final}))/1000'
    ),
    'pot_respi_tmedia'    =>  array(
            '{A} == 0 && {B} == 0'    =>  '0',
            '{A} != 0 || {B} != 0'   =>  '(exp({A}+{B}*({temp_inicial}+{temp_final})/2))/1000'
    ),
    'renov_aire'=>'0',
    'diam_esfera'=>'0',
    'diam_cilindro'=>'0',
    'long_cilindro'=>'0',
    'diam_rodaja'=>'0',
    'espe_rodaja'=>'0',
    'espe_filete'=>'0',
    'long_prisma'=>'0',
    'ancho_prisma'=>'0',
    'espe_prisma'=>'0',
    'long_oval'=>'0',
    'ancho_oval'=>'0',
    'espe_oval'=>'0',

    'long_cam'=>'0',
    'ancho_cam'=>'0',
    'alto_cam'=>'0',
    'volumen_cam'=>'{long_cam}*{ancho_cam}*{alto_cam}',
    'densi_almacen'=>'0',
    'capacidad_cam'    =>  array(
            '{idCamara} != 3 && {idCamara} != 4 && {idCamara} != 5'    =>  '{volumen_cam}*{densi_almacen}',
            '{idCamara} == 5'   =>  '{volumen_cam}*(-5.03*log({volumen_cam})+72.127)',
            '{idCamara} == 3 || {idCamara} == 4'   =>  '80*{long_cam}*{ancho_cam}'
    ),
    'rotacion_tipo'=>'0',
    'rotacion'    =>  array(
            '{rotacion_tipo} == 1 && {volumen_cam} < 100'    =>  '42',
            '{rotacion_tipo} == 1 && {volumen_cam} >= 100 && {volumen_cam} < 2000'   =>  '-7.587*log({volumen_cam})+76.566',
            '{rotacion_tipo} == 1 && {volumen_cam} > 2000'   =>  '19',
            '{rotacion_tipo} == 2 && {volumen_cam} < 100'   =>  '26',
            '{rotacion_tipo} == 2 && {volumen_cam} >= 100 && {volumen_cam} < 2000'   =>  '-4.915*log({volumen_cam})+48.603',
            '{rotacion_tipo} == 2 && {volumen_cam} >= 2000'   =>  '12',
            '{rotacion_tipo} == 3 && {volumen_cam} < 100'   =>  '16',
            '{rotacion_tipo} == 3 && {volumen_cam} >= 100 && {volumen_cam} < 2000'   =>  '-3.794*log({volumen_cam})+33.288',
            '{rotacion_tipo} == 3 && {volumen_cam} >= 2000'   =>  '5'
    ),
    'carga_diaria'=>'{capacidad_cam}* ({rotacion}/ 100)',
    'mayor_embalaje'=>'15',


    'doble_puerta'=>'0',
    'num_puertas_tunel'=>'0',
    'renov_infiltra'    =>  array(
            '{doble_puerta} == 1 && {idCamara} != 5'    =>  '((100-0.005*{volumen_cam})/sqrt({volumen_cam}))/2',
            '{doble_puerta} == 1 && {idCamara} == 5'    =>  '(((100-0.005*{volumen_cam})/sqrt({volumen_cam}))/2)*2',
            '{doble_puerta} == 2 && {idCamara} != 5'   =>  '(100-0.005*{volumen_cam})/sqrt({volumen_cam})',
            '{doble_puerta} == 2 && {idCamara} == 5'   =>  '((100-0.005*{volumen_cam})/sqrt({volumen_cam}))*2'
    ),
    'renov_aire_calc'    =>  array(
            '{idCamara} == 1 || {idCamara} == 2 || {idCamara} == 3 || {idCamara} == 4 || {idCamara} >= 6'    =>  '0',
            '{idCamara} == 5'   =>  '50*24*({long_cam}*{ancho_cam}/5)/{volumen_cam}'
    ),
    'renov_aire_adiccional'    =>  array(
            '{renov_aire} > {renov_infiltra}'    =>  '{renov_aire}-({renov_infiltra})-({renov_aire_calc})',
            '{renov_aire} <= {renov_infiltra}'   =>  '0'
    ),
    'temp_recinto'    =>  array(
            '{idCamara} == 5 || {idCamara} == 8 || {idCamara} == 9 || {idCamara} == 10'    =>  '25',
            '{idCamara} == 1 || {idCamara} == 3 || {idCamara} == 6 || {idCamara} == 7'   =>  '12',
            '{idCamara} == 2 || {idCamara} == 4'   =>  '5'
    ),
    'hum_recinto'    =>  array(
            '{idCamara} == 5 || {idCamara} == 8 || {idCamara} == 9 || {idCamara} == 10'    =>  '50',
            '{idCamara} == 1 || {idCamara} == 2 || {idCamara} == 3 || {idCamara} == 4 || {idCamara} == 6 || {idCamara} == 7'   =>  '80'
    ),
    'p_sat_agua_ext'    =>  array(
            '{temp_recinto} < 0'    =>  'exp(-5674.5359*pow(({temp_recinto}+273.15),-1)+6.3925247-0.009677843*({temp_recinto}+273.15)+0.00000062215701*pow(({temp_recinto}+273.15),2)+0.0000000020747825*pow(({temp_recinto}+273.15).3)-0.0000000000009484024*pow(({temp_recinto}+273.15),4)+4.1635019*log({temp_recinto}+273.15))',
            '{temp_recinto} >= 0'   =>  'exp(-5800.2206*pow(({temp_recinto}+273.15),-1)+1.3914993-0.048640239*({temp_recinto}+273.15)+0.000041764768*pow(({temp_recinto}+273.15),2)-0.000000014452093*pow(({temp_recinto}+273.15),3)+6.5459673*log({temp_recinto}+273.15))'
    ),
    'hum_absol_ext'=>'1000*0.62198*({hum_recinto}/100)*{p_sat_agua_ext}/({presion}-({hum_recinto}/100)*{p_sat_agua_ext})',
    'p_parcial_agua_ext'=>'(({presion}/1000)*{hum_absol_ext}/1000)/(0.2883419/0.46191511+{hum_absol_ext}/1000)',
    'temp_rocio_ext'    =>  array(
            '{temp_recinto} < 0'    =>  '5.994+12.41*log({p_parcial_agua_ext})+0.4273*pow(log({p_parcial_agua_ext}),2)',
            '{temp_recinto} >= 0'   =>  '6.983+14.38*log({p_parcial_agua_ext})+1.079*pow(log({p_parcial_agua_ext}),2)'
    ),
    'entalpia_ext'=>'(1.006*{temp_recinto}+({hum_absol_ext}/1000)*(2501.3+1.89*{temp_recinto}))*1000',
    'entalpia_ext_sens'=>'(1.006*{temp_recinto}+({hum_absol_ext}/1000)*(1.89*{temp_recinto}))*1000',
    'entalpia_ext_lat'=>'({hum_absol_ext}/1000)*(2501.3)*1000',
    'volumen_espe_ext'=>'0.2870551882*({temp_recinto}+273.15)*(1+1.6078*{hum_absol_ext}/1000)/101.32503',
    'tipo_ubicacion'=>'0',
    'cerr_ext'=>'0',

    'orientacion_cerr1'=>'0',
    'temp_ext_calc_cerr1'=>'{temp_recinto}',
    'tipo_cerr2'=>'0',
    'color_cerr2'=>'0',
    'orientacion_cerr2'    =>  array(
            '{orientacion_cerr1} == 1'    =>  '4',
            '{orientacion_cerr1} == 2'   =>  '3',
            '{orientacion_cerr1} == 3'   =>  '1',
            '{orientacion_cerr1} == 4'   =>  '2',
            '{orientacion_cerr1} == 5'   =>  '6',
            '{orientacion_cerr1} == 6'   =>  '8',
            '{orientacion_cerr1} == 7'   =>  '5',
            '{orientacion_cerr1} == 8'   =>  '7'
    ),

    'temp_ext_norte_cerr2'=>'{temp_ext}',
    'temp_ext_sur_cerr2'    =>  array(
            '{color_cerr2} == 1'    =>  '{temp_ext}+2',
            '{color_cerr2} == 2 || {color_cerr2} == 3'   =>  '{temp_ext}+3'
    ),
    'temp_ext_este_cerr2'    =>  array(
            '{color_cerr2} == 1'    =>  '{temp_ext}+3',
            '{color_cerr2} == 2'   =>  '{temp_ext}+4',
            '{color_cerr2} == 3'   =>  '{temp_ext}+5'
    ),
    'temp_ext_oeste_cerr2'    =>  array(
            '{color_cerr2} == 1'    =>  '{temp_ext}+3',
            '{color_cerr2} == 2'   =>  '{temp_ext}+4',
            '{color_cerr2} == 3'   =>  '{temp_ext}+5'
    ),
    'temp_ext_cerr2'    =>  array(
            '{tipo_cerr2} != 1 && {orientacion_cerr2} == 1'   =>  '{temp_ext_norte_cerr2}',
            '{tipo_cerr2} != 1 && {orientacion_cerr2} == 2'   =>  '{temp_ext_sur_cerr2}',
            '{tipo_cerr2} != 1 && {orientacion_cerr2} == 3'   =>  '{temp_ext_este_cerr2}',
            '{tipo_cerr2} != 1 && {orientacion_cerr2} == 4'   =>  '{temp_ext_oeste_cerr2}',
            '{tipo_cerr2} != 1 && {orientacion_cerr2} == 5'   =>  '({temp_ext_norte_cerr2}+{temp_ext_este_cerr2})/2',
            '{tipo_cerr2} != 1 && {orientacion_cerr2} == 6'   =>  '({temp_ext_norte_cerr2}+{temp_ext_oeste_cerr2})/2',
            '{tipo_cerr2} != 1 && {orientacion_cerr2} == 7'   =>  '({temp_ext_sur_cerr2}+{temp_ext_este_cerr2})/2',
            '{tipo_cerr2} != 1 && {orientacion_cerr2} == 8'   =>  '({temp_ext_sur_cerr2}+{temp_ext_oeste_cerr2})/2'
    ),
    'temp_ext_calc_cerr2'    =>  array(
            '{tipo_cerr2} == 1 || {tipo_ubicacion} == 1 || {cerr_ext} == 2'    =>  '{temp_recinto}',
            '{tipo_cerr2} == 2 && {tipo_ubicacion} == 2 && {cerr_ext} == 1'    =>  '{temp_ext_cerr2}',
            '{tipo_cerr2} == 3 && {tipo_ubicacion} == 2 && {cerr_ext} == 1'    =>  '{temp_ext_cerr2}-7',
            '{tipo_cerr2} == 4 && {tipo_ubicacion} == 2 && {cerr_ext} == 1'    =>  '{temp_ext_cerr2}-3'
    ),
    'tipo_cerr3'=>'0',
    'color_cerr3'=>'0',
    'orientacion_cerr3'    =>  array(
            '{orientacion_cerr1} == 1'    =>  '2',
            '{orientacion_cerr1} == 2'   =>  '1',
            '{orientacion_cerr1} == 3'   =>  '4',
            '{orientacion_cerr1} == 4'   =>  '3',
            '{orientacion_cerr1} == 5'   =>  '8',
            '{orientacion_cerr1} == 6'   =>  '7',
            '{orientacion_cerr1} == 7'   =>  '6',
            '{orientacion_cerr1} == 8'   =>  '5'
    ),

    'temp_ext_norte_cerr3'=>'{temp_ext}',
    'temp_ext_sur_cerr3'    =>  array(
            '{color_cerr3} == 1'    =>  '{temp_ext}+2',
            '{color_cerr3} == 2 || {color_cerr3} == 3'   =>  '{temp_ext}+3'
    ),
    'temp_ext_este_cerr3'    =>  array(
            '{color_cerr3} == 1'    =>  '{temp_ext}+3',
            '{color_cerr3} == 2'   =>  '{temp_ext}+4',
            '{color_cerr3} == 3'   =>  '{temp_ext}+5'
    ),
    'temp_ext_oeste_cerr3'    =>  array(
            '{color_cerr3} == 1'    =>  '{temp_ext}+3',
            '{color_cerr3} == 2'   =>  '{temp_ext}+4',
            '{color_cerr3} == 3'   =>  '{temp_ext}+5'
    ),
    'temp_ext_cerr3'    =>  array(
            '{tipo_cerr3} != 1 && {orientacion_cerr3} == 1'   =>  '{temp_ext_norte_cerr3}',
            '{tipo_cerr3} != 1 && {orientacion_cerr3} == 2'   =>  '{temp_ext_sur_cerr3}',
            '{tipo_cerr3} != 1 && {orientacion_cerr3} == 3'   =>  '{temp_ext_este_cerr3}',
            '{tipo_cerr3} != 1 && {orientacion_cerr3} == 4'   =>  '{temp_ext_oeste_cerr3}',
            '{tipo_cerr3} != 1 && {orientacion_cerr3} == 5'   =>  '({temp_ext_norte_cerr3}+{temp_ext_este_cerr3})/2',
            '{tipo_cerr3} != 1 && {orientacion_cerr3} == 6'   =>  '({temp_ext_norte_cerr3}+{temp_ext_oeste_cerr3})/2',
            '{tipo_cerr3} != 1 && {orientacion_cerr3} == 7'   =>  '({temp_ext_sur_cerr3}+{temp_ext_este_cerr3})/2',
            '{tipo_cerr3} != 1 && {orientacion_cerr3} == 8'   =>  '({temp_ext_sur_cerr3}+{temp_ext_oeste_cerr3})/2'
    ),
    'temp_ext_calc_cerr3'    =>  array(
            '{tipo_cerr3} == 1 || {tipo_ubicacion} == 1 || {cerr_ext} == 2'    =>  '{temp_recinto}',
            '{tipo_cerr3} == 2 && {tipo_ubicacion} == 2 && {cerr_ext} == 1'    =>  '{temp_ext_cerr3}',
            '{tipo_cerr3} == 3 && {tipo_ubicacion} == 2 && {cerr_ext} == 1'    =>  '{temp_ext_cerr3}-7',
            '{tipo_cerr3} == 4 && {tipo_ubicacion} == 2 && {cerr_ext} == 1'    =>  '{temp_ext_cerr3}-3'
    ),
    'tipo_cerr4'=>'0',
    'color_cerr4'=>'0',

    'orientacion_cerr4'    =>  array(
            '{orientacion_cerr1} == 1'    =>  '3',
            '{orientacion_cerr1} == 2'   =>  '4',
            '{orientacion_cerr1} == 3'   =>  '2',
            '{orientacion_cerr1} == 4'   =>  '1',
            '{orientacion_cerr1} == 5'   =>  '7',
            '{orientacion_cerr1} == 6'   =>  '5',
            '{orientacion_cerr1} == 7'   =>  '8',
            '{orientacion_cerr1} == 8'   =>  '6'
    ),

    'temp_ext_norte_cerr4'=>'{temp_ext}',
    'temp_ext_sur_cerr4'    =>  array(
            '{color_cerr4} == 1'    =>  '{temp_ext}+2',
            '{color_cerr4} == 2 || {color_cerr4} == 3'   =>  '{temp_ext}+3'
    ),
    'temp_ext_este_cerr4'    =>  array(
            '{color_cerr4} == 1'    =>  '{temp_ext}+3',
            '{color_cerr4} == 2'   =>  '{temp_ext}+4',
            '{color_cerr4} == 3'   =>  '{temp_ext}+5'
    ),
    'temp_ext_oeste_cerr4'    =>  array(
            '{color_cerr4} == 1'    =>  '{temp_ext}+3',
            '{color_cerr4} == 2'   =>  '{temp_ext}+4',
            '{color_cerr4} == 3'   =>  '{temp_ext}+5'
    ),
    'temp_ext_cerr4'    =>  array(
            '{tipo_cerr4} != 1 && {orientacion_cerr4} == 1'   =>  '{temp_ext_norte_cerr4}',
            '{tipo_cerr4} != 1 && {orientacion_cerr4} == 2'   =>  '{temp_ext_sur_cerr4}',
            '{tipo_cerr4} != 1 && {orientacion_cerr4} == 3'   =>  '{temp_ext_este_cerr4}',
            '{tipo_cerr4} != 1 && {orientacion_cerr4} == 4'   =>  '{temp_ext_oeste_cerr4}',
            '{tipo_cerr4} != 1 && {orientacion_cerr4} == 5'   =>  '({temp_ext_norte_cerr4}+{temp_ext_este_cerr4})/2',
            '{tipo_cerr4} != 1 && {orientacion_cerr4} == 6'   =>  '({temp_ext_norte_cerr4}+{temp_ext_oeste_cerr4})/2',
            '{tipo_cerr4} != 1 && {orientacion_cerr4} == 7'   =>  '({temp_ext_sur_cerr4}+{temp_ext_este_cerr4})/2',
            '{tipo_cerr4} != 1 && {orientacion_cerr4} == 8'   =>  '({temp_ext_sur_cerr4}+{temp_ext_oeste_cerr4})/2'
    ),
    'temp_ext_calc_cerr4'    =>  array(
            '{tipo_cerr4} == 1 || {tipo_ubicacion} == 1 || {cerr_ext} == 2'    =>  '{temp_recinto}',
            '{tipo_cerr4} == 2 && {tipo_ubicacion} == 2 && {cerr_ext} == 1'    =>  '{temp_ext_cerr4}',
            '{tipo_cerr4} == 3 && {tipo_ubicacion} == 2 && {cerr_ext} == 1'    =>  '{temp_ext_cerr4}-7',
            '{tipo_cerr4} == 4 && {tipo_ubicacion} == 2 && {cerr_ext} == 1'    =>  '{temp_ext_cerr4}-3'
    ),
    'tipo_cubierta'=>'0',
    'color_cubierta'=>'0',
    'temp_ext_cubierta'    =>  array(
            '{color_cubierta} == 1'    =>  '{temp_ext}+5',
            '{color_cubierta} == 2'   =>  '{temp_ext}+9',
            '{color_cubierta} == 3'   =>  '{temp_ext}+11'
    ),

    'temp_ext_calc_techo'    =>  array(
        '{tipo_cubierta} == 1 || {tipo_ubicacion} == 1 || {cerr_ext} == 2'    =>  '{temp_recinto}',
        '{tipo_cubierta} == 2 && {tipo_ubicacion} == 2 && {cerr_ext} == 1'    =>  '{temp_ext_cubierta}',
        '{tipo_cubierta} == 3 && {tipo_ubicacion} == 2 && {cerr_ext} == 1'    =>  '{temp_ext_cubierta}-7',
        '{tipo_cubierta} == 4 && {tipo_ubicacion} == 2 && {cerr_ext} == 1'    =>  '{temp_ext_cubierta}-3'
    ),
    'cond_cerr1'=>'0',
    'cond_cerr2'=>'0',
    'cond_cerr3'=>'0',
    'cond_cerr4'=>'0',
    'cond_techo'=>'0',
    'cond_suelo'=>'0',
    'cond_aisla_suelo'=>'0',
    'cond_cam_aire'=>'0.75',
    'cond_puerta'=>'0',
    'cond_ventana'=>'0',
    'h_int_pared'=>'9+3.7*{vel_aire_cam}',
    'h_int_techo'=>'11+3.7*{vel_aire_cam}',
    'h_int_suelo'=>'6+3.7*{vel_aire_cam}',

    'h_ext_pared'=>'9+3.7*{vel_aire_cam}',
    'h_ext_techo'=>'11+3.7*{vel_aire_cam}',
    'espe_cerr1'=>'1000*{cond_cerr1}*(({temp_ext_calc_cerr1}-({temp_cam}))/(0.0667*{temp_cam}+9)-1/{h_int_pared}-1/{h_ext_pared})',
    'espe_cerr2'=>'1000*{cond_cerr2}*(({temp_ext_calc_cerr2}-({temp_cam}))/(0.0667*{temp_cam}+9)-1/{h_int_pared}-1/{h_ext_pared})',
    'espe_cerr3'=>'1000*{cond_cerr3}*(({temp_ext_calc_cerr3}-({temp_cam}))/(0.0667*{temp_cam}+9)-1/{h_int_pared}-1/{h_ext_pared})',
    'espe_cerr4'=>'1000*{cond_cerr4}*(({temp_ext_calc_cerr4}-({temp_cam}))/(0.0667*{temp_cam}+9)-1/{h_int_pared}-1/{h_ext_pared})',
    'espe_techo'=>'1000*{cond_techo}*(({temp_ext_calc_techo}-({temp_cam}))/(0.0667*{temp_cam}+9)-1/{h_int_techo}-1/{h_ext_techo})',
    'espe_promedio'=>'({espe_cerr1}+{espe_cerr2}+{espe_cerr3}+{espe_cerr4}+{espe_techo})/5',
    'cerr_verticales'=>'0',
    'espe_cerr1_calc'    =>  array(
            '{cerr_verticales} == 1'    =>  '{espe_promedio}',
            '{cerr_verticales} == 2'   =>  '{espe_cerr1}'
    ),
    'espe_cerr2_calc'    =>  array(
            '{cerr_verticales} == 1'    =>  '{espe_promedio}',
            '{cerr_verticales} == 2'   =>  '{espe_cerr2}'
    ),
    'espe_cerr3_calc'    =>  array(
            '{cerr_verticales} == 1'    =>  '{espe_promedio}',
            '{cerr_verticales} == 2'   =>  '{espe_cerr3}'
    ),
    'espe_cerr4_calc'    =>  array(
            '{cerr_verticales} == 1'    =>  '{espe_promedio}',
            '{cerr_verticales} == 2'   =>  '{espe_cerr4}'
    ),
    'espe_techo_calc'    =>  array(
            '{cerr_verticales} == 1'    =>  '{espe_promedio}',
            '{cerr_verticales} == 2'   =>  '{espe_techo}'
    ),
    'espe_suelo'=>'150',
    'camara_de_aire'=>'0',
    'espe_cam_aire'    =>  array(
            '{camara_de_aire} == 1'    =>  '150',
            '{camara_de_aire} == 2'   =>  '0'
    ),
    'aislamiento'=>'0',
    'espe_aisla_suelo'    =>  array(
            '{aislamiento} == 1'    =>  '1000*{cond_aisla_suelo}*(({temp_suelo}-({temp_cam}))/(0.0667*{temp_cam}+9)-1/{h_int_suelo}-({espe_suelo}/1000)/{cond_suelo}-({espe_cam_aire}/1000)/{cond_cam_aire})',
            '{aislamiento} == 2'   =>  '0'
    ),
    'espe_puerta'=>'1000*{cond_puerta}*(({temp_ext_calc_cerr1}-({temp_cam}))/(0.0667*{temp_cam}+9)-1/{h_int_pared}-1/{h_ext_pared})',
    'material_ventanas'=>'0',
    'espe_ventana'    =>  array(
            '{material_ventanas} == 23'    =>  '6',
            '{material_ventanas} == 24'   =>  '20'
    ),
    'sup_puerta'=>'3',
    'ventanas'=>'0',
    'sup_ventana'    =>  array(
            '{ventanas} == 1'    =>  '2',
            '{ventanas} == 2'   =>  '0'
    ),
    'num_personas'    =>  array(
            '{idCamara} == 5'    =>  '{long_cam}*{ancho_cam}/5',
            '{idCamara} != 5'   =>  '1'
    ),
    'pot_persona'=>'-4.4286*{temp_cam}+332.86',
    'pot_pers_sens'=>'-6.4571*{temp_cam}+251.71',
    'pot_pers_lat'=>'2.0285*{temp_cam}+81.15',

    'tiempo_personas'    =>  array(
            '{idCamara} == 5'    =>  '12',
            '{idCamara} == 1 || {idCamara} >= 6'   =>  '1',
            '{idCamara} == 2'   =>  '0.5'
    ),

    'num_lamp'=>'20*{long_cam}*{ancho_cam}/15',
    'pot_lamp'=>'15',
    'tiempo_lamp'=>'{tiempo_personas}',
    'pot_m2_ilum'=>'20',
    'pot_vent'    =>  array(
            '{tipoEvap} == 1'    =>  '0',
            '{tipoEvap} == 2 && {volumen_cam} <= 100 && ({idCamara} == 1 || {idCamara} == 2 || {idCamara} == 5 || {idCamara} >= 6)'   =>  '10.45*{volumen_cam}+650',
            '{tipoEvap} == 2 && {volumen_cam} > 100 && ({idCamara} == 1 || {idCamara} == 2 || {idCamara} == 5 || {idCamara} >= 6)'   =>  '3.6679*{volumen_cam}+2331.6',
            '{tipoEvap} == 2 && {idCamara} == 3'   =>  '3.2434*{rend_tunel}+734.26',
            '{tipoEvap} == 2 && {idCamara} == 4'   =>  '18.706*{rend_tunel}+1570.1'
    ),
    'tiempo_vent'    =>  array(
            '{idCamara} == 5'   =>  '22',
            '{idCamara} == 1 || {idCamara} >= 6'   =>  '21',
            '{idCamara} == 2'   =>  '20'
    ),
    'pot_vent_estima'    =>  array(
            '{tipoEvap} == 1'    =>  '0',
            '{tipoEvap} == 2 && ({idCamara} == 1 || {idCamara} == 2 || {idCamara} == 5 || {idCamara} >= 6)'   =>  '10',
            '{tipoEvap} == 2 && {idCamara} == 3'   =>  '15',
            '{tipoEvap} == 2 && {idCamara} == 4'   =>  '20'
    ),
    'pot_resist'    =>  array(
            '{volumen_cam} <= 200 && ({idCamara} == 1 || {idCamara} == 6 || {idCamara} == 7)'    =>  '110*{volumen_cam}',
            '{volumen_cam} > 200 && ({idCamara} == 1 || {idCamara} == 6 || {idCamara} == 7)'   =>  '55.035*{volumen_cam}+11007',
            '{volumen_cam} <= 300 && {idCamara} == 2'   =>  '42.515*{volumen_cam}+1149.7',
            '{volumen_cam} > 300 && {idCamara} == 2'   =>  '21.485*{volumen_cam}+8537.5',
            '{idCamara} == 3'   =>  '29.859*{rend_tunel}+2795.9',
            '{idCamara} == 4'   =>  '145.54*{rend_tunel}+4941.2',
            '{idCamara} == 5 || {idCamara} == 8 || {idCamara} == 9 || {idCamara} == 10'   =>  '0'
    ),
    'tiempo_resist'    =>  array(
            '{idCamara} == 5'    =>  '0',
            '{idCamara} == 1'   =>  '1',
            '{idCamara} == 2'   =>  '2'
    ),
    'pot_resist_estima'    =>  array(
            '{idCamara} == 1'    =>  '5',
            '{idCamara} == 2'   =>  '15',
            '{idCamara} == 5'   =>  '0'
    ),
    'pot_otros'=>'0',
    'tiempo_otros'=>'0',
    'pot_otros_estima'=>'0',
    'horas_dia'    =>  array(
            '{idCamara} == 3 || {idCamara} == 4'    =>  '23',
            '{idCamara} == 5'   =>  '22',
            '{idCamara} == 1 || {idCamara} >= 6'   =>  '21',
            '{idCamara} == 2'   =>  '20'
    ),
    'mayor_segur'    =>  array(
            '{volumen_cam} <= 500'    =>  '10',
            '{volumen_cam} > 500'   =>  '5'
    ),

    'calculoA'=>'(1/(1/{h_int_pared}+1/{h_ext_pared}+({espe_cerr1_calc}/1000)/{cond_cerr1}))*({long_cam}*{alto_cam})*({temp_ext_calc_cerr1}-({temp_cam}))+(1/(1/{h_int_pared}+1/{h_ext_pared}+({espe_cerr2_calc}/1000)/{cond_cerr2}))*({ancho_cam}*{alto_cam})*({temp_ext_calc_cerr2}-({temp_cam}))+(1/(1/{h_int_pared}+1/{h_ext_pared}+({espe_cerr3_calc}/1000)/{cond_cerr3}))*({long_cam}*{alto_cam})*({temp_ext_calc_cerr3}-({temp_cam}))+(1/(1/{h_int_pared}+1/{h_ext_pared}+({espe_cerr4_calc}/1000)/{cond_cerr4}))*({ancho_cam}*{alto_cam})*({temp_ext_calc_cerr4}-({temp_cam}))+(1/(1/{h_int_techo}+1/{h_ext_techo}+({espe_techo_calc}/1000)/{cond_techo}))*({long_cam}*{ancho_cam})*({temp_ext_calc_techo}-({temp_cam}))+(1/(1/{h_int_suelo}+({espe_suelo}/1000)/{cond_suelo}+({espe_aisla_suelo}/1000)/{cond_aisla_suelo}+({espe_cam_aire}/1000)/{cond_cam_aire}))*({long_cam}*{ancho_cam})*({temp_suelo}-({temp_cam}))+(1/(1/{h_int_pared}+1/{h_ext_pared}+({espe_puerta}/1000)/{cond_puerta}))*({sup_puerta})*({temp_recinto}-({temp_cam}))-(1/(1/{h_int_pared}+1/{h_ext_pared}+({espe_cerr1_calc}/1000)/{cond_cerr1}))*({sup_puerta})*({temp_ext_calc_cerr1}-({temp_cam}))+(1/(1/{h_int_pared}+1/{h_ext_pared}+({espe_ventana}/1000)/{cond_ventana}))*({sup_ventana})*({temp_ext_calc_cerr1}-({temp_cam}))-(1/(1/{h_int_pared}+1/{h_ext_pared}+({espe_cerr1_calc}/1000)/{cond_cerr1}))*({sup_ventana})*({temp_ext_calc_cerr1}-({temp_cam}))',
    'calculoB'=>'{renov_infiltra}*{volumen_cam}*({entalpia_ext}-({entalpia_cam}))/({volumen_espe_cam}*24*3600)',
    'calculoB1'=>'{renov_infiltra}*{volumen_cam}*({entalpia_ext_sens}-({entalpia_cam_sens}))/({volumen_espe_cam}*24*3600)',
    'calculoB2'=>'{renov_infiltra}*{volumen_cam}*({entalpia_ext_lat}-({entalpia_cam_lat}))/({volumen_espe_cam}*24*3600)',
    'calculoC'=>'{renov_aire_calc}*{volumen_cam}*({entalpia_ext}-({entalpia_cam}))/({volumen_espe_cam}*24*3600)',
    'calculoC1'=>'{renov_aire_calc}*{volumen_cam}*({entalpia_ext_sens}-({entalpia_cam_sens}))/({volumen_espe_cam}*24*3600)',
    'calculoC2'=>'{renov_aire_calc}*{volumen_cam}*({entalpia_ext_lat}-({entalpia_cam_lat}))/({volumen_espe_cam}*24*3600)',

    'calculoD'    =>  array(
            '{idCamara} == 1'    =>  '(1+{mayor_embalaje}/100)*{capacidad_cam}*({rotacion}/100)*{calor_esp_fres}*({temp_inicial}-({temp_final}))/(24*3600)',
            '{idCamara} == 2'    =>  '(1+{mayor_embalaje}/100)*{capacidad_cam}*({rotacion}/100)*{calor_esp_cong_calc}*({temp_inicial}-({temp_final}))/(24*3600)',
            '{idCamara} == 3'   =>  '(1+{mayor_embalaje}/100)*{rend_tunel}*{calor_esp_fres}*({temp_inicial}-({temp_final}))/3600',
            '{idCamara} == 4'   =>  '(1+{mayor_embalaje}/100)*{rend_tunel}*({calor_esp_fres}*({temp_inicial}-({temp_cong}))+{calor_lat_cong}+{calor_esp_cong_calc}*({temp_cong}-({temp_final})))/3600',
            '{idCamara} == 5'    =>  '(1+{mayor_embalaje}/100)*{capacidad_cam}*({rotacion}/100)*{calor_esp_fres}*({temp_inicial}-({temp_final}))/(24*3600)',
            '{idCamara} >= 6'    =>  '(1+{mayor_embalaje}/100)*{capacidad_cam}*{calor_esp_fres}*({temp_inicial}-({temp_final}))/({duracion_secado}*24*3600)+1000*{capacidad_cam}*({merma_producto}/100)*(2501-2.38*{temp_cam})/({duracion_secado}*24*3600)',
    ),
    'calculoD2'    =>  array(
            '{idCamara} < 6'    =>  '0',
            '{idCamara} >= 6'    =>  '1000*{capacidad_cam}*({merma_producto}/100)*(2501-2.38*{temp_cam})/({duracion_secado}*24*3600)',
    ),
    'calculoD1'=>'{calculoD}- ({calculoD2})',

    'calculoE'    =>  array(
            '{idCamara} == 1 || {idCamara} == 2 || {idCamara} == 5 || {idCamara} >= 6'    =>  '({capacidad_cam}*{rotacion}/100)*{pot_respi_tmedia}+({capacidad_cam}-({capacidad_cam})*{rotacion}/100)*{pot_respi_tfinal}',
            '{idCamara} == 3'   =>  '{rend_tunel}*({tiempo_enf}/60)*{pot_respi_tmedia}',
            '{idCamara} == 4'   =>  '{rend_tunel}*({tiempo_cong}/60)*{pot_respi_tmedia}'
    ),

    'calculoF'=>'{pot_persona}*{num_personas}*{tiempo_personas}/24',
    'calculoF1'=>'{pot_pers_sens}*{num_personas}*{tiempo_personas}/24',
    'calculoF2'=>'{pot_pers_lat}*{num_personas}*{tiempo_personas}/24',

    'tipoNumPot'=>'0',
    'tipoLamp'=>'0',
    'calculoG'    =>  array(
            '{tipoNumPot} == 1 && {tipoLamp} == 1'    =>  '{num_lamp}*{pot_lamp}*{tiempo_lamp}/24',
            '{tipoNumPot} == 1 && {tipoLamp} == 2'   =>  '{num_lamp}*{pot_lamp}*1.25*{tiempo_lamp}/24',
            '{tipoNumPot} == 2'   =>  '{pot_m2_ilum}*{tiempo_lamp}*{ancho_cam}*{long_cam}/24'
    ),

    'potVentEst'=>'0',
    'calculoH'    =>  array(
            '{tipoEvap} == 2 && {potVentEst} == 1'    =>  '{pot_vent}*{tiempo_vent}/24',
            '{tipoEvap} == 2 && {potVentEst} == 2'   =>  '({pot_vent_estima}/100)*({calculoA}+{calculoB}+{calculoC}+{calculoD})'
    ),

    'potResisEst'=>'0',
    'calculoI'    =>  array(
            '{potResisEst} == 1'    =>  '{pot_resist}*{tiempo_resist}/24',
            '{potResisEst} == 2'   =>  '({pot_resist_estima}/100)*({calculoA}+{calculoB}+{calculoC}+{calculoD})'
    ),

    'potConEst'=>'0',
    'calculoJ'    =>  array(
            '{potConEst} == 1'    =>  '{pot_otros}*{tiempo_otros}/24',
            '{potConEst} == 2'   =>  '({pot_otros_estima}/100)*({calculoA}+{calculoB}+{calculoC}+{calculoD})'
    ),
    'carga_total'=>'{calculoA}+{calculoB}+{calculoC}+{calculoD}+{calculoE}+{calculoF}+{calculoG}+{calculoH}+{calculoI}+{calculoJ}',
    'carga_sens'=>'{calculoA}+{calculoB1}+{calculoC1}+{calculoD1}+{calculoE}+{calculoF1}+{calculoG}+{calculoH}+{calculoI}+{calculoJ}',
    'carga_lat'=>'{carga_total} - {carga_sens}',
    'porcentaje_calculoA'=>'({calculoA}/ {carga_total})* 100',
    'porcentaje_calculoB'=>'({calculoB}/ {carga_total})* 100',
    'porcentaje_calculoC'=>'({calculoC}/ {carga_total})* 100',
    'porcentaje_calculoD'=>'({calculoD}/ {carga_total})* 100',
    'porcentaje_calculoE'=>'({calculoE}/ {carga_total})* 100',
    'porcentaje_calculoF'=>'({calculoF}/ {carga_total})* 100',
    'porcentaje_calculoG'=>'({calculoG}/ {carga_total})* 100',
    'porcentaje_calculoH'=>'({calculoH}/ {carga_total})* 100',
    'porcentaje_calculoI'=>'({calculoI}/ {carga_total})* 100',
    'porcentaje_calculoJ'=>'({calculoJ}/ {carga_total})* 100',
    'porcentaje_carga_sens'=>'({carga_sens}/ {carga_total})* 100',
    'porcentaje_carga_lat'=>'({carga_lat}/ {carga_total})* 100',
    'porcen_carga_sens'=>'{carga_sens}/{carga_total}',
    'porcen_carga_lat'=>'{carga_lat}/{carga_total}',
    'pot_frig_cam'=>'{carga_sens}*(24/{horas_dia})*(1+{mayor_segur}/100)+{carga_lat}*(24/{horas_dia})*(1+{mayor_segur}/100)',
    'pot_frig_cam_sens'=>'{pot_frig_cam}*{porcen_carga_sens}',
    'pot_frig_cam_lat'=>'{pot_frig_cam}*{porcen_carga_lat}',
);

${NAME_ARRAY_FORMULAS_TUNEL} = array(
    'temp_ext'=>'0',
    'hum_ext'=>'0',
    'temp_suelo'=>'({temp_ext}+15)/2',
    'altitud'=>'0',
    'vel_viento'=>'0',
    'temp_cam'=>'0',
    'hum_cam'=>'0',
    'idCamara'=>'0',
    'presion'=>'101325*pow((1-0.0065/(15+273.15)*{altitud}),(9.807/0.2883419/0.0065/1000))',
    'p_sat_agua_cam'    =>  array(
            '{temp_cam} < 0'    =>  'exp(-5674.5359*pow(({temp_cam}+273.15),-1)+6.3925247-0.009677843*({temp_cam}+273.15)+0.00000062215701*pow(({temp_cam}+273.15),2)+0.0000000020747825*pow(({temp_cam}+273.15),3)-0.0000000000009484024*pow(({temp_cam}+273.15),4)+4.1635019*log({temp_cam}+273.15))',
            '{temp_cam} >= 0'   =>  'exp(-5800.2206*pow(({temp_cam}+273.15),-1)+1.3914993-0.048640239*({temp_cam}+273.15)+0.000041764768*pow(({temp_cam}+273.15),2)-0.000000014452093*pow(({temp_cam}+273.15),3)+6.5459673*log({temp_cam}+273.15))'
    ),
    'hum_absol_cam'=>'1000*0.62198*({hum_cam}/100)*{p_sat_agua_cam}/({presion}-({hum_cam}/100)*{p_sat_agua_cam})',
    'p_parcial_agua_cam'=>'(({presion}/1000)*{hum_absol_cam}/1000)/(0.2883419/0.46191511+{hum_absol_cam}/1000)',
    'temp_rocio_cam'    =>  array(
            '{temp_cam} < 0'    =>  '5.994+12.41*log({p_parcial_agua_cam})+0.4273*pow(log({p_parcial_agua_cam}),2)',
            '{temp_cam} >= 0'   =>  '6.983+14.38*log({p_parcial_agua_cam})+1.079*pow(log({p_parcial_agua_cam}),2)'
    ),
    'entalpia_cam'=>'(1.006*{temp_cam}+({hum_absol_cam}/1000)*(2501.3+1.89*{temp_cam}))*1000',
    'entalpia_cam_sens'=>'(1.006*{temp_cam}+({hum_absol_cam}/1000)*(1.89*{temp_cam}))*1000',
    'entalpia_cam_lat'=>'({hum_absol_cam}/1000)*(2501.3)*1000',
    'volumen_espe_cam'=>'0.2870551882*({temp_cam}+273.15)*(1+1.6078*{hum_absol_cam}/1000)/101.32503',
    'dt_vent'=>'-0.4267*{hum_cam}+42.502',
    'dt_est'=>'-0.472*{hum_cam}+49.815',

    'tipoEvap'=>'0',
    'temp_evap'    =>  array(
            '{tipoEvap} == 2'    =>  '{temp_cam}-({dt_vent})',
            '{tipoEvap} == 1'   =>  '{temp_cam}-({dt_est})'
    ),
    'p_sat_agua_evap'    =>  array(
            '{temp_evap} < 0'    =>  'exp(-5674.5359*pow(({temp_evap}+273.15),-1)+6.3925247-0.009677843*({temp_evap}+273.15)+0.00000062215701*pow(({temp_evap}+273.15),2)+0.0000000020747825*pow(({temp_evap}+273.15),3)-0.0000000000009484024*pow(({temp_evap}+273.15),4)+4.1635019*log({temp_evap}+273.15))',
            '{temp_evap} >= 0'   =>  'exp(-5800.2206*pow(({temp_evap}+273.15),-1)+1.3914993-0.048640239*({temp_evap}+273.15)+0.000041764768*pow(({temp_evap}+273.15),2)-0.000000014452093*pow(({temp_evap}+273.15),3)+6.5459673*log({temp_evap}+273.15))'
    ),
    'hum_absol_evap'=>'1000*0.62198*(100/100)*{p_sat_agua_evap}/({presion}-(100/100)*{p_sat_agua_evap})',
    'p_parcial_agua_evap'=>'(({presion}/1000)*{hum_absol_evap}/1000)/(0.2883419/0.46191511+{hum_absol_evap}/1000)',
    'temp_rocio_evap'    =>  array(
            '{temp_evap} < 0'    =>  '5.994+12.41*log({p_parcial_agua_evap})+0.4273*pow(log({p_parcial_agua_evap}),2)',
            '{temp_evap} >= 0'   =>  '6.983+14.38*log({p_parcial_agua_evap})+1.079*pow(log({p_parcial_agua_evap}),2)'
    ),
    'entalpia_evap'=>'(1.006*{temp_evap}+({hum_absol_evap}/1000)*(2501.3+1.89*{temp_evap}))*1000',
    'entalpia_evap_sens'=>'(1.006*{temp_evap}+({hum_absol_evap}/1000)*(1.89*{temp_evap}))*1000',
    'entalpia_evap_lat'=>'({hum_absol_evap}/1000)*(2501.3)*1000',
    'volumen_espe_evap'=>'0.2870551882*({temp_evap}+273.15)*(1+1.6078*{hum_absol_evap}/1000)/101.32503',
    'd_entalpia_evap_sens'=>'({entalpia_cam_sens}-({entalpia_evap_sens}))/({entalpia_cam}-({entalpia_evap}))',
    'd_entalpia_evap_lat'=>'({entalpia_cam_lat}-({entalpia_evap_lat}))/({entalpia_cam}-({entalpia_evap}))',
    'vel_aire_cam'    =>  array(
            '{idCamara} == 1 || {idCamara} == 2 || {idCamara} == 5'    =>  '1',
            '{idCamara} == 3 || {idCamara} == 4'   =>  '7',
            '{idCamara} >= 6'   =>  '0.5'
    ),
    'h_cam'=>'9+3.7*{vel_aire_cam}',

    'categoriaProducto'=>'0',
    'idProducto'=>'0',
    'temp_inicial'    =>  array(
            '{idCamara} == 3 && {categoriaProducto} == 2'    =>  '15',
            '{idCamara} == 3 && {idProducto} == 612'   =>  '90',
            '{idCamara} == 3 && {idProducto} != 612 && ({categoriaProducto}== 1 || {categoriaProducto}== 6 || {categoriaProducto}== 5 || {categoriaProducto}== 7)'   =>  '25',
            '{idCamara} == 3 && {idProducto} != 612 && ({categoriaProducto}== 3 || {categoriaProducto}== 4)'   =>  '35',
            '{idCamara} == 4'   =>  '15'
    ),
    'temp_final'    =>  array(
            '{idCamara} == 3'    =>  '5',
            '{idCamara} == 4'   =>  '-10'
    ),
    'agua'=>'0',
    'proteinas'=>'0',
    'grasas'=>'0',
    'carbohidratos'=>'0',
    'fibras'=>'0',
    'cenizas'=>'0',
    'temp_cong'=>'0',
    'calor_esp_fres'=>'(({agua}/100)*(4.217-0.000090864*{temp_inicial}+0.0000054731*pow({temp_inicial},2))+({proteinas}/100)*(2.0082+0.0012089*{temp_inicial}-0.0000013129*pow({temp_inicial},2))+({grasas}/100)*(1.9842+0.0014733*{temp_inicial}-0.0000048008*pow({temp_inicial},2))+({carbohidratos}/100)*(1.5488+0.0019625*{temp_inicial}-0.0000059399*pow({temp_inicial},2))+({fibras}/100)*(1.8459+0.0018306*{temp_inicial}-0.0000046509*pow({temp_inicial},2))+({cenizas}/100)*(1.0926+0.0018896*{temp_inicial}-0.0000036817*pow({temp_inicial},2)))*1000',
    'calor_esp_cong'=>'0',
    'calor_esp_cong_calc'=>'{calor_esp_cong}*1000+0.0031*(40+{temp_final})',
    'conduct_fres'=>'({agua}/100)*(0.57109+0.0017625*{temp_inicial}-0.0000067036*pow({temp_inicial},2))+({proteinas}/100)*(0.17881+0.0011958*{temp_inicial}-0.0000027178*pow({temp_inicial},2))+({grasas}/100)*(0.18071+0.00027604*{temp_inicial}-0.00000017749*pow({temp_inicial},2))+({carbohidratos}/100)*(0.20141+0.0013874*{temp_inicial}-0.0000043312*pow({temp_inicial},2))+({fibras}/100)*(0.18331+0.0012497*{temp_inicial}-0.0000031683*pow({temp_inicial},2))+({cenizas}/100)*(0.32962+0.0014011*{temp_inicial}-0.0000029069*pow({temp_inicial},2))',
    'conduct_cong'=>'(1.105*({agua}/100)/(1+0.7138/log({temp_cong}-({temp_final})+1)))*(2.2196-0.0062489*{temp_final}+0.00010154*pow({temp_final},2))+({agua}/100-1.105*({agua}/100)/(1+0.7138/log({temp_cong}-({temp_final})+1)))*(0.57109+0.0017625*{temp_final}-0.0000067036*pow({temp_final},2))+({proteinas}/100)*(0.17881+0.0011958*{temp_final}-0.0000027178*pow({temp_final},2))+({grasas}/100)*(0.18071+0.00027604*{temp_final}-0.00000017749*pow({temp_final},2))+({carbohidratos}/100)*(0.20141+0.0013874*{temp_final}-0.0000043312*pow({temp_final},2))+({fibras}/100)*(0.18331+0.0012497*{temp_final}-0.0000031683*pow({temp_final},2))+({cenizas}/100)*(0.32962+0.0014011*{temp_final}-0.0000029069*pow({temp_final},2))',
    'densi_fres'=>'({agua}/100)*(997.18+0.0031439*{temp_inicial}-0.0037574*pow({temp_inicial},2))+({proteinas}/100)*(1329.9-0.5184*{temp_inicial})+({grasas}/100)*(925.59-0.41757*{temp_inicial})+({carbohidratos}/100)*(1599.1-0.3104*{temp_inicial})+({fibras}/100)*(1311.5-0.36589*{temp_inicial})+({cenizas}/100)*(2423.8-0.28063*{temp_inicial})',
    'densi_cong'=>'(1.105*({agua}/100)/(1+0.7138/log({temp_cong}-({temp_final})+1)))*(916.89-0.13071*{temp_final})+({agua}/100-1.105*({agua}/100)/(1+0.7138/log({temp_cong}-({temp_final})+1)))*(997.18+0.0031439*{temp_final}-0.0037574*pow({temp_final},2))+({proteinas}/100)*(1329.9-0.5184*{temp_final})+({grasas}/100)*(925.59-0.41757*{temp_final})+({carbohidratos}/100)*(1599.1-0.31046*{temp_final})+({fibras}/100)*(1311.5-0.36589*{temp_final})+({cenizas}/100)*(2423.8-0.28063*{temp_final})',
    'calor_lat_cong'=>'333600*{agua}/100+54000*{grasas}/100',

    'A'=>'0',

    'B'=>'0',
    'pot_respi_tfinal'    =>  array(
            '{A} == 0 && {B} == 0'    =>  '0',
            '{A} != 0 || {B} != 0'   =>  '(exp({A}+{B}*{temp_final}))/1000'
    ),
    'pot_respi_tmedia'    =>  array(
            '{A} == 0 && {B} == 0'    =>  '0',
            '{A} != 0 || {B} != 0'   =>  '(exp({A}+{B}*({temp_inicial}+{temp_final})/2))/1000'
    ),
    'renov_aire'=>'0',
    'diam_esfera'=>'0',
    'diam_cilindro'=>'0',
    'long_cilindro'=>'0',
    'diam_rodaja'=>'0',
    'espe_rodaja'=>'0',
    'espe_filete'=>'0',
    'long_prisma'=>'0',
    'ancho_prisma'=>'0',
    'espe_prisma'=>'0',
    'long_oval'=>'0',
    'ancho_oval'=>'0',
    'espe_oval'=>'0',

    'forma'=>'0',
    'long_cam'=>'0',
    'ancho_cam'=>'0',
    'alto_cam'=>'0',
    'volumen_cam'=>'{long_cam}*{ancho_cam}*{alto_cam}',
    'densi_almacen'=>'0',
    'capacidad_cam'    =>  array(
            '{idCamara} != 3 && {idCamara} != 4 && {idCamara} != 5'    =>  '{volumen_cam}*{densi_almacen}',
            '{idCamara} == 5'   =>  '{volumen_cam}*(-5.03*log({volumen_cam})+72.127)',
            '{idCamara} == 3 || {idCamara} == 4'   =>  '80*{long_cam}*{ancho_cam}'
    ),
    'dhv'=>'{calor_lat_cong}*{densi_fres}+{calor_esp_cong_calc}*({temp_cong}+10)*{densi_cong}',
    'ste'=>'{calor_esp_cong_calc}*{densi_cong}*({temp_cong}-({temp_cam}))/{dhv}',
    'pk'=>'{calor_esp_fres}*{densi_fres}*({temp_inicial}-({temp_cong}))/{dhv}',
    'd'    =>  array(
            '{forma} == 1'    =>  '{diam_esfera}',
            '{forma} == 2'   =>  '{diam_cilindro}',
            '{forma} == 3'   =>  '{espe_rodaja}',
            '{forma} == 4'   =>  '{espe_filete}',
            '{forma} == 5'   =>  '{espe_prisma}',
            '{forma} == 6'   =>  '{espe_oval}/2'
    ),
    'bi'=>'{h_cam}*{d}/(1000*{conduct_cong})',
    'p'=>'0.5072+0.2018*{pk}+{ste}*(0.3224*{pk}+0.0105/{bi}+0.0681)',
    'r'=>'0.1684+{ste}*(0.2740*{pk}-0.0135)',
    'b1'    =>  array(
            '{forma} == 2'    =>  '1',
            '{forma} == 3'   =>  '{diam_rodaja}/{espe_rodaja}',
            '{forma} == 5'   =>  '{ancho_prisma}/{espe_prisma}',
            '{forma} == 6'   =>  '{ancho_oval}/{espe_oval}'
    ),
    'b2'    =>  array(
            '{forma} == 2'    =>  '{long_cilindro}/{diam_cilindro}',
            '{forma} == 3'   =>  '1',
            '{forma} == 5'   =>  '{long_prisma}/{espe_prisma}',
            '{forma} == 6'   =>  '{long_oval}/{espe_oval}'
    ),
    'x1'=>'2.32/(pow({b1},1.77))',
    'x2'=>'2.32/(pow({b2},1.77))',
    'x_1'=>'{x1}/(pow({bi},1.34)+{x1})',
    'x_2'=>'{x2}/(pow({bi},1.34)+{x2})',
    'e1'=>'{x_1}*(1/{b1})+(1-({x_1}))*(0.73/(pow({b1},2.5)))',
    'e2'=>'{x_2}*(1/{b2})+(1-({x_2}))*(0.5/(pow({b2},3.69)))',
    'g1'    =>  array(
            '{forma} == 2'    =>  '2',
            '{forma} == 3 || {forma} == 5'   =>  '1'
    ),
    'g2'    =>  array(
            '{forma} == 2'    =>  '0',
            '{forma} == 3'   =>  '2',
            '{forma} == 5'   =>  '1'
    ),
    'g3'    =>  array(
            '{forma} == 2 || {forma} == 5'    =>  '1',
            '{forma} == 3'   =>  '0'
    ),
    'ehtd'    =>  array(
            '{forma} == 1'    =>  '3',
            '{forma} == 4'   =>  '1',
            '{forma} == 2 || {forma} == 3 || {forma} == 5'   =>  '{g1}+{g2}*{e1}+{g3}*{e2}',
            '{forma} == 6'   =>  '1+(1+2/{bi})/(pow({b1},2)+2*{b1}/{bi})+(1+2/{bi})/(pow({b2},2)+2*{b2}/{bi})'
    ),
    'dif'=>'{conduct_fres}/({densi_fres}*{calor_esp_fres})',
    's1'    =>  array(
            '{forma} == 1'    =>  '3.14159*pow(({diam_esfera}/2000),2)',
            '{forma} == 2'   =>  '3.14159*pow(({diam_cilindro}/2000),2)',
            '{forma} == 3'   =>  '({espe_rodaja}/1000)*({diam_rodaja}/1000)',
            '{forma} == 4'   =>  '{espe_filete}/1000',
            '{forma} == 5'   =>  '({espe_prisma}/1000)*({ancho_prisma}/1000)',
            '{forma} == 6'   =>  '3.14159*({espe_oval}/2000)*({ancho_oval}/2000)'
    ),
    's2'    =>  array(
            '{forma} == 1'    =>  '3.14159*pow(({diam_esfera}/2000),2)',
            '{forma} == 2'   =>  '({diam_cilindro}/1000)*({long_cilindro}/1000)',
            '{forma} == 3'   =>  '3.14159*pow(({diam_rodaja}/2000),2)',
            '{forma} == 4'   =>  '{espe_filete}/1000',
            '{forma} == 5'   =>  '({espe_prisma}/1000)*({long_prisma}/1000)',
            '{forma} == 6'   =>  '3.14159*({espe_oval}/2000)*({long_oval}/2000)'
    ),
    'a1'    =>  array(
            '{forma} == 1 || {forma} == 2'    =>  '1',
            '{forma} == 3'   =>  '{s1}/(3.14159*pow(({espe_rodaja}/2000),2))',
            '{forma} == 4'   =>  '{s1}/(3.14159*pow(({espe_filete}/2000),2))',
            '{forma} == 5'   =>  '{s1}/(3.14159*pow(({espe_prisma}/2000),2))',
            '{forma} == 6'   =>  '{s1}/(3.14159*pow(({espe_oval}/2000),2))'
    ),
    'a2'    =>  array(
            '{forma} == 1'    =>  '1',
            '{forma} == 2'   =>  '{s2}/(3.14159*pow(({diam_cilindro}/2000),2))',
            '{forma} == 3'   =>  '{s2}/(3.14159*pow(({espe_rodaja}/2000),2))',
            '{forma} == 4'   =>  '{s2}/(3.14159*pow(({espe_filete}/2000),2))',
            '{forma} == 5'   =>  '{s2}/(3.14159*pow(({espe_prisma}/2000),2))',
            '{forma} == 6'   =>  '{s2}/(3.14159*pow(({espe_oval}/2000),2))'
    ),
    'g'=>'1/4+3/(8*pow({a1},2))+3/(8*pow({a2},2))',
    'd'    =>  array(
            '{forma} == 1'    =>  '{diam_esfera}',
            '{forma} == 2'   =>  '{diam_cilindro}',
            '{forma} == 3'   =>  '{espe_rodaja}',
            '{forma} == 4'   =>  '{espe_filete}',
            '{forma} == 5'   =>  '{espe_prisma}',
            '{forma} == 6'   =>  '{espe_oval}'
    ),
    'bi'=>'{h_cam}*{d}/(2000*{conduct_fres})',
    'xg'=>'log({g})',
    'xb'=>'log(1/{bi})',
    'm1'=>'exp(0.9208309+0.83409615*{xg}-0.78765739*{xb}-0.04821784*{xg}*{xb}-0.04088987*pow({xg},2)-0.10045526*pow({xb},2)+0.01521388*pow({xg},3)+0.00119941*{xg}*pow({xb},3)+0.00129982*pow({xb},4))',
    'f'=>'(2.303*pow(({d}/2000),2)/({m1}*{dif}))',
    'jm'=>'0.892*exp(-0.0388*{m1})',
    'tiempo_cong_calc'=>'(({dhv}/({temp_cong}-({temp_cam})))*({p}*({d}/1000)/{h_cam}+{r}*(pow(({d}/1000),2))/{conduct_cong})*(1-(1.65*{ste}/{conduct_cong})*log(({temp_final}-({temp_cam}))/(-10-({temp_cam})))))/({ehtd}*60)',
    'tiempo_cong'=>'{tiempo_cong_calc}',
    'tiempo_enf_calc'=>'(1/60)*(-({f})/2.303)*log(({temp_final}-({temp_cam}))/({jm}*({temp_inicial}-({temp_cam}))))',
    'tiempo_enf'=>'{tiempo_enf_calc}',
    'rend_tunel'    =>  array(
            '{idCamara} == 4'    =>  '{capacidad_cam}/({tiempo_cong}/60)',
            '{idCamara} == 3'   =>  '{capacidad_cam}/({tiempo_enf}/60)'
    ),
    'mayor_embalaje'=>'15',

    'doble_puerta'=>'0',
    'num_puertas_tunel'=>'0',
    'renov_infiltra'    =>  array(
            '{num_puertas_tunel} == 1'    =>  '1.5',
            '{num_puertas_tunel} == 2'   =>  '3'
    ),
    'renov_aire_calc'    =>  array(
            '{idCamara} == 1 || {idCamara} == 2 || {idCamara} == 3 || {idCamara} == 4 || {idCamara} >= 6'    =>  '0',
            '{idCamara} == 5'   =>  '50*24*({long_cam}*{ancho_cam}/5)/{volumen_cam}'
    ),
    'renov_aire_adiccional'    =>  array(
            '{renov_aire} > {renov_infiltra}'    =>  '{renov_aire}-({renov_infiltra})-({renov_aire_calc})',
            '{renov_aire} <= {renov_infiltra}'   =>  '0'
    ),
    'temp_recinto'    =>  array(
            '{idCamara} == 5 || {idCamara} == 8 || {idCamara} == 9 || {idCamara} == 10'    =>  '25',
            '{idCamara} == 1 || {idCamara} == 3 || {idCamara} == 6 || {idCamara} == 7'   =>  '12',
            '{idCamara} == 2 || {idCamara} == 4'   =>  '5'
    ),
    'hum_recinto'    =>  array(
            '{idCamara} == 5 || {idCamara} == 8 || {idCamara} == 9 || {idCamara} == 10'    =>  '50',
            '{idCamara} == 1 || {idCamara} == 2 || {idCamara} == 3 || {idCamara} == 4 || {idCamara} == 6 || {idCamara} == 7'   =>  '80'
    ),
    'p_sat_agua_ext'    =>  array(
            '{temp_recinto} < 0'    =>  'exp(-5674.5359*pow(({temp_recinto}+273.15),-1)+6.3925247-0.009677843*({temp_recinto}+273.15)+0.00000062215701*pow(({temp_recinto}+273.15),2)+0.0000000020747825*pow(({temp_recinto}+273.15).3)-0.0000000000009484024*pow(({temp_recinto}+273.15),4)+4.1635019*log({temp_recinto}+273.15))',
            '{temp_recinto} >= 0'   =>  'exp(-5800.2206*pow(({temp_recinto}+273.15),-1)+1.3914993-0.048640239*({temp_recinto}+273.15)+0.000041764768*pow(({temp_recinto}+273.15),2)-0.000000014452093*pow(({temp_recinto}+273.15),3)+6.5459673*log({temp_recinto}+273.15))'
    ),
    'hum_absol_ext'=>'1000*0.62198*({hum_recinto}/100)*{p_sat_agua_ext}/({presion}-({hum_recinto}/100)*{p_sat_agua_ext})',
    'p_parcial_agua_ext'=>'(({presion}/1000)*{hum_absol_ext}/1000)/(0.2883419/0.46191511+{hum_absol_ext}/1000)',
    'temp_rocio_ext'    =>  array(
            '{temp_recinto} < 0'    =>  '5.994+12.41*log({p_parcial_agua_ext})+0.4273*pow(log({p_parcial_agua_ext}),2)',
            '{temp_recinto} >= 0'   =>  '6.983+14.38*log({p_parcial_agua_ext})+1.079*pow(log({p_parcial_agua_ext}),2)'
    ),
    'entalpia_ext'=>'(1.006*{temp_recinto}+({hum_absol_ext}/1000)*(2501.3+1.89*{temp_recinto}))*1000',
    'entalpia_ext_sens'=>'(1.006*{temp_recinto}+({hum_absol_ext}/1000)*(1.89*{temp_recinto}))*1000',
    'entalpia_ext_lat'=>'({hum_absol_ext}/1000)*(2501.3)*1000',
    'volumen_espe_ext'=>'0.2870551882*({temp_recinto}+273.15)*(1+1.6078*{hum_absol_ext}/1000)/101.32503',
    'tipo_ubicacion'=>'0',
    'cerr_ext'=>'0',

    'orientacion_cerr1'=>'0',
    'temp_ext_calc_cerr1'=>'{temp_recinto}',
    'tipo_cerr2'=>'0',
    'color_cerr2'=>'0',
    'orientacion_cerr2'    =>  array(
            '{orientacion_cerr1} == 1'    =>  '4',
            '{orientacion_cerr1} == 2'   =>  '3',
            '{orientacion_cerr1} == 3'   =>  '1',
            '{orientacion_cerr1} == 4'   =>  '2',
            '{orientacion_cerr1} == 5'   =>  '6',
            '{orientacion_cerr1} == 6'   =>  '8',
            '{orientacion_cerr1} == 7'   =>  '5',
            '{orientacion_cerr1} == 8'   =>  '7'
    ),

    'temp_ext_norte_cerr2'=>'{temp_ext}',
    'temp_ext_sur_cerr2'    =>  array(
            '{color_cerr2} == 1'    =>  '{temp_ext}+2',
            '{color_cerr2} == 2 || {color_cerr2} == 3'   =>  '{temp_ext}+3'
    ),
    'temp_ext_este_cerr2'    =>  array(
            '{color_cerr2} == 1'    =>  '{temp_ext}+3',
            '{color_cerr2} == 2'   =>  '{temp_ext}+4',
            '{color_cerr2} == 3'   =>  '{temp_ext}+5'
    ),
    'temp_ext_oeste_cerr2'    =>  array(
            '{color_cerr2} == 1'    =>  '{temp_ext}+3',
            '{color_cerr2} == 2'   =>  '{temp_ext}+4',
            '{color_cerr2} == 3'   =>  '{temp_ext}+5'
    ),
    'temp_ext_cerr2'    =>  array(
            '{tipo_cerr2} != 1 && {orientacion_cerr2} == 1'   =>  '{temp_ext_norte_cerr2}',
            '{tipo_cerr2} != 1 && {orientacion_cerr2} == 2'   =>  '{temp_ext_sur_cerr2}',
            '{tipo_cerr2} != 1 && {orientacion_cerr2} == 3'   =>  '{temp_ext_este_cerr2}',
            '{tipo_cerr2} != 1 && {orientacion_cerr2} == 4'   =>  '{temp_ext_oeste_cerr2}',
            '{tipo_cerr2} != 1 && {orientacion_cerr2} == 5'   =>  '({temp_ext_norte_cerr2}+{temp_ext_este_cerr2})/2',
            '{tipo_cerr2} != 1 && {orientacion_cerr2} == 6'   =>  '({temp_ext_norte_cerr2}+{temp_ext_oeste_cerr2})/2',
            '{tipo_cerr2} != 1 && {orientacion_cerr2} == 7'   =>  '({temp_ext_sur_cerr2}+{temp_ext_este_cerr2})/2',
            '{tipo_cerr2} != 1 && {orientacion_cerr2} == 8'   =>  '({temp_ext_sur_cerr2}+{temp_ext_oeste_cerr2})/2'
    ),
    'temp_ext_calc_cerr2'    =>  array(
            '{tipo_cerr2} == 1 || {tipo_ubicacion} == 1 || {cerr_ext} == 2'    =>  '{temp_recinto}',
            '{tipo_cerr2} == 2 && {tipo_ubicacion} == 2 && {cerr_ext} == 1'    =>  '{temp_ext_cerr2}',
            '{tipo_cerr2} == 3 && {tipo_ubicacion} == 2 && {cerr_ext} == 1'    =>  '{temp_ext_cerr2}-7',
            '{tipo_cerr2} == 4 && {tipo_ubicacion} == 2 && {cerr_ext} == 1'    =>  '{temp_ext_cerr2}-3'
    ),
    'tipo_cerr3'=>'0',
    'color_cerr3'=>'0',
    'orientacion_cerr3'    =>  array(
            '{orientacion_cerr1} == 1'    =>  '2',
            '{orientacion_cerr1} == 2'   =>  '1',
            '{orientacion_cerr1} == 3'   =>  '4',
            '{orientacion_cerr1} == 4'   =>  '3',
            '{orientacion_cerr1} == 5'   =>  '8',
            '{orientacion_cerr1} == 6'   =>  '7',
            '{orientacion_cerr1} == 7'   =>  '6',
            '{orientacion_cerr1} == 8'   =>  '5'
    ),

    'temp_ext_norte_cerr3'=>'{temp_ext}',
    'temp_ext_sur_cerr3'    =>  array(
            '{color_cerr3} == 1'    =>  '{temp_ext}+2',
            '{color_cerr3} == 2 || {color_cerr3} == 3'   =>  '{temp_ext}+3'
    ),
    'temp_ext_este_cerr3'    =>  array(
            '{color_cerr3} == 1'    =>  '{temp_ext}+3',
            '{color_cerr3} == 2'   =>  '{temp_ext}+4',
            '{color_cerr3} == 3'   =>  '{temp_ext}+5'
    ),
    'temp_ext_oeste_cerr3'    =>  array(
            '{color_cerr3} == 1'    =>  '{temp_ext}+3',
            '{color_cerr3} == 2'   =>  '{temp_ext}+4',
            '{color_cerr3} == 3'   =>  '{temp_ext}+5'
    ),
    'temp_ext_cerr3'    =>  array(
            '{tipo_cerr3} != 1 && {orientacion_cerr3} == 1'   =>  '{temp_ext_norte_cerr3}',
            '{tipo_cerr3} != 1 && {orientacion_cerr3} == 2'   =>  '{temp_ext_sur_cerr3}',
            '{tipo_cerr3} != 1 && {orientacion_cerr3} == 3'   =>  '{temp_ext_este_cerr3}',
            '{tipo_cerr3} != 1 && {orientacion_cerr3} == 4'   =>  '{temp_ext_oeste_cerr3}',
            '{tipo_cerr3} != 1 && {orientacion_cerr3} == 5'   =>  '({temp_ext_norte_cerr3}+{temp_ext_este_cerr3})/2',
            '{tipo_cerr3} != 1 && {orientacion_cerr3} == 6'   =>  '({temp_ext_norte_cerr3}+{temp_ext_oeste_cerr3})/2',
            '{tipo_cerr3} != 1 && {orientacion_cerr3} == 7'   =>  '({temp_ext_sur_cerr3}+{temp_ext_este_cerr3})/2',
            '{tipo_cerr3} != 1 && {orientacion_cerr3} == 8'   =>  '({temp_ext_sur_cerr3}+{temp_ext_oeste_cerr3})/2'
    ),
    'temp_ext_calc_cerr3'    =>  array(
            '{tipo_cerr3} == 1 || {tipo_ubicacion} == 1 || {cerr_ext} == 2'    =>  '{temp_recinto}',
            '{tipo_cerr3} == 2 && {tipo_ubicacion} == 2 && {cerr_ext} == 1'    =>  '{temp_ext_cerr3}',
            '{tipo_cerr3} == 3 && {tipo_ubicacion} == 2 && {cerr_ext} == 1'    =>  '{temp_ext_cerr3}-7',
            '{tipo_cerr3} == 4 && {tipo_ubicacion} == 2 && {cerr_ext} == 1'    =>  '{temp_ext_cerr3}-3'
    ),
    'tipo_cerr4'=>'0',
    'color_cerr4'=>'0',

    'orientacion_cerr4'    =>  array(
            '{orientacion_cerr1} == 1'    =>  '3',
            '{orientacion_cerr1} == 2'   =>  '4',
            '{orientacion_cerr1} == 3'   =>  '2',
            '{orientacion_cerr1} == 4'   =>  '1',
            '{orientacion_cerr1} == 5'   =>  '7',
            '{orientacion_cerr1} == 6'   =>  '5',
            '{orientacion_cerr1} == 7'   =>  '8',
            '{orientacion_cerr1} == 8'   =>  '6'
    ),

    'temp_ext_norte_cerr4'=>'{temp_ext}',
    'temp_ext_sur_cerr4'    =>  array(
            '{color_cerr4} == 1'    =>  '{temp_ext}+2',
            '{color_cerr4} == 2 || {color_cerr4} == 3'   =>  '{temp_ext}+3'
    ),
    'temp_ext_este_cerr4'    =>  array(
            '{color_cerr4} == 1'    =>  '{temp_ext}+3',
            '{color_cerr4} == 2'   =>  '{temp_ext}+4',
            '{color_cerr4} == 3'   =>  '{temp_ext}+5'
    ),
    'temp_ext_oeste_cerr4'    =>  array(
            '{color_cerr4} == 1'    =>  '{temp_ext}+3',
            '{color_cerr4} == 2'   =>  '{temp_ext}+4',
            '{color_cerr4} == 3'   =>  '{temp_ext}+5'
    ),
    'temp_ext_cerr4'    =>  array(
            '{tipo_cerr4} != 1 && {orientacion_cerr4} == 1'   =>  '{temp_ext_norte_cerr4}',
            '{tipo_cerr4} != 1 && {orientacion_cerr4} == 2'   =>  '{temp_ext_sur_cerr4}',
            '{tipo_cerr4} != 1 && {orientacion_cerr4} == 3'   =>  '{temp_ext_este_cerr4}',
            '{tipo_cerr4} != 1 && {orientacion_cerr4} == 4'   =>  '{temp_ext_oeste_cerr4}',
            '{tipo_cerr4} != 1 && {orientacion_cerr4} == 5'   =>  '({temp_ext_norte_cerr4}+{temp_ext_este_cerr4})/2',
            '{tipo_cerr4} != 1 && {orientacion_cerr4} == 6'   =>  '({temp_ext_norte_cerr4}+{temp_ext_oeste_cerr4})/2',
            '{tipo_cerr4} != 1 && {orientacion_cerr4} == 7'   =>  '({temp_ext_sur_cerr4}+{temp_ext_este_cerr4})/2',
            '{tipo_cerr4} != 1 && {orientacion_cerr4} == 8'   =>  '({temp_ext_sur_cerr4}+{temp_ext_oeste_cerr4})/2'
    ),
    'temp_ext_calc_cerr4'    =>  array(
            '{tipo_cerr4} == 1 || {tipo_ubicacion} == 1 || {cerr_ext} == 2'    =>  '{temp_recinto}',
            '{tipo_cerr4} == 2 && {tipo_ubicacion} == 2 && {cerr_ext} == 1'    =>  '{temp_ext_cerr4}',
            '{tipo_cerr4} == 3 && {tipo_ubicacion} == 2 && {cerr_ext} == 1'    =>  '{temp_ext_cerr4}-7',
            '{tipo_cerr4} == 4 && {tipo_ubicacion} == 2 && {cerr_ext} == 1'    =>  '{temp_ext_cerr4}-3'
    ),
    'tipo_cubierta'=>'0',
    'color_cubierta'=>'0',
    'temp_ext_cubierta'    =>  array(
            '{color_cubierta} == 1'    =>  '{temp_ext}+5',
            '{color_cubierta} == 2'   =>  '{temp_ext}+9',
            '{color_cubierta} == 3'   =>  '{temp_ext}+11'
    ),

    'temp_ext_calc_techo'    =>  array(
        '{tipo_cubierta} == 1 || {tipo_ubicacion} == 1 || {cerr_ext} == 2'    =>  '{temp_recinto}',
        '{tipo_cubierta} == 2 && {tipo_ubicacion} == 2 && {cerr_ext} == 1'    =>  '{temp_ext_cubierta}',
        '{tipo_cubierta} == 3 && {tipo_ubicacion} == 2 && {cerr_ext} == 1'    =>  '{temp_ext_cubierta}-7',
        '{tipo_cubierta} == 4 && {tipo_ubicacion} == 2 && {cerr_ext} == 1'    =>  '{temp_ext_cubierta}-3'
    ),
    'cond_cerr1'=>'0',
    'cond_cerr2'=>'0',
    'cond_cerr3'=>'0',
    'cond_cerr4'=>'0',
    'cond_techo'=>'0',
    'cond_suelo'=>'0',
    'cond_aisla_suelo'=>'0',
    'cond_cam_aire'=>'0.75',
    'cond_puerta'=>'0',
    'cond_ventana'=>'0',
    'h_int_pared'=>'9+3.7*{vel_aire_cam}',
    'h_int_techo'=>'11+3.7*{vel_aire_cam}',
    'h_int_suelo'=>'6+3.7*{vel_aire_cam}',

    'h_ext_pared'=>'9+3.7*{vel_aire_cam}',
    'h_ext_techo'=>'11+3.7*{vel_aire_cam}',
    'espe_cerr1'=>'1000*{cond_cerr1}*(({temp_ext_calc_cerr1}-({temp_cam}))/(0.0667*{temp_cam}+9)-1/{h_int_pared}-1/{h_ext_pared})',
    'espe_cerr2'=>'1000*{cond_cerr2}*(({temp_ext_calc_cerr2}-({temp_cam}))/(0.0667*{temp_cam}+9)-1/{h_int_pared}-1/{h_ext_pared})',
    'espe_cerr3'=>'1000*{cond_cerr3}*(({temp_ext_calc_cerr3}-({temp_cam}))/(0.0667*{temp_cam}+9)-1/{h_int_pared}-1/{h_ext_pared})',
    'espe_cerr4'=>'1000*{cond_cerr4}*(({temp_ext_calc_cerr4}-({temp_cam}))/(0.0667*{temp_cam}+9)-1/{h_int_pared}-1/{h_ext_pared})',
    'espe_techo'=>'1000*{cond_techo}*(({temp_ext_calc_techo}-({temp_cam}))/(0.0667*{temp_cam}+9)-1/{h_int_techo}-1/{h_ext_techo})',
    'espe_promedio'=>'({espe_cerr1}+{espe_cerr2}+{espe_cerr3}+{espe_cerr4}+{espe_techo})/5',
    'cerr_verticales'=>'0',
    'espe_cerr1_calc'    =>  array(
            '{cerr_verticales} == 1'    =>  '{espe_promedio}',
            '{cerr_verticales} == 2'   =>  '{espe_cerr1}'
    ),
    'espe_cerr2_calc'    =>  array(
            '{cerr_verticales} == 1'    =>  '{espe_promedio}',
            '{cerr_verticales} == 2'   =>  '{espe_cerr2}'
    ),
    'espe_cerr3_calc'    =>  array(
            '{cerr_verticales} == 1'    =>  '{espe_promedio}',
            '{cerr_verticales} == 2'   =>  '{espe_cerr3}'
    ),
    'espe_cerr4_calc'    =>  array(
            '{cerr_verticales} == 1'    =>  '{espe_promedio}',
            '{cerr_verticales} == 2'   =>  '{espe_cerr4}'
    ),
    'espe_techo_calc'    =>  array(
            '{cerr_verticales} == 1'    =>  '{espe_promedio}',
            '{cerr_verticales} == 2'   =>  '{espe_techo}'
    ),
    'espe_suelo'=>'150',
    'camara_de_aire'=>'0',
    'espe_cam_aire'    =>  array(
            '{camara_de_aire} == 1'    =>  '150',
            '{camara_de_aire} == 2'   =>  '0'
    ),
    'aislamiento'=>'0',
    'espe_aisla_suelo'    =>  array(
            '{aislamiento} == 1'    =>  '1000*{cond_aisla_suelo}*(({temp_suelo}-({temp_cam}))/(0.0667*{temp_cam}+9)-1/{h_int_suelo}-({espe_suelo}/1000)/{cond_suelo}-({espe_cam_aire}/1000)/{cond_cam_aire})',
            '{aislamiento} == 2'   =>  '0'
    ),
    'espe_puerta'=>'1000*{cond_puerta}*(({temp_ext_calc_cerr1}-({temp_cam}))/(0.0667*{temp_cam}+9)-1/{h_int_pared}-1/{h_ext_pared})',
    'material_ventanas'=>'0',
    'espe_ventana'    =>  array(
            '{material_ventanas} == 23'    =>  '6',
            '{material_ventanas} == 24'   =>  '20'
    ),
    'sup_puerta'=>'3',
    'ventanas'=>'0',
    'sup_ventana'    =>  array(
            '{ventanas} == 1'    =>  '2',
            '{ventanas} == 2'   =>  '0'
    ),
    'pot_persona'=>'-4.4286*{temp_cam}+332.86',
    'pot_pers_sens'=>'-6.4571*{temp_cam}+251.71',
    'pot_pers_lat'=>'2.0285*{temp_cam}+81.15',

    'pot_lamp'=>'15',
    'pot_m2_ilum'=>'20',
    'pot_vent'    =>  array(
            '{tipoEvap} == 1'    =>  '0',
            '{tipoEvap} == 2 && {volumen_cam} <= 100 && ({idCamara} == 1 || {idCamara} == 2 || {idCamara} == 5 || {idCamara} >= 6)'   =>  '10.45*{volumen_cam}+650',
            '{tipoEvap} == 2 && {volumen_cam} > 100 && ({idCamara} == 1 || {idCamara} == 2 || {idCamara} == 5 || {idCamara} >= 6)'   =>  '3.6679*{volumen_cam}+2331.6',
            '{tipoEvap} == 2 && {idCamara} == 3'   =>  '3.2434*{rend_tunel}+734.26',
            '{tipoEvap} == 2 && {idCamara} == 4'   =>  '18.706*{rend_tunel}+1570.1'
    ),
    'tiempo_vent'    =>  array(
            '{idCamara} == 3  && {tiempo_enf} < 360'    =>  '24',
            '{idCamara} == 3 && {tiempo_enf} >= 360'   =>  '23',
            '{idCamara} == 4 && {tiempo_cong} < 240'   =>  '24',
            '{idCamara} == 4 && {tiempo_cong} >= 240'   =>  '22'
    ),
    'pot_vent_estima'    =>  array(
            '{tipoEvap} == 1'    =>  '0',
            '{tipoEvap} == 2 && ({idCamara} == 1 || {idCamara} == 2 || {idCamara} == 5 || {idCamara} >= 6)'   =>  '10',
            '{tipoEvap} == 2 && {idCamara} == 3'   =>  '15',
            '{tipoEvap} == 2 && {idCamara} == 4'   =>  '20'
    ),
    'pot_resist'    =>  array(
            '{volumen_cam} <= 200 && ({idCamara} == 1 || {idCamara} == 6 || {idCamara} == 7)'    =>  '110*{volumen_cam}',
            '{volumen_cam} > 200 && ({idCamara} == 1 || {idCamara} == 6 || {idCamara} == 7)'   =>  '55.035*{volumen_cam}+11007',
            '{volumen_cam} <= 300 && {idCamara} == 2'   =>  '42.515*{volumen_cam}+1149.7',
            '{volumen_cam} > 300 && {idCamara} == 2'   =>  '21.485*{volumen_cam}+8537.5',
            '{idCamara} == 3'   =>  '29.859*{rend_tunel}+2795.9',
            '{idCamara} == 4'   =>  '145.54*{rend_tunel}+4941.2',
            '{idCamara} == 5 || {idCamara} == 8 || {idCamara} == 9 || {idCamara} == 10'   =>  '0'
    ),
    'tiempo_resist'    =>  array(
            '{idCamara} == 3 && {tiempo_enf} < 360'    =>  '0',
            '{idCamara} == 3 && {tiempo_enf} >= 360'   =>  '1',
            '{idCamara} == 4 && {tiempo_cong} < 240'   =>  '0',
            '{idCamara} == 4 && {tiempo_cong} >= 240'   =>  '2'
    ),
    'pot_resist_estima'    =>  array(
            '{idCamara} == 3'    =>  '5',
            '{idCamara} == 4'   =>  '15'
    ),
    'pot_otros'=>'0',
    'tiempo_otros'=>'0',
    'pot_otros_estima'=>'0',
    'horas_dia'    =>  array(
            '{idCamara} == 3 || {idCamara} == 4'    =>  '23',
            '{idCamara} == 5'   =>  '22',
            '{idCamara} == 1 || {idCamara} >= 6'   =>  '21',
            '{idCamara} == 2'   =>  '20'
    ),
    'mayor_segur'    =>  array(
            '{volumen_cam} <= 500'    =>  '10',
            '{volumen_cam} > 500'   =>  '5'
    ),

    'calculoA'=>'(1/(1/{h_int_pared}+1/{h_ext_pared}+({espe_cerr1_calc}/1000)/{cond_cerr1}))*({long_cam}*{alto_cam})*({temp_ext_calc_cerr1}-({temp_cam}))+(1/(1/{h_int_pared}+1/{h_ext_pared}+({espe_cerr2_calc}/1000)/{cond_cerr2}))*({ancho_cam}*{alto_cam})*({temp_ext_calc_cerr2}-({temp_cam}))+(1/(1/{h_int_pared}+1/{h_ext_pared}+({espe_cerr3_calc}/1000)/{cond_cerr3}))*({long_cam}*{alto_cam})*({temp_ext_calc_cerr3}-({temp_cam}))+(1/(1/{h_int_pared}+1/{h_ext_pared}+({espe_cerr4_calc}/1000)/{cond_cerr4}))*({ancho_cam}*{alto_cam})*({temp_ext_calc_cerr4}-({temp_cam}))+(1/(1/{h_int_techo}+1/{h_ext_techo}+({espe_techo_calc}/1000)/{cond_techo}))*({long_cam}*{ancho_cam})*({temp_ext_calc_techo}-({temp_cam}))+(1/(1/{h_int_suelo}+({espe_suelo}/1000)/{cond_suelo}+({espe_aisla_suelo}/1000)/{cond_aisla_suelo}+({espe_cam_aire}/1000)/{cond_cam_aire}))*({long_cam}*{ancho_cam})*({temp_suelo}-({temp_cam}))+(1/(1/{h_int_pared}+1/{h_ext_pared}+({espe_puerta}/1000)/{cond_puerta}))*({sup_puerta})*({temp_recinto}-({temp_cam}))-(1/(1/{h_int_pared}+1/{h_ext_pared}+({espe_cerr1_calc}/1000)/{cond_cerr1}))*({sup_puerta})*({temp_ext_calc_cerr1}-({temp_cam}))+(1/(1/{h_int_pared}+1/{h_ext_pared}+({espe_ventana}/1000)/{cond_ventana}))*({sup_ventana})*({temp_ext_calc_cerr1}-({temp_cam}))-(1/(1/{h_int_pared}+1/{h_ext_pared}+({espe_cerr1_calc}/1000)/{cond_cerr1}))*({sup_ventana})*({temp_ext_calc_cerr1}-({temp_cam}))',
    'calculoB'=>'{renov_infiltra}*{volumen_cam}*({entalpia_ext}-({entalpia_cam}))/({volumen_espe_cam}*24*3600)',
    'calculoB1'=>'{renov_infiltra}*{volumen_cam}*({entalpia_ext_sens}-({entalpia_cam_sens}))/({volumen_espe_cam}*24*3600)',
    'calculoB2'=>'{renov_infiltra}*{volumen_cam}*({entalpia_ext_lat}-({entalpia_cam_lat}))/({volumen_espe_cam}*24*3600)',
    'calculoC'=>'{renov_aire_calc}*{volumen_cam}*({entalpia_ext}-({entalpia_cam}))/({volumen_espe_cam}*24*3600)',
    'calculoC1'=>'{renov_aire_calc}*{volumen_cam}*({entalpia_ext_sens}-({entalpia_cam_sens}))/({volumen_espe_cam}*24*3600)',
    'calculoC2'=>'{renov_aire_calc}*{volumen_cam}*({entalpia_ext_lat}-({entalpia_cam_lat}))/({volumen_espe_cam}*24*3600)',

    'calculoD'    =>  array(
            '{idCamara} == 1'    =>  '(1+{mayor_embalaje}/100)*{capacidad_cam}*({rotacion}/100)*{calor_esp_fres}*({temp_inicial}-({temp_final}))/(24*3600)',
            '{idCamara} == 2'    =>  '(1+{mayor_embalaje}/100)*{capacidad_cam}*({rotacion}/100)*{calor_esp_cong_calc}*({temp_inicial}-({temp_final}))/(24*3600)',
            '{idCamara} == 3'   =>  '(1+{mayor_embalaje}/100)*{rend_tunel}*{calor_esp_fres}*({temp_inicial}-({temp_final}))/3600',
            '{idCamara} == 4'   =>  '(1+{mayor_embalaje}/100)*{rend_tunel}*({calor_esp_fres}*({temp_inicial}-({temp_cong}))+{calor_lat_cong}+{calor_esp_cong_calc}*({temp_cong}-({temp_final})))/3600',
            '{idCamara} == 5'    =>  '(1+{mayor_embalaje}/100)*{capacidad_cam}*({rotacion}/100)*{calor_esp_fres}*({temp_inicial}-({temp_final}))/(24*3600)',
            '{idCamara} >= 6'    =>  '(1+{mayor_embalaje}/100)*{capacidad_cam}*{calor_esp_fres}*({temp_inicial}-({temp_final}))/({duracion_secado}*24*3600)+1000*{capacidad_cam}*({merma_producto}/100)*(2501-2.38*{temp_cam})/({duracion_secado}*24*3600)',
    ),
    'calculoD2'    =>  array(
            '{idCamara} < 6'    =>  '0',
            '{idCamara} >= 6'    =>  '1000*{capacidad_cam}*({merma_producto}/100)*(2501-2.38*{temp_cam})/({duracion_secado}*24*3600)',
    ),
    'calculoD1'=>'{calculoD}- ({calculoD2})',

    'calculoE'    =>  array(
            '{idCamara} == 1 || {idCamara} == 2 || {idCamara} == 5 || {idCamara} >= 6'    =>  '({capacidad_cam}*{rotacion}/100)*{pot_respi_tmedia}+({capacidad_cam}-({capacidad_cam})*{rotacion}/100)*{pot_respi_tfinal}',
            '{idCamara} == 3'   =>  '{rend_tunel}*({tiempo_enf}/60)*{pot_respi_tmedia}',
            '{idCamara} == 4'   =>  '{rend_tunel}*({tiempo_cong}/60)*{pot_respi_tmedia}'
    ),

    'tipoNumPot'=>'0',
    'tipoLamp'=>'0',

    'potVentEst'=>'0',
    'calculoH'    =>  array(
            '{tipoEvap} == 2 && {potVentEst} == 1'    =>  '{pot_vent}*{tiempo_vent}/24',
            '{tipoEvap} == 2 && {potVentEst} == 2'   =>  '({pot_vent_estima}/100)*({calculoA}+{calculoB}+{calculoC}+{calculoD})'
    ),

    'potResisEst'=>'0',
    'calculoI'    =>  array(
            '{potResisEst} == 1'    =>  '{pot_resist}*{tiempo_resist}/24',
            '{potResisEst} == 2'   =>  '({pot_resist_estima}/100)*({calculoA}+{calculoB}+{calculoC}+{calculoD})'
    ),

    'potConEst'=>'0',
    'calculoJ'    =>  array(
            '{potConEst} == 1'    =>  '{pot_otros}*{tiempo_otros}/24',
            '{potConEst} == 2'   =>  '({pot_otros_estima}/100)*({calculoA}+{calculoB}+{calculoC}+{calculoD})'
    ),
    'carga_total'=>'{calculoA}+{calculoB}+{calculoC}+{calculoD}+{calculoE}+{calculoH}+{calculoI}+{calculoJ}',
    'carga_sens'=>'{calculoA}+{calculoB1}+{calculoC1}+{calculoD1}+{calculoE}+{calculoH}+{calculoI}+{calculoJ}',
    'carga_lat'=>'{carga_total} - {carga_sens}',
    'porcentaje_calculoA'=>'({calculoA}/ {carga_total})* 100',
    'porcentaje_calculoB'=>'({calculoB}/ {carga_total})* 100',
    'porcentaje_calculoC'=>'({calculoC}/ {carga_total})* 100',
    'porcentaje_calculoD'=>'({calculoD}/ {carga_total})* 100',
    'porcentaje_calculoE'=>'({calculoE}/ {carga_total})* 100',
    'porcentaje_calculoH'=>'({calculoH}/ {carga_total})* 100',
    'porcentaje_calculoI'=>'({calculoI}/ {carga_total})* 100',
    'porcentaje_calculoJ'=>'({calculoJ}/ {carga_total})* 100',
    'porcentaje_carga_sens'=>'({carga_sens}/ {carga_total})* 100',
    'porcentaje_carga_lat'=>'({carga_lat}/ {carga_total})* 100',
    'porcen_carga_sens'=>'{carga_sens}/{carga_total}',
    'porcen_carga_lat'=>'{carga_lat}/{carga_total}',
    'pot_frig_cam'=>'{carga_sens}*(24/{horas_dia})*(1+{mayor_segur}/100)+{carga_lat}*(24/{horas_dia})*(1+{mayor_segur}/100)',
    'pot_frig_cam_sens'=>'{pot_frig_cam}*{porcen_carga_sens}',
    'pot_frig_cam_lat'=>'{pot_frig_cam}*{porcen_carga_lat}',
);

${NAME_ARRAY_FORMULAS_SECADERO} = array(
    'temp_ext'=>'0',
    'hum_ext'=>'0',
    'temp_suelo'=>'({temp_ext}+15)/2',
    'altitud'=>'0',
    'vel_viento'=>'0',
    'temp_cam'=>'0',
    'hum_cam'=>'0',
    'idCamara'=>'0',
    'presion'=>'101325*pow((1-0.0065/(15+273.15)*{altitud}),(9.807/0.2883419/0.0065/1000))',
    'p_sat_agua_cam'    =>  array(
            '{temp_cam} < 0'    =>  'exp(-5674.5359*pow(({temp_cam}+273.15),-1)+6.3925247-0.009677843*({temp_cam}+273.15)+0.00000062215701*pow(({temp_cam}+273.15),2)+0.0000000020747825*pow(({temp_cam}+273.15),3)-0.0000000000009484024*pow(({temp_cam}+273.15),4)+4.1635019*log({temp_cam}+273.15))',
            '{temp_cam} >= 0'   =>  'exp(-5800.2206*pow(({temp_cam}+273.15),-1)+1.3914993-0.048640239*({temp_cam}+273.15)+0.000041764768*pow(({temp_cam}+273.15),2)-0.000000014452093*pow(({temp_cam}+273.15),3)+6.5459673*log({temp_cam}+273.15))'
    ),
    'hum_absol_cam'=>'1000*0.62198*({hum_cam}/100)*{p_sat_agua_cam}/({presion}-({hum_cam}/100)*{p_sat_agua_cam})',
    'p_parcial_agua_cam'=>'(({presion}/1000)*{hum_absol_cam}/1000)/(0.2883419/0.46191511+{hum_absol_cam}/1000)',
    'temp_rocio_cam'    =>  array(
            '{temp_cam} < 0'    =>  '5.994+12.41*log({p_parcial_agua_cam})+0.4273*pow(log({p_parcial_agua_cam}),2)',
            '{temp_cam} >= 0'   =>  '6.983+14.38*log({p_parcial_agua_cam})+1.079*pow(log({p_parcial_agua_cam}),2)'
    ),
    'entalpia_cam'=>'(1.006*{temp_cam}+({hum_absol_cam}/1000)*(2501.3+1.89*{temp_cam}))*1000',
    'entalpia_cam_sens'=>'(1.006*{temp_cam}+({hum_absol_cam}/1000)*(1.89*{temp_cam}))*1000',
    'entalpia_cam_lat'=>'({hum_absol_cam}/1000)*(2501.3)*1000',
    'volumen_espe_cam'=>'0.2870551882*({temp_cam}+273.15)*(1+1.6078*{hum_absol_cam}/1000)/101.32503',
    'dt_vent'=>'-0.4267*{hum_cam}+42.502',
    'dt_est'=>'-0.472*{hum_cam}+49.815',

    'tipoEvap'=>'0',
    'temp_evap'    =>  array(
            '{tipoEvap} == 2'    =>  '{temp_cam}-({dt_vent})',
            '{tipoEvap} == 1'   =>  '{temp_cam}-({dt_est})'
    ),
    'p_sat_agua_evap'    =>  array(
            '{temp_evap} < 0'    =>  'exp(-5674.5359*pow(({temp_evap}+273.15),-1)+6.3925247-0.009677843*({temp_evap}+273.15)+0.00000062215701*pow(({temp_evap}+273.15),2)+0.0000000020747825*pow(({temp_evap}+273.15),3)-0.0000000000009484024*pow(({temp_evap}+273.15),4)+4.1635019*log({temp_evap}+273.15))',
            '{temp_evap} >= 0'   =>  'exp(-5800.2206*pow(({temp_evap}+273.15),-1)+1.3914993-0.048640239*({temp_evap}+273.15)+0.000041764768*pow(({temp_evap}+273.15),2)-0.000000014452093*pow(({temp_evap}+273.15),3)+6.5459673*log({temp_evap}+273.15))'
    ),
    'hum_absol_evap'=>'1000*0.62198*(100/100)*{p_sat_agua_evap}/({presion}-(100/100)*{p_sat_agua_evap})',
    'p_parcial_agua_evap'=>'(({presion}/1000)*{hum_absol_evap}/1000)/(0.2883419/0.46191511+{hum_absol_evap}/1000)',
    'temp_rocio_evap'    =>  array(
            '{temp_evap} < 0'    =>  '5.994+12.41*log({p_parcial_agua_evap})+0.4273*pow(log({p_parcial_agua_evap}),2)',
            '{temp_evap} >= 0'   =>  '6.983+14.38*log({p_parcial_agua_evap})+1.079*pow(log({p_parcial_agua_evap}),2)'
    ),
    'entalpia_evap'=>'(1.006*{temp_evap}+({hum_absol_evap}/1000)*(2501.3+1.89*{temp_evap}))*1000',
    'entalpia_evap_sens'=>'(1.006*{temp_evap}+({hum_absol_evap}/1000)*(1.89*{temp_evap}))*1000',
    'entalpia_evap_lat'=>'({hum_absol_evap}/1000)*(2501.3)*1000',
    'volumen_espe_evap'=>'0.2870551882*({temp_evap}+273.15)*(1+1.6078*{hum_absol_evap}/1000)/101.32503',
    'd_entalpia_evap_sens'=>'({entalpia_cam_sens}-({entalpia_evap_sens}))/({entalpia_cam}-({entalpia_evap}))',
    'd_entalpia_evap_lat'=>'({entalpia_cam_lat}-({entalpia_evap_lat}))/({entalpia_cam}-({entalpia_evap}))',
    'vel_aire_cam'    =>  array(
            '{idCamara} == 1 || {idCamara} == 2 || {idCamara} == 5'    =>  '1',
            '{idCamara} == 3 || {idCamara} == 4'   =>  '7',
            '{idCamara} >= 6'   =>  '0.5'
    ),
    'h_cam'=>'9+3.7*{vel_aire_cam}',

    'temp_inicial'=>'25',
    'temp_final'=>'{temp_cam}',
    'agua_prod_fres'=>'0',
    'agua_prod_seco'=>'0',
    'merma_producto'=>'100*({agua_prod_fres}/100-({agua_prod_seco})/100)/(1-({agua_prod_seco})/100)',
    'agua'=>'0',
    'proteinas'=>'0',
    'grasas'=>'0',
    'carbohidratos'=>'0',
    'fibras'=>'0',
    'cenizas'=>'0',
    'calor_esp_fres'=>'(({agua}/100)*(4.217-0.000090864*{temp_inicial}+0.0000054731*pow({temp_inicial},2))+({proteinas}/100)*(2.0082+0.0012089*{temp_inicial}-0.0000013129*pow({temp_inicial},2))+({grasas}/100)*(1.9842+0.0014733*{temp_inicial}-0.0000048008*pow({temp_inicial},2))+({carbohidratos}/100)*(1.5488+0.0019625*{temp_inicial}-0.0000059399*pow({temp_inicial},2))+({fibras}/100)*(1.8459+0.0018306*{temp_inicial}-0.0000046509*pow({temp_inicial},2))+({cenizas}/100)*(1.0926+0.0018896*{temp_inicial}-0.0000036817*pow({temp_inicial},2)))*1000',
    'densi_fres'=>'({agua}/100)*(997.18+0.0031439*{temp_inicial}-0.0037574*pow({temp_inicial},2))+({proteinas}/100)*(1329.9-0.5184*{temp_inicial})+({grasas}/100)*(925.59-0.41757*{temp_inicial})+({carbohidratos}/100)*(1599.1-0.3104*{temp_inicial})+({fibras}/100)*(1311.5-0.36589*{temp_inicial})+({cenizas}/100)*(2423.8-0.28063*{temp_inicial})',
    'densi_cong'=>'(1.105*({agua}/100)/(1+0.7138/log({temp_cong}-({temp_final})+1)))*(916.89-0.13071*{temp_final})+({agua}/100-1.105*({agua}/100)/(1+0.7138/log({temp_cong}-({temp_final})+1)))*(997.18+0.0031439*{temp_final}-0.0037574*pow({temp_final},2))+({proteinas}/100)*(1329.9-0.5184*{temp_final})+({grasas}/100)*(925.59-0.41757*{temp_final})+({carbohidratos}/100)*(1599.1-0.31046*{temp_final})+({fibras}/100)*(1311.5-0.36589*{temp_final})+({cenizas}/100)*(2423.8-0.28063*{temp_final})',

    'A'=>'0',

    'B'=>'0',
    'pot_respi_tfinal'    =>  array(
            '{A} == 0 && {B} == 0'    =>  '0',
            '{A} != 0 || {B} != 0'   =>  '(exp({A}+{B}*{temp_final}))/1000'
    ),
    'pot_respi_tmedia'    =>  array(
            '{A} == 0 && {B} == 0'    =>  '0',
            '{A} != 0 || {B} != 0'   =>  '(exp({A}+{B}*({temp_inicial}+{temp_final})/2))/1000'
    ),
    'renov_aire'=>'0',
    'diam_esfera'=>'0',
    'diam_cilindro'=>'0',
    'long_cilindro'=>'0',
    'diam_rodaja'=>'0',
    'espe_rodaja'=>'0',
    'espe_filete'=>'0',
    'long_prisma'=>'0',
    'ancho_prisma'=>'0',
    'espe_prisma'=>'0',
    'long_oval'=>'0',
    'ancho_oval'=>'0',
    'espe_oval'=>'0',

    'forma'=>'0',
    'ms'    =>  array(
            '{forma} == 1'    =>  '{densi_fres}*(1-({agua})/100)*(4/3)*3.14159*pow(({diam_esfera}/2000),3)',
            '{forma} == 2'   =>  '{densi_fres}*(1-({agua})/100)*3.14159*pow(({diam_cilindro}/2000),2)*{long_cilindro}',
            '{forma} == 3'   =>  '{densi_fres}*(1-({agua})/100)*3.14159*pow(({diam_rodaja}/2000),2)*{espe_rodaja}',
            '{forma} == 5'   =>  '{densi_fres}*(1-({agua})/100)*({long_prisma}/1000)*({ancho_prisma}/1000)*({espe_prisma}/1000)',
            '{forma} == 6'   =>  '{densi_fres}*(1-({agua})/100)*({long_oval}/2000)*({ancho_oval}/2000)*({espe_oval}/2000)*3.14159*(4/3)'
    ),
    'x0'=>'({agua_prod_fres}/100)/(1-({agua_prod_fres})/100)',
    'xf'=>'({agua_prod_seco}/100)/(1-({agua_prod_seco})/100)',
    'sup'    =>  array(
            '{forma} == 1'    =>  '3.14159*pow(({diam_esfera}/1000),2)',
            '{forma} == 2'   =>  '2*3.14159*pow(({diam_cilindro}/2000),2)+{long_cilindro}*3.14159*{diam_cilindro}/1000',
            '{forma} == 3'   =>  '2*3.14159*pow(({diam_rodaja}/2000),2)+{long_rodaja}*3.14159*{diam_rodaja}/1000',
            '{forma} == 5'   =>  '2*({long_prisma}/1000)*({ancho_prisma}/1000)+2*({long_prisma}/1000)*({espe_prisma}/1000)+2*({espe_prisma}/1000)*({ancho_prisma}/1000)',
            '{forma} == 6'   =>  '4*3.14159*pow(((pow(({long_oval}/2000),1.6075)*pow(({ancho_oval}/2000),1.6075)+pow(({long_oval}/2000),1.6075)*pow(({espe_oval}/2000),1.6075)+pow(({espe_oval}/2000),1.6075)*pow(({ancho_oval}/2000),1.6075))/3),(1/1.6075))'
    ),
    'th'=>'{temp_cam}-(0.2726*{temp_cam}+{hum_absol_cam}*(-1.569+0.04316*{temp_cam}-0.000342*pow({temp_cam},2))+0.003606*pow({temp_cam},2)-0.00006374*pow({temp_cam},3)+5.864)',
    'pth'=>'exp(-5800.2206*pow(({th}+273.15),-1)+1.3914993-0.048640239*({th}+273.15)+0.000041764768*pow(({th}+273.15),2)-0.000000014452093*pow(({th}+273.15),3)+6.5459673*log({th}+273.15))',
    'ya'=>'0.62198*({hum_cam}/100)*{p_sat_agua_cam}/({presion}-({hum_cam}/100)*{p_sat_agua_cam})',
    'ys'=>'0.62198*pth/({presion}-({pth}))',
    'kg'=>'{h_cam}/10000',
    'xc'=>'0',
    'xeq'=>'0',
    'duracion_secado'    =>  array(
            '{xf} < {xc}'    =>  '({ms}*({x0}-({xc}))/({kg}*{sup}*({ys}-({ya})))+(({ms}*({xc}-({xeq}))/({kg}*{sup}*({ys}-({ya}))))*log(({xc}-({xeq}))/({xf}-({xeq}))))/(3600*24)',
            '{xf} >= {xc}'   =>  '({ms}*({x0}-({xf}))/({kg}*{sup}*({ys}-({ya}))))/(3600*24)'
    ),
    'long_cam'=>'0',
    'ancho_cam'=>'0',
    'alto_cam'=>'0',
    'volumen_cam'=>'{long_cam}*{ancho_cam}*{alto_cam}',
    'densi_almacen'=>'0',
    'capacidad_cam'    =>  array(
            '{idCamara} != 3 && {idCamara} != 4 && {idCamara} != 5'    =>  '{volumen_cam}*{densi_almacen}',
            '{idCamara} == 5'   =>  '{volumen_cam}*(-5.03*log({volumen_cam})+72.127)',
            '{idCamara} == 3 || {idCamara} == 4'   =>  '80*{long_cam}*{ancho_cam}'
    ),
    'rotacion_tipo'=>'0',
    'rotacion'    =>  array(
            '{rotacion_tipo} == 1 && {volumen_cam} < 100'    =>  '42',
            '{rotacion_tipo} == 1 && {volumen_cam} >= 100 && {volumen_cam} < 2000'   =>  '-7.587*log({volumen_cam})+76.566',
            '{rotacion_tipo} == 1 && {volumen_cam} > 2000'   =>  '19',
            '{rotacion_tipo} == 2 && {volumen_cam} < 100'   =>  '26',
            '{rotacion_tipo} == 2 && {volumen_cam} >= 100 && {volumen_cam} < 2000'   =>  '-4.915*log({volumen_cam})+48.603',
            '{rotacion_tipo} == 2 && {volumen_cam} >= 2000'   =>  '12',
            '{rotacion_tipo} == 3 && {volumen_cam} < 100'   =>  '16',
            '{rotacion_tipo} == 3 && {volumen_cam} >= 100 && {volumen_cam} < 2000'   =>  '-3.794*log({volumen_cam})+33.288',
            '{rotacion_tipo} == 3 && {volumen_cam} >= 2000'   =>  '5'
    ),
    'carga_diaria'=>'{capacidad_cam}* ({rotacion}/ 100)',
    'mayor_embalaje'=>'15',


    'doble_puerta'=>'0',
    'num_puertas_tunel'=>'0',
    'renov_infiltra'    =>  array(
            '{doble_puerta} == 1'    =>  '((100-0.005*{volumen_cam})/sqrt({volumen_cam}))/2',
            '{doble_puerta} == 2'   =>  '(100-0.005*{volumen_cam})/sqrt({volumen_cam})'
    ),
    'renov_aire_calc'    =>  array(
            '{idCamara} == 1 || {idCamara} == 2 || {idCamara} == 3 || {idCamara} == 4 || {idCamara} >= 6'    =>  '0',
            '{idCamara} == 5'   =>  '50*24*({long_cam}*{ancho_cam}/5)/{volumen_cam}'
    ),
    'renov_aire_adiccional'    =>  array(
            '{renov_aire} > {renov_infiltra}'    =>  '{renov_aire}-({renov_infiltra})-({renov_aire_calc})',
            '{renov_aire} <= {renov_infiltra}'   =>  '0'
    ),
    'temp_recinto'    =>  array(
            '{idCamara} == 5 || {idCamara} == 8 || {idCamara} == 9 || {idCamara} == 10'    =>  '25',
            '{idCamara} == 1 || {idCamara} == 3 || {idCamara} == 6 || {idCamara} == 7'   =>  '12',
            '{idCamara} == 2 || {idCamara} == 4'   =>  '5'
    ),
    'hum_recinto'    =>  array(
            '{idCamara} == 5 || {idCamara} == 8 || {idCamara} == 9 || {idCamara} == 10'    =>  '50',
            '{idCamara} == 1 || {idCamara} == 2 || {idCamara} == 3 || {idCamara} == 4 || {idCamara} == 6 || {idCamara} == 7'   =>  '80'
    ),
    'p_sat_agua_ext'    =>  array(
            '{temp_recinto} < 0'    =>  'exp(-5674.5359*pow(({temp_recinto}+273.15),-1)+6.3925247-0.009677843*({temp_recinto}+273.15)+0.00000062215701*pow(({temp_recinto}+273.15),2)+0.0000000020747825*pow(({temp_recinto}+273.15).3)-0.0000000000009484024*pow(({temp_recinto}+273.15),4)+4.1635019*log({temp_recinto}+273.15))',
            '{temp_recinto} >= 0'   =>  'exp(-5800.2206*pow(({temp_recinto}+273.15),-1)+1.3914993-0.048640239*({temp_recinto}+273.15)+0.000041764768*pow(({temp_recinto}+273.15),2)-0.000000014452093*pow(({temp_recinto}+273.15),3)+6.5459673*log({temp_recinto}+273.15))'
    ),
    'hum_absol_ext'=>'1000*0.62198*({hum_recinto}/100)*{p_sat_agua_ext}/({presion}-({hum_recinto}/100)*{p_sat_agua_ext})',
    'p_parcial_agua_ext'=>'(({presion}/1000)*{hum_absol_ext}/1000)/(0.2883419/0.46191511+{hum_absol_ext}/1000)',
    'temp_rocio_ext'    =>  array(
            '{temp_recinto} < 0'    =>  '5.994+12.41*log({p_parcial_agua_ext})+0.4273*pow(log({p_parcial_agua_ext}),2)',
            '{temp_recinto} >= 0'   =>  '6.983+14.38*log({p_parcial_agua_ext})+1.079*pow(log({p_parcial_agua_ext}),2)'
    ),
    'entalpia_ext'=>'(1.006*{temp_recinto}+({hum_absol_ext}/1000)*(2501.3+1.89*{temp_recinto}))*1000',
    'entalpia_ext_sens'=>'(1.006*{temp_recinto}+({hum_absol_ext}/1000)*(1.89*{temp_recinto}))*1000',
    'entalpia_ext_lat'=>'({hum_absol_ext}/1000)*(2501.3)*1000',
    'volumen_espe_ext'=>'0.2870551882*({temp_recinto}+273.15)*(1+1.6078*{hum_absol_ext}/1000)/101.32503',

    'tipo_ubicacion'=>'0',
    'cerr_ext'=>'0',
    'orientacion_cerr1'=>'0',
    'temp_ext_calc_cerr1'=>'{temp_recinto}',
    'tipo_cerr2'=>'0',
    'color_cerr2'=>'0',
    'orientacion_cerr2'    =>  array(
            '{orientacion_cerr1} == 1'    =>  '4',
            '{orientacion_cerr1} == 2'   =>  '3',
            '{orientacion_cerr1} == 3'   =>  '1',
            '{orientacion_cerr1} == 4'   =>  '2',
            '{orientacion_cerr1} == 5'   =>  '6',
            '{orientacion_cerr1} == 6'   =>  '8',
            '{orientacion_cerr1} == 7'   =>  '5',
            '{orientacion_cerr1} == 8'   =>  '7'
    ),

    'temp_ext_norte_cerr2'=>'{temp_ext}',
    'temp_ext_sur_cerr2'    =>  array(
            '{color_cerr2} == 1'    =>  '{temp_ext}+2',
            '{color_cerr2} == 2 || {color_cerr2} == 3'   =>  '{temp_ext}+3'
    ),
    'temp_ext_este_cerr2'    =>  array(
            '{color_cerr2} == 1'    =>  '{temp_ext}+3',
            '{color_cerr2} == 2'   =>  '{temp_ext}+4',
            '{color_cerr2} == 3'   =>  '{temp_ext}+5'
    ),
    'temp_ext_oeste_cerr2'    =>  array(
            '{color_cerr2} == 1'    =>  '{temp_ext}+3',
            '{color_cerr2} == 2'   =>  '{temp_ext}+4',
            '{color_cerr2} == 3'   =>  '{temp_ext}+5'
    ),
    'temp_ext_cerr2'    =>  array(
            '{tipo_cerr2} != 1 && {orientacion_cerr2} == 1'   =>  '{temp_ext_norte_cerr2}',
            '{tipo_cerr2} != 1 && {orientacion_cerr2} == 2'   =>  '{temp_ext_sur_cerr2}',
            '{tipo_cerr2} != 1 && {orientacion_cerr2} == 3'   =>  '{temp_ext_este_cerr2}',
            '{tipo_cerr2} != 1 && {orientacion_cerr2} == 4'   =>  '{temp_ext_oeste_cerr2}',
            '{tipo_cerr2} != 1 && {orientacion_cerr2} == 5'   =>  '({temp_ext_norte_cerr2}+{temp_ext_este_cerr2})/2',
            '{tipo_cerr2} != 1 && {orientacion_cerr2} == 6'   =>  '({temp_ext_norte_cerr2}+{temp_ext_oeste_cerr2})/2',
            '{tipo_cerr2} != 1 && {orientacion_cerr2} == 7'   =>  '({temp_ext_sur_cerr2}+{temp_ext_este_cerr2})/2',
            '{tipo_cerr2} != 1 && {orientacion_cerr2} == 8'   =>  '({temp_ext_sur_cerr2}+{temp_ext_oeste_cerr2})/2'
    ),
    'temp_ext_calc_cerr2'    =>  array(
            '{tipo_cerr2} == 1 || {tipo_ubicacion} == 1 || {cerr_ext} == 2'    =>  '{temp_recinto}',
            '{tipo_cerr2} == 2 && {tipo_ubicacion} == 2 && {cerr_ext} == 1'    =>  '{temp_ext_cerr2}',
            '{tipo_cerr2} == 3 && {tipo_ubicacion} == 2 && {cerr_ext} == 1'    =>  '{temp_ext_cerr2}-7',
            '{tipo_cerr2} == 4 && {tipo_ubicacion} == 2 && {cerr_ext} == 1'    =>  '{temp_ext_cerr2}-3'
    ),
    'tipo_cerr3'=>'0',
    'color_cerr3'=>'0',
    'orientacion_cerr3'    =>  array(
            '{orientacion_cerr1} == 1'    =>  '2',
            '{orientacion_cerr1} == 2'   =>  '1',
            '{orientacion_cerr1} == 3'   =>  '4',
            '{orientacion_cerr1} == 4'   =>  '3',
            '{orientacion_cerr1} == 5'   =>  '8',
            '{orientacion_cerr1} == 6'   =>  '7',
            '{orientacion_cerr1} == 7'   =>  '6',
            '{orientacion_cerr1} == 8'   =>  '5'
    ),

    'temp_ext_norte_cerr3'=>'{temp_ext}',
    'temp_ext_sur_cerr3'    =>  array(
            '{color_cerr3} == 1'    =>  '{temp_ext}+2',
            '{color_cerr3} == 2 || {color_cerr3} == 3'   =>  '{temp_ext}+3'
    ),
    'temp_ext_este_cerr3'    =>  array(
            '{color_cerr3} == 1'    =>  '{temp_ext}+3',
            '{color_cerr3} == 2'   =>  '{temp_ext}+4',
            '{color_cerr3} == 3'   =>  '{temp_ext}+5'
    ),
    'temp_ext_oeste_cerr3'    =>  array(
            '{color_cerr3} == 1'    =>  '{temp_ext}+3',
            '{color_cerr3} == 2'   =>  '{temp_ext}+4',
            '{color_cerr3} == 3'   =>  '{temp_ext}+5'
    ),
    'temp_ext_cerr3'    =>  array(
            '{tipo_cerr3} != 1 && {orientacion_cerr3} == 1'   =>  '{temp_ext_norte_cerr3}',
            '{tipo_cerr3} != 1 && {orientacion_cerr3} == 2'   =>  '{temp_ext_sur_cerr3}',
            '{tipo_cerr3} != 1 && {orientacion_cerr3} == 3'   =>  '{temp_ext_este_cerr3}',
            '{tipo_cerr3} != 1 && {orientacion_cerr3} == 4'   =>  '{temp_ext_oeste_cerr3}',
            '{tipo_cerr3} != 1 && {orientacion_cerr3} == 5'   =>  '({temp_ext_norte_cerr3}+{temp_ext_este_cerr3})/2',
            '{tipo_cerr3} != 1 && {orientacion_cerr3} == 6'   =>  '({temp_ext_norte_cerr3}+{temp_ext_oeste_cerr3})/2',
            '{tipo_cerr3} != 1 && {orientacion_cerr3} == 7'   =>  '({temp_ext_sur_cerr3}+{temp_ext_este_cerr3})/2',
            '{tipo_cerr3} != 1 && {orientacion_cerr3} == 8'   =>  '({temp_ext_sur_cerr3}+{temp_ext_oeste_cerr3})/2'
    ),
    'temp_ext_calc_cerr3'    =>  array(
            '{tipo_cerr3} == 1 || {tipo_ubicacion} == 1 || {cerr_ext} == 2'    =>  '{temp_recinto}',
            '{tipo_cerr3} == 2 && {tipo_ubicacion} == 2 && {cerr_ext} == 1'    =>  '{temp_ext_cerr3}',
            '{tipo_cerr3} == 3 && {tipo_ubicacion} == 2 && {cerr_ext} == 1'    =>  '{temp_ext_cerr3}-7',
            '{tipo_cerr3} == 4 && {tipo_ubicacion} == 2 && {cerr_ext} == 1'    =>  '{temp_ext_cerr3}-3'
    ),
    'tipo_cerr4'=>'0',
    'color_cerr4'=>'0',

    'orientacion_cerr4'    =>  array(
            '{orientacion_cerr1} == 1'    =>  '3',
            '{orientacion_cerr1} == 2'   =>  '4',
            '{orientacion_cerr1} == 3'   =>  '2',
            '{orientacion_cerr1} == 4'   =>  '1',
            '{orientacion_cerr1} == 5'   =>  '7',
            '{orientacion_cerr1} == 6'   =>  '5',
            '{orientacion_cerr1} == 7'   =>  '8',
            '{orientacion_cerr1} == 8'   =>  '6'
    ),

    'temp_ext_norte_cerr4'=>'{temp_ext}',
    'temp_ext_sur_cerr4'    =>  array(
            '{color_cerr4} == 1'    =>  '{temp_ext}+2',
            '{color_cerr4} == 2 || {color_cerr4} == 3'   =>  '{temp_ext}+3'
    ),
    'temp_ext_este_cerr4'    =>  array(
            '{color_cerr4} == 1'    =>  '{temp_ext}+3',
            '{color_cerr4} == 2'   =>  '{temp_ext}+4',
            '{color_cerr4} == 3'   =>  '{temp_ext}+5'
    ),
    'temp_ext_oeste_cerr4'    =>  array(
            '{color_cerr4} == 1'    =>  '{temp_ext}+3',
            '{color_cerr4} == 2'   =>  '{temp_ext}+4',
            '{color_cerr4} == 3'   =>  '{temp_ext}+5'
    ),
    'temp_ext_cerr4'    =>  array(
            '{tipo_cerr4} != 1 && {orientacion_cerr4} == 1'   =>  '{temp_ext_norte_cerr4}',
            '{tipo_cerr4} != 1 && {orientacion_cerr4} == 2'   =>  '{temp_ext_sur_cerr4}',
            '{tipo_cerr4} != 1 && {orientacion_cerr4} == 3'   =>  '{temp_ext_este_cerr4}',
            '{tipo_cerr4} != 1 && {orientacion_cerr4} == 4'   =>  '{temp_ext_oeste_cerr4}',
            '{tipo_cerr4} != 1 && {orientacion_cerr4} == 5'   =>  '({temp_ext_norte_cerr4}+{temp_ext_este_cerr4})/2',
            '{tipo_cerr4} != 1 && {orientacion_cerr4} == 6'   =>  '({temp_ext_norte_cerr4}+{temp_ext_oeste_cerr4})/2',
            '{tipo_cerr4} != 1 && {orientacion_cerr4} == 7'   =>  '({temp_ext_sur_cerr4}+{temp_ext_este_cerr4})/2',
            '{tipo_cerr4} != 1 && {orientacion_cerr4} == 8'   =>  '({temp_ext_sur_cerr4}+{temp_ext_oeste_cerr4})/2'
    ),
    'temp_ext_calc_cerr4'    =>  array(
            '{tipo_cerr4} == 1 || {tipo_ubicacion} == 1 || {cerr_ext} == 2'    =>  '{temp_recinto}',
            '{tipo_cerr4} == 2 && {tipo_ubicacion} == 2 && {cerr_ext} == 1'    =>  '{temp_ext_cerr4}',
            '{tipo_cerr4} == 3 && {tipo_ubicacion} == 2 && {cerr_ext} == 1'    =>  '{temp_ext_cerr4}-7',
            '{tipo_cerr4} == 4 && {tipo_ubicacion} == 2 && {cerr_ext} == 1'    =>  '{temp_ext_cerr4}-3'
    ),
    'tipo_cubierta'=>'0',
    'color_cubierta'=>'0',
    'temp_ext_cubierta'    =>  array(
            '{color_cubierta} == 1'    =>  '{temp_ext}+5',
            '{color_cubierta} == 2'   =>  '{temp_ext}+9',
            '{color_cubierta} == 3'   =>  '{temp_ext}+11'
    ),

    'temp_ext_calc_techo'    =>  array(
        '{tipo_cubierta} == 1 || {tipo_ubicacion} == 1 || {cerr_ext} == 2'    =>  '{temp_recinto}',
        '{tipo_cubierta} == 2 && {tipo_ubicacion} == 2 && {cerr_ext} == 1'    =>  '{temp_ext_cubierta}',
        '{tipo_cubierta} == 3 && {tipo_ubicacion} == 2 && {cerr_ext} == 1'    =>  '{temp_ext_cubierta}-7',
        '{tipo_cubierta} == 4 && {tipo_ubicacion} == 2 && {cerr_ext} == 1'    =>  '{temp_ext_cubierta}-3'
    ),
    'cond_cerr1'=>'0',
    'cond_cerr2'=>'0',
    'cond_cerr3'=>'0',
    'cond_cerr4'=>'0',
    'cond_techo'=>'0',
    'cond_suelo'=>'0',
    'cond_aisla_suelo'=>'0',
    'cond_cam_aire'=>'0.75',
    'cond_puerta'=>'0',
    'cond_ventana'=>'0',
    'h_int_pared'=>'9+3.7*{vel_aire_cam}',
    'h_int_techo'=>'11+3.7*{vel_aire_cam}',
    'h_int_suelo'=>'6+3.7*{vel_aire_cam}',

    'h_ext_pared'=>'9+3.7*{vel_aire_cam}',
    'h_ext_techo'=>'11+3.7*{vel_aire_cam}',
    'espe_cerr1'=>'1000*{cond_cerr1}*(({temp_ext_calc_cerr1}-({temp_cam}))/(0.0667*{temp_cam}+9)-1/{h_int_pared}-1/{h_ext_pared})',
    'espe_cerr2'=>'1000*{cond_cerr2}*(({temp_ext_calc_cerr2}-({temp_cam}))/(0.0667*{temp_cam}+9)-1/{h_int_pared}-1/{h_ext_pared})',
    'espe_cerr3'=>'1000*{cond_cerr3}*(({temp_ext_calc_cerr3}-({temp_cam}))/(0.0667*{temp_cam}+9)-1/{h_int_pared}-1/{h_ext_pared})',
    'espe_cerr4'=>'1000*{cond_cerr4}*(({temp_ext_calc_cerr4}-({temp_cam}))/(0.0667*{temp_cam}+9)-1/{h_int_pared}-1/{h_ext_pared})',
    'espe_techo'=>'1000*{cond_techo}*(({temp_ext_calc_techo}-({temp_cam}))/(0.0667*{temp_cam}+9)-1/{h_int_techo}-1/{h_ext_techo})',
    'espe_promedio'=>'({espe_cerr1}+{espe_cerr2}+{espe_cerr3}+{espe_cerr4}+{espe_techo})/5',
    'cerr_verticales'=>'0',
    'espe_cerr1_calc'    =>  array(
            '{cerr_verticales} == 1'    =>  '{espe_promedio}',
            '{cerr_verticales} == 2'   =>  '{espe_cerr1}'
    ),
    'espe_cerr2_calc'    =>  array(
            '{cerr_verticales} == 1'    =>  '{espe_promedio}',
            '{cerr_verticales} == 2'   =>  '{espe_cerr2}'
    ),
    'espe_cerr3_calc'    =>  array(
            '{cerr_verticales} == 1'    =>  '{espe_promedio}',
            '{cerr_verticales} == 2'   =>  '{espe_cerr3}'
    ),
    'espe_cerr4_calc'    =>  array(
            '{cerr_verticales} == 1'    =>  '{espe_promedio}',
            '{cerr_verticales} == 2'   =>  '{espe_cerr4}'
    ),
    'espe_techo_calc'    =>  array(
            '{cerr_verticales} == 1'    =>  '{espe_promedio}',
            '{cerr_verticales} == 2'   =>  '{espe_techo}'
    ),
    'espe_suelo'=>'150',
    'camara_de_aire'=>'0',
    'espe_cam_aire'    =>  array(
            '{camara_de_aire} == 1'    =>  '150',
            '{camara_de_aire} == 2'   =>  '0'
    ),
    'aislamiento'=>'0',
    'espe_aisla_suelo'    =>  array(
            '{aislamiento} == 1'    =>  '1000*{cond_aisla_suelo}*(({temp_suelo}-({temp_cam}))/(0.0667*{temp_cam}+9)-1/{h_int_suelo}-({espe_suelo}/1000)/{cond_suelo}-({espe_cam_aire}/1000)/{cond_cam_aire})',
            '{aislamiento} == 2'   =>  '0'
    ),
    'espe_puerta'=>'1000*{cond_puerta}*(({temp_ext_calc_cerr1}-({temp_cam}))/(0.0667*{temp_cam}+9)-1/{h_int_pared}-1/{h_ext_pared})',
    'material_ventanas'=>'0',
    'espe_ventana'    =>  array(
            '{material_ventanas} == 23'    =>  '6',
            '{material_ventanas} == 24'   =>  '20'
    ),
    'sup_puerta'=>'3',
    'ventanas'=>'0',
    'sup_ventana'    =>  array(
            '{ventanas} == 1'    =>  '2',
            '{ventanas} == 2'   =>  '0'
    ),
    'num_personas'    =>  array(
            '{idCamara} == 5'    =>  '{long_cam}*{ancho_cam}/5',
            '{idCamara} != 5'   =>  '1'
    ),
    'pot_persona'=>'-4.4286*{temp_cam}+332.86',
    'pot_pers_sens'=>'-6.4571*{temp_cam}+251.71',
    'pot_pers_lat'=>'2.0285*{temp_cam}+81.15',

    'tiempo_personas'    =>  array(
            '{idCamara} == 5'    =>  '12',
            '{idCamara} == 1 || {idCamara} >= 6'   =>  '1',
            '{idCamara} == 2'   =>  '0.5'
    ),

    'num_lamp'=>'20*{long_cam}*{ancho_cam}/15',
    'pot_lamp'=>'15',
    'tiempo_lamp'=>'{tiempo_personas}',
    'pot_m2_ilum'=>'20',
    'pot_vent'    =>  array(
            '{tipoEvap} == 1'    =>  '0',
            '{tipoEvap} == 2 && {volumen_cam} <= 100 && ({idCamara} == 1 || {idCamara} == 2 || {idCamara} == 5 || {idCamara} >= 6)'   =>  '10.45*{volumen_cam}+650',
            '{tipoEvap} == 2 && {volumen_cam} > 100 && ({idCamara} == 1 || {idCamara} == 2 || {idCamara} == 5 || {idCamara} >= 6)'   =>  '3.6679*{volumen_cam}+2331.6',
            '{tipoEvap} == 2 && {idCamara} == 3'   =>  '3.2434*{rend_tunel}+734.26',
            '{tipoEvap} == 2 && {idCamara} == 4'   =>  '18.706*{rend_tunel}+1570.1'
    ),
    'tiempo_vent'    =>  array(
            '{idCamara} == 5'   =>  '22',
            '{idCamara} == 1 || {idCamara} >= 6'   =>  '21',
            '{idCamara} == 2'   =>  '20'
    ),
    'pot_vent_estima'    =>  array(
            '{tipoEvap} == 1'    =>  '0',
            '{tipoEvap} == 2 && ({idCamara} == 1 || {idCamara} == 2 || {idCamara} == 5 || {idCamara} >= 6)'   =>  '10',
            '{tipoEvap} == 2 && {idCamara} == 3'   =>  '15',
            '{tipoEvap} == 2 && {idCamara} == 4'   =>  '20'
    ),
    'pot_resist'    =>  array(
            '{volumen_cam} <= 200 && ({idCamara} == 1 || {idCamara} == 6 || {idCamara} == 7)'    =>  '110*{volumen_cam}',
            '{volumen_cam} > 200 && ({idCamara} == 1 || {idCamara} == 6 || {idCamara} == 7)'   =>  '55.035*{volumen_cam}+11007',
            '{volumen_cam} <= 300 && {idCamara} == 2'   =>  '42.515*{volumen_cam}+1149.7',
            '{volumen_cam} > 300 && {idCamara} == 2'   =>  '21.485*{volumen_cam}+8537.5',
            '{idCamara} == 3'   =>  '29.859*{rend_tunel}+2795.9',
            '{idCamara} == 4'   =>  '145.54*{rend_tunel}+4941.2',
            '{idCamara} == 5 || {idCamara} == 8 || {idCamara} == 9 || {idCamara} == 10'   =>  '0'
    ),
    'tiempo_resist'    =>  array(
            '{idCamara} == 8 || {idCamara} == 9 || {idCamara} == 10'    =>  '0',
            '{idCamara} == 6 || {idCamara} == 7'   =>  '1'
    ),
    'pot_resist_estima'    =>  array(
            '{idCamara} == 6 || {idCamara} == 7'    =>  '5',
            '{idCamara} == 8 || {idCamara} == 9 || {idCamara} == 10'   =>  '0'
    ),
    'pot_otros'=>'0',
    'tiempo_otros'=>'0',
    'pot_otros_estima'=>'0',
    'horas_dia'    =>  array(
            '{idCamara} == 3 || {idCamara} == 4'    =>  '23',
            '{idCamara} == 5'   =>  '22',
            '{idCamara} == 1 || {idCamara} >= 6'   =>  '21',
            '{idCamara} == 2'   =>  '20'
    ),
    'mayor_segur'    =>  array(
            '{volumen_cam} <= 500'    =>  '10',
            '{volumen_cam} > 500'   =>  '5'
    ),

    'calculoA'=>'(1/(1/{h_int_pared}+1/{h_ext_pared}+({espe_cerr1_calc}/1000)/{cond_cerr1}))*({long_cam}*{alto_cam})*({temp_ext_calc_cerr1}-({temp_cam}))+(1/(1/{h_int_pared}+1/{h_ext_pared}+({espe_cerr2_calc}/1000)/{cond_cerr2}))*({ancho_cam}*{alto_cam})*({temp_ext_calc_cerr2}-({temp_cam}))+(1/(1/{h_int_pared}+1/{h_ext_pared}+({espe_cerr3_calc}/1000)/{cond_cerr3}))*({long_cam}*{alto_cam})*({temp_ext_calc_cerr3}-({temp_cam}))+(1/(1/{h_int_pared}+1/{h_ext_pared}+({espe_cerr4_calc}/1000)/{cond_cerr4}))*({ancho_cam}*{alto_cam})*({temp_ext_calc_cerr4}-({temp_cam}))+(1/(1/{h_int_techo}+1/{h_ext_techo}+({espe_techo_calc}/1000)/{cond_techo}))*({long_cam}*{ancho_cam})*({temp_ext_calc_techo}-({temp_cam}))+(1/(1/{h_int_suelo}+({espe_suelo}/1000)/{cond_suelo}+({espe_aisla_suelo}/1000)/{cond_aisla_suelo}+({espe_cam_aire}/1000)/{cond_cam_aire}))*({long_cam}*{ancho_cam})*({temp_suelo}-({temp_cam}))+(1/(1/{h_int_pared}+1/{h_ext_pared}+({espe_puerta}/1000)/{cond_puerta}))*({sup_puerta})*({temp_recinto}-({temp_cam}))-(1/(1/{h_int_pared}+1/{h_ext_pared}+({espe_cerr1_calc}/1000)/{cond_cerr1}))*({sup_puerta})*({temp_ext_calc_cerr1}-({temp_cam}))+(1/(1/{h_int_pared}+1/{h_ext_pared}+({espe_ventana}/1000)/{cond_ventana}))*({sup_ventana})*({temp_ext_calc_cerr1}-({temp_cam}))-(1/(1/{h_int_pared}+1/{h_ext_pared}+({espe_cerr1_calc}/1000)/{cond_cerr1}))*({sup_ventana})*({temp_ext_calc_cerr1}-({temp_cam}))',
    'calculoB'=>'{renov_infiltra}*{volumen_cam}*({entalpia_ext}-({entalpia_cam}))/({volumen_espe_cam}*24*3600)',
    'calculoB1'=>'{renov_infiltra}*{volumen_cam}*({entalpia_ext_sens}-({entalpia_cam_sens}))/({volumen_espe_cam}*24*3600)',
    'calculoB2'=>'{renov_infiltra}*{volumen_cam}*({entalpia_ext_lat}-({entalpia_cam_lat}))/({volumen_espe_cam}*24*3600)',
    'calculoC'=>'{renov_aire_calc}*{volumen_cam}*({entalpia_ext}-({entalpia_cam}))/({volumen_espe_cam}*24*3600)',
    'calculoC1'=>'{renov_aire_calc}*{volumen_cam}*({entalpia_ext_sens}-({entalpia_cam_sens}))/({volumen_espe_cam}*24*3600)',
    'calculoC2'=>'{renov_aire_calc}*{volumen_cam}*({entalpia_ext_lat}-({entalpia_cam_lat}))/({volumen_espe_cam}*24*3600)',

    'calculoD'    =>  array(
            '{idCamara} == 1'    =>  '(1+{mayor_embalaje}/100)*{capacidad_cam}*({rotacion}/100)*{calor_esp_fres}*({temp_inicial}-({temp_final}))/(24*3600)',
            '{idCamara} == 2'    =>  '(1+{mayor_embalaje}/100)*{capacidad_cam}*({rotacion}/100)*{calor_esp_cong_calc}*({temp_inicial}-({temp_final}))/(24*3600)',
            '{idCamara} == 3'   =>  '(1+{mayor_embalaje}/100)*{rend_tunel}*{calor_esp_fres}*({temp_inicial}-({temp_final}))/3600',
            '{idCamara} == 4'   =>  '(1+{mayor_embalaje}/100)*{rend_tunel}*({calor_esp_fres}*({temp_inicial}-({temp_cong}))+{calor_lat_cong}+{calor_esp_cong_calc}*({temp_cong}-({temp_final})))/3600',
            '{idCamara} == 5'    =>  '(1+{mayor_embalaje}/100)*{capacidad_cam}*({rotacion}/100)*{calor_esp_fres}*({temp_inicial}-({temp_final}))/(24*3600)',
            '{idCamara} >= 6'    =>  '(1+{mayor_embalaje}/100)*{capacidad_cam}*{calor_esp_fres}*({temp_inicial}-({temp_final}))/({duracion_secado}*24*3600)+1000*{capacidad_cam}*({merma_producto}/100)*(2501-2.38*{temp_cam})/({duracion_secado}*24*3600)',
    ),
    'calculoD2'    =>  array(
            '{idCamara} < 6'    =>  '0',
            '{idCamara} >= 6'    =>  '1000*{capacidad_cam}*({merma_producto}/100)*(2501-2.38*{temp_cam})/({duracion_secado}*24*3600)',
    ),
    'calculoD1'=>'{calculoD}- ({calculoD2})',

    'calculoE'    =>  array(
            '{idCamara} == 1 || {idCamara} == 2 || {idCamara} == 5 || {idCamara} >= 6'    =>  '({capacidad_cam}*{rotacion}/100)*{pot_respi_tmedia}+({capacidad_cam}-({capacidad_cam})*{rotacion}/100)*{pot_respi_tfinal}',
            '{idCamara} == 3'   =>  '{rend_tunel}*({tiempo_enf}/60)*{pot_respi_tmedia}',
            '{idCamara} == 4'   =>  '{rend_tunel}*({tiempo_cong}/60)*{pot_respi_tmedia}'
    ),

    'calculoF'=>'{pot_persona}*{num_personas}*{tiempo_personas}/24',
    'calculoF1'=>'{pot_pers_sens}*{num_personas}*{tiempo_personas}/24',
    'calculoF2'=>'{pot_pers_lat}*{num_personas}*{tiempo_personas}/24',

    'tipoNumPot'=>'0',
    'tipoLamp'=>'0',
    'calculoG'    =>  array(
            '{tipoNumPot} == 1 && {tipoLamp} == 1'    =>  '{num_lamp}*{pot_lamp}*{tiempo_lamp}/24',
            '{tipoNumPot} == 1 && {tipoLamp} == 2'   =>  '{num_lamp}*{pot_lamp}*1.25*{tiempo_lamp}/24',
            '{tipoNumPot} == 2'   =>  '{pot_m2_ilum}*{tiempo_lamp}*{ancho_cam}*{long_cam}/24'
    ),

    'potVentEst'=>'0',
    'calculoH'    =>  array(
            '{tipoEvap} == 2 && {potVentEst} == 1'    =>  '{pot_vent}*{tiempo_vent}/24',
            '{tipoEvap} == 2 && {potVentEst} == 2'   =>  '({pot_vent_estima}/100)*({calculoA}+{calculoB}+{calculoC}+{calculoD})'
    ),

    'potResisEst'=>'0',
    'calculoI'    =>  array(
            '{potResisEst} == 1'    =>  '{pot_resist}*{tiempo_resist}/24',
            '{potResisEst} == 2'   =>  '({pot_resist_estima}/100)*({calculoA}+{calculoB}+{calculoC}+{calculoD})'
    ),

    'potConEst'=>'0',
    'calculoJ'    =>  array(
            '{potConEst} == 1'    =>  '{pot_otros}*{tiempo_otros}/24',
            '{potConEst} == 2'   =>  '({pot_otros_estima}/100)*({calculoA}+{calculoB}+{calculoC}+{calculoD})'
    ),
    'carga_total'=>'{calculoA}+{calculoB}+{calculoC}+{calculoD}+{calculoE}+{calculoF}+{calculoG}+{calculoH}+{calculoI}+{calculoJ}',
    'carga_sens'=>'{calculoA}+{calculoB1}+{calculoC1}+{calculoD1}+{calculoE}+{calculoF1}+{calculoG}+{calculoH}+{calculoI}+{calculoJ}',
    'carga_lat'=>'{carga_total} - {carga_sens}',
    'porcentaje_calculoA'=>'({calculoA}/ {carga_total})* 100',
    'porcentaje_calculoB'=>'({calculoB}/ {carga_total})* 100',
    'porcentaje_calculoC'=>'({calculoC}/ {carga_total})* 100',
    'porcentaje_calculoD'=>'({calculoD}/ {carga_total})* 100',
    'porcentaje_calculoE'=>'({calculoE}/ {carga_total})* 100',
    'porcentaje_calculoF'=>'({calculoF}/ {carga_total})* 100',
    'porcentaje_calculoG'=>'({calculoG}/ {carga_total})* 100',
    'porcentaje_calculoH'=>'({calculoH}/ {carga_total})* 100',
    'porcentaje_calculoI'=>'({calculoI}/ {carga_total})* 100',
    'porcentaje_calculoJ'=>'({calculoJ}/ {carga_total})* 100',
    'porcentaje_carga_sens'=>'({carga_sens}/ {carga_total})* 100',
    'porcentaje_carga_lat'=>'({carga_lat}/ {carga_total})* 100',
    'porcen_carga_sens'=>'{carga_sens}/{carga_total}',
    'porcen_carga_lat'=>'{carga_lat}/{carga_total}',
    'pot_frig_cam'    =>  array(
            '{porcen_carga_sens} == {d_entalpia_evap_sens}'    =>  '{carga_sens}*(24/{horas_dia})*(1+{mayor_segur}/100)+{carga_lat}*(24/{horas_dia})*(1+{mayor_segur}/100)',
            '{porcen_carga_sens} < {d_entalpia_evap_sens}'   =>  '((({carga_total}*{porcen_carga_lat}/{d_entalpia_evap_lat})-({carga_lat}))*(24/({horas_dia}*{porcen_carga_sens}))*(1+{mayor_segur}/100))+({carga_lat}*(24/({horas_dia}+{porcen_carga_lat}))*(1+{mayor_segur}/100))',
            '{porcen_carga_sens} > {d_entalpia_evap_sens}'   =>  '({carga_sens}*(24/({horas_dia}*{porcen_carga_sens}))*(1+{mayor_segur}/100))+((({carga_total}*{porcen_carga_sens}/{d_entalpia_evap_sens})-({carga_sens}))*(24/({horas_dia}*{porcen_carga_lat}))*(1+{mayor_segur}/100))'
    ),
    'pot_frig_cam_sens'    =>  array(
            '{porcen_carga_sens} == {d_entalpia_evap_sens}'    =>  '{pot_frig_cam}*{porcen_carga_sens}',
            '{porcen_carga_sens} < {d_entalpia_evap_sens} || {porcen_carga_sens} > {d_entalpia_evap_sens}'   =>  '{pot_frig_cam}*{d_entalpia_evap_sens}'
    ),
    'pot_frig_cam_lat'    =>  array(
            '{porcen_carga_sens} == {d_entalpia_evap_sens}'    =>  '{pot_frig_cam}*{porcen_carga_lat}',
            '{porcen_carga_sens} < {d_entalpia_evap_sens} || {porcen_carga_sens} > {d_entalpia_evap_sens}'   =>  '{pot_frig_cam}*{d_entalpia_evap_lat}'
    ),
);


?>