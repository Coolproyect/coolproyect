<?php
require_once "vendor/autoload.php";
include 'app/core.php';

$dataTrim = $_SESSION['campos'];

$templateProcessor = new \PhpOffice\PhpWord\TemplateProcessor('informe_resultados.docx');
foreach ($dataTrim as $key => $value) {
    $templateProcessor->setValue($key, $value);
}

if($dataTrim["tipoEvap"] == 2){
    $templateProcessor->setValue("evaporadorVentEst", "ΔT evaporador ventilado (K): ".$dataTrim["dt_vent"]);
}
else{
    $templateProcessor->setValue("evaporadorVentEst", "ΔT evaporador estático (K): ".$dataTrim["dt_est"]);
}

header("Cache-Control: public");
header("Content-Description: File Transfer");
header("Content-Disposition: attachment; filename=informe_resultados_" .$dataTrim["nombre_proyecto"].".docx");
header("Content-Transfer-Encoding: binary");
$templateProcessor->saveAs('php://output');