<link href="assets/libs/bootstrap/css/bootstrap.min.css" rel="stylesheet" />
<link href="assets/libs/wizard/bootstrap-wizard.css" rel="stylesheet" />
<link href="assets/libs/chosen/chosen.css" rel="stylesheet" />
<link href="assets/libs/datepicker/css/datepicker.css" rel="stylesheet" />
<link href="assets/libs/jquery-confirm/jquery-confirm.css" rel="stylesheet" />
<link href="assets/css/custom.css" rel="stylesheet" />

<!-- Font Awesome CSS -->
<link rel="stylesheet" href="assets/libs/fontawesome/css/font-awesome.min.css" type="text/css" media="screen">

<!-- Slicknav -->
<link rel="stylesheet" type="text/css" href="assets/css/slicknav.css" media="screen">

<!-- Margo CSS Styles  -->
<link rel="stylesheet" type="text/css" href="assets/css/style.css" media="screen">

<!-- Responsive CSS Styles  -->
<link rel="stylesheet" type="text/css" href="assets/css/responsive.css" media="screen">

<!-- Css3 Transitions Styles  -->
<link rel="stylesheet" type="text/css" href="assets/css/animate.css" media="screen">

<!-- Color CSS Styles  -->
<link rel="stylesheet" type="text/css" href="assets/css/colors/blue.css" title="blue" media="screen" />
<link rel="stylesheet" type="text/css" href="assets/css/colors/jade.css" title="jade" media="screen" />
<link rel="stylesheet" type="text/css" href="assets/css/colors/green.css" title="green" media="screen" />
<link rel="stylesheet" type="text/css" href="assets/css/colors/beige.css" title="beige" media="screen" />
<link rel="stylesheet" type="text/css" href="assets/css/colors/cyan.css" title="cyan" media="screen" />
<link rel="stylesheet" type="text/css" href="assets/css/colors/orange.css" title="orange" media="screen" />
<link rel="stylesheet" type="text/css" href="assets/css/colors/peach.css" title="peach" media="screen" />
<link rel="stylesheet" type="text/css" href="assets/css/colors/pink.css" title="pink" media="screen" />
<link rel="stylesheet" type="text/css" href="assets/css/colors/purple.css" title="purple" media="screen" />
<link rel="stylesheet" type="text/css" href="assets/css/colors/sky-blue.css" title="sky-blue" media="screen" />
<link rel="stylesheet" type="text/css" href="assets/css/colors/yellow.css" title="yellow" media="screen" />

<link rel="stylesheet" href="assets/libs/alertify/css/alertify.min.css">
<link rel="stylesheet" href="assets/libs/alertify/css/themes/semantic.css">

