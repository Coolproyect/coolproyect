
		<script src="assets/js/jquery-2.0.3.min.js" type="text/javascript"></script>
		<script src="assets/js/app.js"></script>
		<script src="assets/libs/jqueryValidate/jquery.validate.min.js"></script>
		<script src="assets/libs/jqueryValidate/localization/messages_es.min.js"></script>
		<?php if ( !is_page('login.php')): ?>
					<script src="assets/libs/chosen/chosen.jquery.js"></script>
					<script src="assets/libs/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
					<script src="assets/js/prettify.js" type="text/javascript"></script>
					<script src="assets/libs/wizard/bootstrap-wizard.js" type="text/javascript"></script>
			        <script src="assets/libs/datepicker/js/bootstrap-datepicker.js" type="text/javascript"></script>
			    	<script src="assets/libs/jquery-confirm/jquery-confirm.js" type="text/javascript"></script>
			        <script src="assets/js/coolProRoom.js"></script>
			        <script src="assets/libs/alertify/alertify.min.js"></script>
			        <script src="assets/libs/html2canvas.js"></script>
			        <script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.5.3/jspdf.debug.js" integrity="sha384-NaWTHo/8YCBYJ59830LTz/P4aQZK1sS0SneOgAvhsIl3zBu8r9RevNg5lHCHAuQ/" crossorigin="anonymous"></script>
		<?php endif; ?>
	</body>
</html>
