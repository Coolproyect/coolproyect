<div class="wizard-card col-md-6" data-cardname="name">
    <input type="hidden" name="id_proyecto">
    <h3>1. <?php _e('Identificación del proyecto') ?></h3>

    <div class="wizard-input-section">
        <p>
            <?php _e('Título del proyecto') ?>
        </p>
        <div class="form-group">
            <div class="col-sm-8">
                <input type="text" class="form-control" id="" name="nombre_proyecto" placeholder="<?php _e('Título del proyecto') ?>" data-validate="validateField" default="a">
            </div>
        </div>
    </div>
    <div class="wizard-input-section">
        <p>
            <?php _e('Fecha') ?>
        </p>


        <div class="form-group">
            <div class="col-sm-8">
                <input type="text" class="form-control datepicker" id="fecha_proyecto" name="fecha_proyecto" placeholder="<?php _e('Fecha') ?>" data-validate="">

            </div>
        </div>
    </div>

    <div class="wizard-input-section">
        <p>
            <?php _e('Proyectista') ?>
        </p>

        <div class="form-group">
            <div class="col-sm-8">
                <input type="text" class="form-control" id="nombre_proyectista" name="nombre_proyectista" placeholder="<?php _e('Proyectista') ?>" data-validate="">
            </div>
        </div>
    </div>

    <div class="wizard-input-section">
        <p>
            <?php _e('Cliente') ?>
        </p>
        <div class="form-group">
            <div class="col-sm-8">
                <input type="text" class="form-control" id="nombre_cliente" name="nombre_cliente" placeholder="<?php _e('Cliente') ?>" data-validate="">
            </div>
        </div>
    </div>

    <div class="wizard-input-section">
        <p>
            <?php _e('Observaciones') ?>
        </p>
        <div class="form-group">
            <div class="col-sm-8">
                <textarea class="form-control" id="" name="observ_proyecto" placeholder="<?php _e('Observaciones') ?>" data-validate=""></textarea>
            </div>
        </div>
    </div>

</div>
