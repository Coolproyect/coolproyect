<div class="wizard-card">
    <h3>5. <?php _e('Dimensiones de la cámara') ?></h3>
    <div class="wizard-input-section col-md-4">

        <img class="img-responsive imagenArriba" src="assets/images/dimensiones.png" alt="<?php _e('Cámara de congelados') ?>">
        <img class="img-responsive imagenAbajo" src="assets/images/camara.png" alt="">

    </div>

    <div class="wizard-input-section col-md-8">
        <div class="form-group">
            <label class="control-label col-sm-7" for="longCam"><?php _e('Longitud cámara (m)') ?></label>
            <div class="col-sm-5">
                <input type="number" class="form-control" id="longCam" placeholder="<?php _e('Longitud cámara (m)') ?>" data-validate="validateField" value="2" min="0.1" step="0.01" max="30" name="long_cam">
            </div>
        </div>

        <div class="form-group">
            <label class="control-label col-sm-7" for="anchCam"><?php _e('Anchura cámara (m)') ?></label>
            <div class="col-sm-5">
                <input type="number" class="form-control" id="anchCam" placeholder="<?php _e('Anchura cámara (m)') ?>" data-validate="validateField" value="2" min="0.1" max="30" name="ancho_cam">
            </div>
        </div>

        <div class="form-group">
            <label class="control-label col-sm-7" for="altCam"><?php _e('Altura cámara (m)') ?></label>
            <div class="col-sm-5">
                <input type="number" class="form-control" id="altCam" placeholder="<?php _e('Altura cámara (m)') ?>" data-validate="validateField" value="2" min="0.1" max="20" name="alto_cam">
            </div>
        </div>

        <div class="form-group">
            <label class="control-label col-sm-7" for="voluCam"><?php _e('Volumen cámara (m3)') ?></label>
            <div class="col-sm-5">
                <input type="number" class="form-control" id="voluCam" placeholder="<?php _e('Altura cámara (m3)') ?>" data-validate="validateField" min="5" max="18000" name="volumen_cam">
            </div>
        </div>

        <div class="form-group densNetAlmac oculto active-secadero active-congelados active-refrigerados">
            <label class="control-label col-sm-7" for="densNetAlm"><?php _e('Densidad neta almacenamiento (kg/m3)') ?></label>
            <div class="col-sm-5">
                <input type="number" class="form-control" id="densNetAlm" placeholder="<?php _e('Densidad neta almacenamiento (kg/m3)') ?>" data-validate="validateField" min="0" max="2000" name="densi_almacen">
            </div>
        </div>

        <div class="form-group">
            <label class="control-label col-sm-7" for="capAlm"><?php _e('Capacidad almacenamiento (kg)') ?></label>
            <div class="col-sm-5">
                <input type="number" class="form-control" id="capAlm" placeholder="<?php _e('Capacidad almacenamiento (kg)') ?>" data-validate="validateField" min="0" max="2000000" name="capacidad_cam">
            </div>
        </div>

        <div class="form-group active-secadero active-congelados active-refrigerados active-sala-trabajo oculto">
            <label class="control-label col-sm-7" for="rotDiaria"><?php _e('Rotación diaria (%)') ?></label>
            <div class="col-sm-5 rotacion_tipo">
                <!--<select class='form-control' id='rotDiaria'>
                    <option value="1">Alta</option>
                    <option value="2">Media</option>
                    <option value="3">Baja</option>
                </select>-->

                <label class="radio-inline">
                    <input type="radio" name="rotacion_tipo" id='rotDiaria' value="1"><?php _e('Alta') ?>
                </label>
                <label class="radio-inline">
                    <input type="radio" name="rotacion_tipo" value="2" checked><?php _e('Media') ?>
                </label>
                <label class="radio-inline">
                    <input type="radio" name="rotacion_tipo" value="3"><?php _e('Baja') ?>
                </label>
            </div>
            <div class="col-sm-5 rotacion">
                <input type="number" class="form-control" placeholder="<?php _e('´Rotación') ?>" data-validate="validateField" min="0" max="100" name="rotacion">
            </div>
        </div>

        <div class="form-group active-tunel-enfriamiento oculto">
            <label class="control-label col-sm-7" for="tiempo_enf"><?php _e('Tiempo de enfriamiento (min)') ?></label>
            <div class="col-sm-5">
                <input type="number" class="form-control" placeholder="<?php _e('Tiempo de enfriamiento (min)') ?>" data-validate="validateField" min="0" max="1440" name="tiempo_enf">
            </div>
        </div>

        <div class="form-group active-tunel-congelacion oculto">
            <label class="control-label col-sm-7" for="tiempo_cong"><?php _e('Tiempo de congelación (min)') ?></label>
            <div class="col-sm-5">
                <input type="number" class="form-control" placeholder="<?php _e('Tiempo de congelación (min)') ?>" data-validate="validateField" min="0" max="1440" name="tiempo_cong">
            </div>
        </div>

        <div class="form-group active-secadero active-congelados active-refrigerados active-sala-trabajo oculto">
            <label class="control-label col-sm-7"><?php _e('Carga diaria (kg/día)') ?></label>
            <div class="col-sm-5">
                <input type="number" class="form-control" placeholder="<?php _e('Carga diaria (kg/día)') ?>" data-validate="validateField" min="0" max="2000000" name="carga_diaria">
            </div>
        </div>

        <div class="form-group active-tunel-congelacion active-tunel-enfriamiento oculto">
            <label class="control-label col-sm-7" for="rendTunel"><?php _e('Rendimiento Túnel (kg/h)') ?></label>
            <div class="col-sm-5">
                <input type="number" class="form-control" id="rendTunel" placeholder="<?php _e('Rendimiento Túnel (kg/h)') ?>" data-validate="validateField" min="0" max="2000000" value="6000" name="rend_tunel">

            </div>
        </div>

        <div class="form-group">
            <label class="control-label col-sm-7" for="mayor_embalaje"><?php echo _e('Mayoración carga térmica embalajes y palets') ?> (%)</label>
            <div class="col-sm-5">
                <input type="number" class="form-control" name="mayor_embalaje" data-validate="validateField" min="0" max="100" id="mayor_embalaje" placeholder="<?php echo _e('Mayoración carga térmica embalajes y palets') ?> (%)">
            </div>
        </div>

        <div class="form-group">
            <label class="control-label col-sm-7" for="dobPuerCor"><?php _e('Doble puerta o cortina aire') ?></label>
            <div class="col-sm-5">
                <!--<select class='form-control' id='dobPuerCor'>
                    <option value="1">Si</option>
                    <option value="2">No</option>
                </select>-->

                <label class="radio-inline">
                    <input type="radio" name="doble_puerta" data-validate="" value="1"><?php _e('Si') ?>
                </label>
                <label class="radio-inline">
                    <input type="radio" name="doble_puerta" value="2" checked><?php _e('No') ?>
                </label>
            </div>
        </div>

        <div class="form-group active-tunel-congelacion active-tunel-congelaciontunel-congelacion oculto">
            <label class="control-label col-sm-7" for="numPuerTunel">
                <?php _e('Nº de puertas del túnel') ?>
            </label>
            <div class="col-sm-5">
                <!--<select class='form-control' id='numPuerTunel'>
                    <option value="1">1</option>
                    <option value="2">2</option>
                </select>-->

                <label class="radio-inline">
                    <input type="radio" name="num_puertas_tunel" value="1" checked>1
                </label>
                <label class="radio-inline">
                    <input type="radio" name="num_puertas_tunel" value="2">2
                </label>
            </div>
        </div>

        <div class="form-group">
            <label class="control-label col-sm-7" for="numRenov"><?php _e('Nº renovaciones diarias infiltración') ?></label>
            <div class="col-sm-5">
                <input type="number" class="form-control" id="numRenov" placeholder="<?php _e('Nº renovaciones diarias infiltración') ?>" data-validate="validateField" min="0" max="10000" value="1.5" name="renov_infiltra">

            </div>
        </div>

        <div class="form-group active-refrigerados active-sala-trabajo oculto">
            <label class="control-label col-sm-7" for="renov_aire_calc"><?php echo _e('Nº renovaciones adicionales ventilación') ?></label>
            <div class="col-sm-5">
                <input type="number" class="form-control" name="renov_aire_calc" data-validate="validateField" min="0" max="10000" id="renov_aire_calc" placeholder="<?php echo _e('Nº renovaciones adicionales ventilación') ?> ">
            </div>
        </div>

        <div class="form-group oculto ventilacion_adiccional">
            <label class="control-label"><?php _e('* El producto requiere ') ?><label id="renov_adiccional"></label><?php _e(' renovaciones diarias de aire adicionales para su ventilación, ya que las renovaciones por infiltración son insuficientes. Podría ser recomendable un sistema de ventilación adicional. Pulse en “Incluir ventilación adicional” si piensa instalar un sistema de ventilación para cubrir las renovaciones requeridas.') ?></label>
            <label class="control-label col-sm-7"></label>
            <div class="col-sm-5">
                <a class="btn btn-info btn" id="renov_aire_adiccional"><?php _e('Incluir ventilación adicional') ?></a>

            </div>
        </div>
    </div>
</div>
