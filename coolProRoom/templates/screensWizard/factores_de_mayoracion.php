<div class="wizard-card">



    <h3>12. <?php _e('Factores de mayoración') ?></h3>
    <div class="wizard-input-section col-md-3">
        <img class="img-responsive imagenArriba" src="assets/images/factores_de_mayoracion.png" alt="">
    </div>

    <div class="wizard-input-section col-md-9">
        <div class="form-group">
            <label class="control-label col-sm-7" for="horas_dia"><?php _e('Tiempo de funcionamiento de la instalación (h/día)') ?></label>
            <div class="col-sm-5">
                <input type="number" class="form-control" placeholder="<?php _e('Tiempo de funcionamiento de la instalación (h/día)') ?>" data-validate="validateField" min="0" max="24" name="horas_dia">
            </div>
        </div>

        <div class="form-group">
            <label class="control-label col-sm-7" for="mayor_segur"><?php _e('Factor seguridad ') ?>(%)</label>
            <div class="col-sm-5">
                <input type="number" class="form-control" placeholder="<?php _e('Factor seguridad ') ?>(%)" data-validate="validateField" min="0" max="100" name="mayor_segur">
            </div>
        </div>

    </div>
</div>
