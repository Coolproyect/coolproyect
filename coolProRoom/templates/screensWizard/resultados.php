<div class="wizard-card resultadosPDF">
    <h3 data-html2canvas-ignore="true"><?php _e('Resultados') ?></h3>
    <a href="#" class="btn btn-primary pull-right imprimir" data-html2canvas-ignore="true" style="display: inline;"><i class="fa fa-print" aria-hidden="true"></i> Imprimir</a>
    <div class="wizard-input-section col-md-12">
        <table class="table table-hover">
            <thead>
            <tr>
                <th class="col-md-6">Resultados de cálculo</th>
                <th class="col-md-2 text-center">Sensibles</th>
                <th class="col-md-2 text-center">Latentes</th>
                <th class="col-md-2 text-center">% del total</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td>A. Cargas térmicas a través de cerramientos (W)</td>
                <td id="calculoA" class="resultados text-center"></td>
                <td class="text-center">0</td>
                <td id="porcentaje_calculoA" class="resultados ent text-center"></td>
            </tr>
            <tr>
                <td>B. Cargas térmicas por infiltraciones (W)</td>
                <td id="calculoB1" class="resultados text-center"></td>
                <td id="calculoB2" class="resultados text-center"></td>
                <td id="porcentaje_calculoB" class="resultados ent text-center"></td>
            </tr>
            <tr>
                <td>C. Cargas térmicas por renovación de aire (W)</td>
                <td id="calculoC1" class="resultados text-center"></td>
                <td id="calculoC2" class="resultados text-center"></td>
                <td id="porcentaje_calculoC" class="resultados ent text-center"></td>
            </tr>

            <tr>
                <td>D. Cargas térmicas por enfriamiento producto (W)</td>
                <td id="calculoD1" class="resultados text-center"></td>
                <td id="calculoD2" class="resultados text-center"></td>
                <td id="porcentaje_calculoD" class="resultados ent text-center"></td>
            </tr>

            <tr>
                <td>E. Cargas térmicas por respiración producto (W)</td>
                <td id="calculoE" class="resultados text-center"></td>
                <td class="text-center">0</td>
                <td id="porcentaje_calculoE" class="resultados ent text-center"></td>
            </tr>

            <tr>
                <td>F. Cargas térmicas por ocupación personas (W)</td>
                <td id="calculoF1" class="resultados text-center"></td>
                <td id="calculoF2" class="resultados text-center"></td>
                <td id="porcentaje_calculoF" class="resultados ent text-center"></td>
            </tr>

            <tr>
                <td>G. Cargas térmicas por iluminación (W)</td>
                <td id="calculoG" class="resultados text-center"></td>
                <td class="text-center">0</td>
                <td id="porcentaje_calculoG" class="resultados ent text-center"></td>
            </tr>

            <tr>
                <td>H. Cargas térmicas por ventiladores evaporador (W)</td>
                <td id="calculoH" class="resultados text-center"></td>
                <td class="text-center">0</td>
                <td id="porcentaje_calculoH" class="resultados ent text-center"></td>
            </tr>
            <tr>
                <td>I. Cargas térmicas por desescarche evaporador (W)</td>
                <td id="calculoI" class="resultados text-center"></td>
                <td class="text-center">0</td>
                <td id="porcentaje_calculoI" class="resultados ent text-center"></td>
            </tr>
            <tr>
                <td>J. Otras cargas térmicas (W)</td>
                <td id="calculoJ" class="resultados text-center"></td>
                <td class="text-center">0</td>
                <td id="porcentaje_calculoJ" class="resultados ent text-center"></td>
            </tr>

            <tr class="cargaTotal">
                <td>Carga térmica total (W)</td>
                <td id="carga_total" class="resultados text-center" colspan="2"></td>
                <td class="text-center">100</td>
            </tr>
            <tr>
                <td class="cargaSensibleLatente">Carga térmica total sensible (W)</td>
                <td id="carga_sens" class="resultados text-center"></td>
                <td class="text-center">0</td>
                <td id="porcentaje_carga_sens" class="resultados ent text-center"></td>
            </tr>
            <tr>
                <td class="cargaSensibleLatente">Carga térmica total latente (W)</td>
                <td class="text-center">0</td>
                <td id="carga_lat" class="resultados text-center"></td>
                <td id="porcentaje_carga_lat" class="resultados ent text-center"></td>
            </tr>
            </tbody>
        </table>
    </div>

    <div class="wizard-input-section col-md-12">
        <table class="table table-hover">
            <thead>
            <tr>
                <th class="col-md-8">Potencia frigorífica cámara</th>
                <th class="col-md-4"></th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td>
                    <?php _e('Horas diarias de funcionamiento instalación (h/día)') ?>
                </td>
                <td id="horas_dia" class="resultados"></td>
            </tr>
            <tr>
                <td>
                    <?php _e('Factor de seguridad (%)') ?>
                </td>
                <td id="mayor_segur" class="resultados"></td>
            </tr>
            <tr class="potenciaTotal">
                <td>
                    <?php _e('Potencia frigorífica cámara (W)') ?>
                </td>
                <td id="pot_frig_cam" class="resultados ent"></td>
            </tr>
            <tr>
                <td class="potenciaSensibleLantete">
                    <?php _e('Sensible (W)') ?>
                </td>
                <td id="pot_frig_cam_sens" class="resultados ent"></td>
            </tr>
            <tr>
                <td class="potenciaSensibleLantete">
                    <?php _e('Latente (W)') ?>
                </td>
                <td id="pot_frig_cam_lat" class="resultados ent"></td>
            </tr>

            <tr class="tiempoEnfCong active-tunel-enfriamiento oculto">
                <td>
                    <?php _e('Tiempo de enfriamiento (min)') ?>
                </td>
                <td id="tiempo_enf" class="resultados ent"></td>
            </tr>
            <tr class="tiempoEnfCong active-tunel-congelacion oculto">
                <td>
                    <?php _e('Tiempo de congelación (min)') ?>
                </td>
                <td id="tiempo_cong" class="resultados ent"></td>
            </tr>
            </tbody>
        </table>
    </div>

    <div class="wizard-input-section col-md-12">
        <table class="table table-hover">
            <thead>
            <tr>
                <th class="col-md-8">
                    <?php _e('Recomendaciones de diseño') ?>
                </th>
                <th class="col-md-4"></th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td>
                    <?php _e('Tª cámara (ºC)') ?>
                </td>
                <td id="temp_cam" class="resultados"></td>
            </tr>
            <tr>
                <td>
                    <?php _e('H.R. cámara (%)') ?>
                </td>
                <td id="hum_cam" class="resultados"></td>
            </tr>
            <tr class="active-tipoEvap1 oculto">
                <td>ΔT evaporador estático (K)</td>
                <td id="dt_est" class="resultados ent"></td>
            </tr>
            <tr class="active-tipoEvap2 oculto">
                <td>ΔT evaporador ventilado (K)</td>
                <td id="dt_vent" class="resultados ent"></td>
            </tr>
            </tbody>
        </table>
    </div>


</div>
