<div class="wizard-card" data-cardname="group">
    <h3>2. <?php _e('Emplazamiento') ?></h3>
    <div class="wizard-input-section">
        <label for="provincias"><?php _e('Seleccione una ciudad') ?>:</label>
        <div class="form-group">
            <div class="col-md-5">
        <select class="form-control" id="idProvincia" style="width:350px;" data-validate="validarProvincias" change-hidden="provinciaValue">
            <option value='0'><?php _e('Seleccione') ?>...</option>
            <?php
            $bd->getColumnOrderBy('pais.nombre AS pais_nombre, provincia.id, provincia.nombre', '`provincia`, `pais`', 'provincia.id_pais= pais.id', 'pais.nombre, provincia.nombre');
            $datos = $bd->getRowsSelect();
            $pais= "";
            foreach ($datos as $prov) {
                if($pais != $prov["pais_nombre"]){
                    if(!$pais== ""){
                        echo "</optgroup>";
                    }
                    $pais= $prov["pais_nombre"];
                    echo "<optgroup label='$pais'>";
                }
                $id=$prov["id"];
                echo " <option value='$id'>".$prov["nombre"]."</option>";
            }
            ?>
        </select>
        <input type="hidden" class="provinciaValue" name="idProvincia">
            </div>
            </div>
        </div>
    <div class="wizard-input-section">

        <label for="tempExt"><?php _e('Tª exterior (ºC)') ?></label>
        <div class="form-group">
            <div class="col-md-5">
                <input type="number" class="form-control" name="temp_ext" data-validate="validateField" min="-100" max="60" id="tempExt">
            </div>
        </div>
    </div>

    <div class="wizard-input-section">
        <!--<p>
            H.R. exterior (%)
        </p>-->

        <label for="humExt"><?php _e('H.R. exterior (%)') ?> </label>
        <div class="form-group">
            <div class="col-md-5">
                <input type="number" class="form-control" name="hum_ext" data-validate="validateField" min="0" max="100" id="humExt">
            </div>
        </div>
    </div>

    <div class="wizard-input-section">
        <!--<p>
            Tª suelo (ºC)
        </p>-->

        <label for="tempSuelo"><?php _e('Tª suelo (ºC)') ?></label>
        <div class="form-group">
            <div class="col-md-5">
                <input type="number" class="form-control" name="temp_suelo" data-validate="validateField" min="-100" max="100" id="tempSuelo">
            </div>
        </div>
    </div>

    <div class="wizard-input-section">
        <!--<p>
            Altitud (m)
        </p>-->

        <label for="altitud"><?php _e('Altitud (m)') ?></label>
        <div class="form-group">
            <div class="col-md-5">
                <input type="number" class="form-control" name="altitud" data-validate="validateField" min="0" max="10000" id="altitud">
            </div>
        </div>
    </div>

    <div class="wizard-input-section oculto">
        <!--<p>
            Velocidad viento (m/s)
        </p>-->

        <label for="velViento"><?php _e('Velocidad viento (m/s)') ?></label>
        <div class="form-group">
            <div class="col-md-5">
                <input type="number" class="form-control" name="vel_viento" data-validate="validateField" min="0" max="10" id="velViento">
            </div>
        </div>
    </div>
</div>
