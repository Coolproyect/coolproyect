<div class="wizard-card">



    <h3>8. <?php _e('Ocupación') ?></h3>
    <div class="wizard-input-section col-md-3">
        <img class="img-responsive imagenArriba" src="assets/images/ocupacion.png" alt="">
    </div>

    <div class="wizard-input-section col-md-9 active-refrigerados active-congelados active-sala-trabajo active-secadero oculto">
        <div class="form-group">
            <label class="control-label col-sm-7" for="num_personas"><?php _e('Nº personas') ?></label>
            <div class="col-sm-5">
                <input type="number" class="form-control" id="num_personas" placeholder="<?php _e('Nº personas') ?>" data-validate="validateField" min="0" max="200" name="num_personas">
            </div>
        </div>

        <div class="form-group">
            <label class="control-label col-sm-7" for="tiempo_personas"><?php _e('Tiempo permanencia (h/día)') ?></label>
            <div class="col-sm-5">
                <input type="number" class="form-control" id="tiempo_personas" placeholder="<?php _e('Tiempo permanencia (h/día)') ?>" data-validate="validateField" min="0" max="24" name="tiempo_personas">
            </div>
        </div>

    </div>

    <div class="wizard-input-section col-md-9 active-tunel-enfriamiento active-tunel-congelacion oculto">
        <div class="form-group">
            <label class="control-label col-sm-7" for="num_personas"><?php _e('No disponible para este tipo de cámara') ?></label>
        </div>
    </div>
</div>
