<div class="wizard-card">



    <h3>10. <?php _e('Evaporador') ?></h3>
    <div class="wizard-input-section col-md-3">
        <img class="img-responsive imagenArriba" src="assets/images/evaporador.png" alt="">
    </div>

    <div class="wizard-input-section col-md-9">
        <div class="form-group">
            <label class="control-label col-sm-7" for="num_lamp"><?php _e('Tipo evaporador') ?></label>
            <div class="col-sm-5">
                <label class="radio-inline">
                    <input type="radio" name="tipoEvap" id='tipoEvap1' value="1"><?php _e('Evaporador estático') ?>
                </label>
                <label class="radio-inline" style="margin-left:0">
                    <input type="radio" name="tipoEvap" id='tipoEvap2' value="2"
                    checked><?php _e('Evaporador ventilado') ?>
                </label>

            </div>
        </div>

         <div class="active-tipoEvap2 oculto">
            <div class="form-group">
                <label class="control-label col-sm-7" for="num_lamp"><?php _e('Método de cálculo') ?></label>
                <div class="col-sm-5">
                    <label class="radio-inline">
                        <input type="radio" name="potVentEst" id='potVentEstVal1' value="1"
                        checked><?php _e('Potencia ventiladores conocida') ?>
                    </label>
                    <label class="radio-inline" style="margin-left:0">
                        <input type="radio" name="potVentEst" id='potVentEstVal2' value="2" ><?php _e('Potencia estimada por mayoración ') ?>(%)
                    </label>

                </div>
            </div>

            <div class="form-group active-potVentEstVal1 oculto">
                <label class="control-label col-sm-7" for="pot_vent"><?php _e('Potencia ventiladores ') ?>(W)</label>
                <div class="col-sm-5">
                    <input type="number" class="form-control" id="pot_vent" placeholder="<?php _e('Potencia ventiladores ') ?>(W)" data-validate="validateField" min="0" max="100000" name="pot_vent">
                </div>
            </div>

            <div class="form-group active-potVentEstVal1 oculto">
                <label class="control-label col-sm-7" for="tiempo_vent"><?php _e('Tiempo de funcionamiento (h/día)') ?></label>
                <div class="col-sm-5">
                    <input type="number" class="form-control" id="tiempo_vent" placeholder="<?php _e('Tiempo de funcionamiento (h/día)') ?>" data-validate="validateField" min="0" max="24" name="tiempo_vent">
                </div>
            </div>

            <div class="form-group active-potVentEstVal2 oculto">
                <label class="control-label col-sm-7" for="pot_vent_estima"><?php _e('Potencia estimada por mayoración (% sobre carga térmica total)') ?></label>
                <div class="col-sm-5">
                    <input type="number" class="form-control" id="pot_vent_estima" placeholder="<?php _e('Potencia estimada por mayoración (% sobre carga térmica total)') ?>" data-validate="validateField" min="0" max="100" name="pot_vent_estima">
                </div>
            </div>
        </div>

         <div class="active-congelados active-refrigerados active-tunel-congelacion active-tunel-enfriamiento active-secadero oculto">
            <div class="form-group">
                <label class="control-label col-sm-7" ></label>
                <label class="control-label col-sm-5 texto-izquierda" ><?php _e('Potencia calorífica desescarche') ?></label>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-7" ><?php _e('Método de cálculo') ?></label>
                <div class="col-sm-5">
                    <label class="radio-inline">
                        <input type="radio" name="potResisEst" id='potResisEstVal1' value="1"
                        checked><?php _e('Potencia desescarche conocida') ?>
                    </label>
                    <label class="radio-inline" style="margin-left:0">
                        <input type="radio" name="potResisEst" id='potResisEstVal2' value="2" ><?php _e('Potencia estimada por mayoración ') ?>(%)
                    </label>

                </div>
            </div>

            <div class="form-group active-potResisEstVal1 oculto">
                <label class="control-label col-sm-7" for="pot_resist"><?php _e('Potencia desescarche ') ?>(W)</label>
                <div class="col-sm-5">
                    <input type="number" class="form-control" id="pot_resist" placeholder="<?php _e('Potencia desescarche ') ?>(W)" data-validate="validateField" min="0" max="100000" name="pot_resist">
                </div>
            </div>

            <div class="form-group active-potResisEstVal1 oculto">
                <label class="control-label col-sm-7" for="tiempo_resist"><?php _e('Tiempo de funcionamiento (h/día)') ?></label>
                <div class="col-sm-5">
                    <input type="number" class="form-control" id="tiempo_resist" placeholder="<?php _e('Tiempo de funcionamiento (h/día)') ?>" data-validate="validateField" min="0" max="24" name="tiempo_resist">
                </div>
            </div>

            <div class="form-group active-potResisEstVal2 oculto">
                <label class="control-label col-sm-7" for="pot_resist_estima"><?php _e('Potencia estimada por mayoración (% sobre carga térmica total)') ?></label>
                <div class="col-sm-5">
                    <input type="number" class="form-control" id="pot_resist_estima" placeholder="<?php _e('Potencia estimada por mayoración (% sobre carga térmica total)') ?>" data-validate="validateField" min="0" max="100" name="pot_resist_estima">
                </div>
            </div>
        </div>

    </div>
</div>
