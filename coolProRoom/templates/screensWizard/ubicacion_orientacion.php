<div class="wizard-card">



    <h3>6. <?php _e('Ubicación y orientación') ?></h3>
    <div class="wizard-input-section col-md-3">
        <img class="img-responsive imagenArriba" src="assets/images/brujula.png" alt="">
        <img class="img-responsive imagenAbajo active-tipo_ubicacion2 oculto" src="assets/images/cam_orienta.png" alt="">

    </div>

    <div class="wizard-input-section col-md-9">
        <div class="form-group">
            <label class="control-label col-sm-7" for=""><?php _e('Ubicación') ?></label>
            <div class="col-sm-5">
                <label class="radio-inline">
                    <input type="radio" name="tipo_ubicacion" id='tipo_ubicacion1' value="1"
                    checked><?php _e('Todos los cerramientos a la misma temperatura') ?>
                </label>
                <label class="radio-inline" style="margin-left:0">
                    <input type="radio" name="tipo_ubicacion" id='tipo_ubicacion2' value="2" ><?php _e('Cerramientos con distintas temperaturas') ?>
                </label>

            </div>
        </div>

        <div class="form-group">
            <label class="control-label col-sm-7 active-tipo_ubicacion2 oculto" for="temp_recinto"><?php _e('Tª antecámara acceso ') ?>(ºC)</label>
            <label class="control-label col-sm-7 active-tipo_ubicacion1 oculto" for="temp_recinto"><?php _e('Tª  recinto ') ?>(ºC)</label>
            <div class="col-sm-5">
                <input type="number" class="form-control" id="temp_recinto" placeholder="<?php _e('Tª antecámara acceso ') ?>(ºC)" data-validate="validateField" min="0" max="30" name="temp_recinto">
            </div>
        </div>

        <div class="form-group">
            <label class="control-label col-sm-7 active-tipo_ubicacion2 oculto" for="hum_recinto"><?php _e('H.R antecámara acceso ') ?>(%)</label>
            <label class="control-label col-sm-7 active-tipo_ubicacion1 oculto" for="hum_recinto"><?php _e('H.R recinto ') ?>(%)</label>
            <div class="col-sm-5">
                <input type="number" class="form-control" id="hum_recinto" placeholder="<?php _e('H.R antecámara acceso ') ?>(%)" data-validate="validateField" min="0" max="100" name="hum_recinto">
            </div>
        </div>

        <div class="form-group active-tipo_ubicacion2 oculto">
            <label class="control-label col-sm-7" for=""><?php _e('¿Existe algún cerramiento exterior o adosado a cerramiento exterior?') ?></label>
            <div class="col-sm-5">
                <label class="radio-inline">
                    <input type="radio"  name="cerr_ext" id='cerr_ext' value="1"><?php _e('Si') ?>
                </label>
                <label class="radio-inline" style="margin-left:0">
                    <input type="radio" name="cerr_ext" value="2" checked><?php _e('No') ?>
                </label>

                <div class="active-cerr_ext oculto" id="modal-cerramiento-wrapper" style="margin-top: 10px">
                    <a class="btn btn-info btn-sm" data-toggle="modal" data-target="#modalCerramientos" data-backdrop="false"><?php _e('Editar características de los cerramientos') ?></a>
                </div>

            </div>
        </div>

        <div class="active-tipo_ubicacion2 oculto">
            <div class="form-group">
                <label class="control-label col-sm-7" for=""></label>
                <label class="control-label col-sm-5 texto-izquierda" for=""><?php _e('Tª cerramientos') ?></label>
            </div>

            <div class="form-group">
                <label class="control-label col-sm-7" for="temp_ext_calc_cerr1"><?php _e('Tª Cerramiento') ?> 1 (ºC)</label>
                <div class="col-sm-5">
                    <input type="number" class="form-control" id="temp_ext_calc_cerr1" placeholder="<?php _e('Tª Cerramiento') ?> 1 (ºC)" data-validate="validateField" name="temp_ext_calc_cerr1" disabled>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-7" for="temp_ext_calc_cerr2"><?php _e('Tª Cerramiento') ?> 2 (ºC)</label>
                <div class="col-sm-5">
                    <input type="number" class="form-control" id="temp_ext_calc_cerr2" placeholder="<?php _e('Tª Cerramiento') ?> 2 (ºC)" data-validate="validateField" min="-100" max="100" name="temp_ext_calc_cerr2">
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-7" for="temp_ext_calc_cerr3"><?php _e('Tª Cerramiento') ?> 3 (ºC)</label>
                <div class="col-sm-5">
                    <input type="number" class="form-control" id="temp_ext_calc_cerr3" placeholder="<?php _e('Tª Cerramiento') ?> 3 (ºC)" data-validate="validateField" min="-100" max="100" name="temp_ext_calc_cerr3">
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-7" for="temp_ext_calc_cerr4"><?php _e('Tª Cerramiento') ?> 4 (ºC)</label>
                <div class="col-sm-5">
                    <input type="number" class="form-control" id="temp_ext_calc_cerr4" placeholder="<?php _e('Tª Cerramiento') ?> 4 (ºC)" data-validate="validateField" min="-100" max="100" name="temp_ext_calc_cerr4">
                </div>
            </div>

            <div class="form-group">
                <label class="control-label col-sm-7" for="temp_ext_calc_techo"><?php _e('Tª cubierta ') ?>(ºC)</label>
                <div class="col-sm-5">
                    <input type="number" class="form-control" id="temp_ext_calc_techo" placeholder="<?php _e('Tª  cubierta') ?>(ºC)" data-validate="validateField" min="-100" max="100" name="temp_ext_calc_techo">
                </div>
            </div>
       </div>
        <?php include('templates/modal-cerramientos.php') ?>

    </div>
</div>
