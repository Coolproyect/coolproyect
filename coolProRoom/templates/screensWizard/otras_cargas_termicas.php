<div class="wizard-card">



    <h3>11. <?php _e('Otras cargas térmicas') ?></h3>
    <div class="wizard-input-section col-md-3">
        <img class="img-responsive imagenArriba" src="assets/images/otras_cargas_termicas.png" alt="">
    </div>

    <div class="wizard-input-section col-md-9">
        <div class="form-group">
            <label class="control-label col-sm-7" for="num_lamp"><?php _e('Método de cálculo') ?></label>
            <div class="col-sm-5">
                <label class="radio-inline">
                    <input type="radio" name="potConEst" id='potConEstVal1' value="1"
                    checked><?php _e('Potencia conocida') ?>
                </label>
                <label class="radio-inline" style="margin-left:0">
                    <input type="radio" name="potConEst" id='potConEstVal2' value="2" ><?php _e('Potencia estimada por mayoración ') ?>%
                </label>

            </div>
        </div>

        <div class="form-group active-potConEstVal1 oculto">
            <label class="control-label col-sm-7" for="pot_otros"><?php _e('Potencia otras cargas ') ?>(W)</label>
            <div class="col-sm-5">
                <input type="number" class="form-control" id="pot_otros" placeholder="<?php _e('Potencia otras cargas ') ?>(W)" data-validate="validateField" value="0" min="0" max="100000" name="pot_otros">
            </div>
        </div>

        <div class="form-group active-potConEstVal1 oculto">
            <label class="control-label col-sm-7" for="tiempo_otros"><?php _e('Tiempo de funcionamiento (h/día)') ?></label>
            <div class="col-sm-5">
                <input type="number" class="form-control" id="tiempo_otros" placeholder="<?php _e('Tiempo de funcionamiento (h/día)') ?>" data-validate="validateField" value="0" min="0" max="24" name="tiempo_otros">
            </div>
        </div>

        <div class="form-group active-potConEstVal2 oculto">
            <label class="control-label col-sm-7" for="pot_otros_estima"><?php _e('Potencia estimada por mayoración (% sobre carga térmica total)') ?></label>
            <div class="col-sm-5">
                <input type="number" class="form-control" id="pot_otros_estima" placeholder="<?php _e('Potencia estimada por mayoración (% sobre carga térmica total)') ?>" data-validate="validateField" value="0" min="0" max="100" name="pot_otros_estima">
            </div>
        </div>

    </div>
</div>
