<div class="wizard-card">



    <h3>9. <?php _e('Iluminación') ?></h3>
    <div class="wizard-input-section col-md-3">
        <img class="img-responsive imagenArriba" src="assets/images/iluminacon.png" alt="">
    </div>

    <div class="wizard-input-section col-md-9 active-refrigerados active-congelados active-sala-trabajo active-secadero oculto">
        <div class="form-group">
            <label class="control-label col-sm-7" for="num_lamp"><?php _e('Método de cálculo') ?></label>
            <div class="col-sm-5">
                <label class="radio-inline">
                    <input type="radio" name="tipoNumPot" id='tipoNumPotVal1' value="1"><?php _e('Tipo y número lámparas conocido') ?>
                </label>
                <label class="radio-inline" style="margin-left:0">
                    <input type="radio" name="tipoNumPot" id='tipoNumPotVal2' value="2"
                    checked><?php _e('Potencia iluminación estimada (W/m2)') ?>
                </label>

            </div>
        </div>

         <div class="active-tipoNumPotVal1 oculto">
            <div class="form-group">
                <label class="control-label col-sm-7" for="num_lamp"><?php _e('Tipo lámparas') ?></label>
                <div class="col-sm-5">
                    <label class="radio-inline">
                        <input type="radio" name="tipoLamp" value="1"><?php _e('Incandescente') ?>
                    </label>
                    <label class="radio-inline" style="margin-left:0">
                        <input type="radio" name="tipoLamp" value="2"
                        checked><?php _e('Fluorescente o Halógeno') ?>
                    </label>

                </div>
            </div>

            <div class="form-group">
                <label class="control-label col-sm-7" for="num_lamp"><?php _e('Número lámparas') ?></label>
                <div class="col-sm-5">
                    <input type="number" class="form-control" id="num_lamp" placeholder="<?php _e('Número lámparas') ?>" data-validate="validateField" min="0" max="3000" name="num_lamp">
                </div>
            </div>

            <div class="form-group">
                <label class="control-label col-sm-7" for="pot_lamp"><?php _e('Potencia unitaria (W/ud)') ?></label>
                <div class="col-sm-5">
                    <input type="number" class="form-control" id="pot_lamp" placeholder="<?php _e('Potencia unitaria (W/ud)') ?>" data-validate="validateField" min="0" max="10000" name="pot_lamp">
                </div>
            </div>
        </div>
        
        <div class="form-group active-tipoNumPotVal2 oculto">
            <label class="control-label col-sm-7" for="pot_m2_ilum"><?php _e('Potencia iluminación estimada (W/m2)') ?></label>
            <div class="col-sm-5">
                <input type="number" class="form-control" id="pot_m2_ilum" placeholder="<?php _e('Potencia iluminación estimada (W/m2)') ?>" data-validate="validateField" min="0" max="1000" name="pot_m2_ilum">
            </div>
        </div>

        <div class="form-group">
            <label class="control-label col-sm-7" for="tiempo_lamp"><?php _e('Tiempo de funcionamiento (h/día)') ?></label>
            <div class="col-sm-5">
                <input type="number" class="form-control" id="tiempo_lamp" placeholder="<?php _e('Tiempo de funcionamiento (h/día)') ?>" data-validate="validateField" min="0" max="24" name="tiempo_lamp">
            </div>
        </div>

    </div>

    <div class="wizard-input-section col-md-9 active-tunel-enfriamiento active-tunel-congelacion oculto">
        <div class="form-group">
            <label class="control-label col-sm-7" for="num_personas"><?php _e('No disponible para este tipo de cámara') ?></label>
        </div>
    </div>
</div>
