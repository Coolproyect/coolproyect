<div class="wizard-card">


<?php
$bd->get('cerramientos', 'pared_y_techo= 1');
$datos_pared_y_techo = $bd->getRowsSelect();
$bd->get('cerramientos', 'ventana= 1');
$datos_ventana = $bd->getRowsSelect();
$bd->get('cerramientos', 'suelo= 1');
$datos_suelo = $bd->getRowsSelect();
?>
    <h3>7. <?php _e('Cerramientos') ?></h3>
    <div class="wizard-input-section col-md-3">
        <img class="img-responsive imagenArriba" src="assets/images/cerramientos.png" alt="">
    </div>

    <div class="wizard-input-section col-md-9">
        <div class="form-group">
            <label class="control-label col-sm-7" for=""><?php _e('Cerramientos verticales') ?></label>
            <div class="col-sm-5">
                <label class="radio-inline">
                    <input type="radio" name="cerr_verticales" id='cerr_verticales1' value="1"
                    checked><?php _e('Todos los cerramientos son iguales') ?>
                </label>
                <label class="radio-inline" style="margin-left:0">
                    <input type="radio" name="cerr_verticales" id='cerr_verticales2' value="2" ><?php _e('Cerramientos diferentes') ?>
                </label>

            </div>
        </div>

        <div class="form-group">
            <label class="control-label col-sm-7"></label>
            <label class="control-label col-sm-5 active-cerr_verticales1 oculto texto-izquierda"><?php _e('Cerramientos') ?></label>
            <label class="control-label col-sm-5 active-cerr_verticales2 oculto texto-izquierda"><?php _e('Cerramiento ') ?>1</label>
        </div>

        <div class="form-group">
            <label class="control-label col-sm-7 active-cerr_verticales1 oculto"><?php _e('Material cerramientos') ?></label>
            <label class="control-label col-sm-7 active-cerr_verticales2 oculto"><?php _e('Material cerramiento 1') ?></label>
            <div class="col-sm-5">
                <select class="form-control" id="material_cerramiento_1" change-hidden='material_cerramiento_1'>
                    <?php
                    foreach ($datos_pared_y_techo as $datos) {
                        if($datos["id"]== 2){
                            echo '<option value="'.$datos["id"].'"
                            selected>'.$datos["nombre"].'</option>';
                        }
                        else{
                            echo '<option value="'.$datos["id"].'">'.$datos["nombre"].'</option>';
                        }
                    }
                    ?>
                </select>
            </div>
            <input type="hidden" class="material_cerramiento_1" name="material_cerramiento_1">
        </div>

        <div class="form-group oculto">
            <label class="control-label col-sm-7" for=""><?php _e('Conductividad térmica ') ?>(W/mK)</label>
            <div class="col-sm-5">
                <input type="number" class="form-control dec-4" id="cond_cerr1" placeholder="<?php _e('Conductividad térmica ') ?>(W/mK)" data-validate="validateField" min="0.01" max="500" name="cond_cerr1">
            </div>
        </div>

        <div class="form-group">
            <label class="control-label col-sm-7" for=""><?php _e('Espesor ') ?>(mm)</label>
            <div class="col-sm-5">
                <input type="number" class="form-control" id="espe_cerr1_calc" placeholder="<?php _e('Espesor ') ?>(mm)" data-validate="validateField" min="1" max="1000" name="espe_cerr1_calc">
            </div>
        </div>

        <div class="active-cerr_verticales2 oculto">
            <div class="form-group">
                <label class="control-label col-sm-7"></label>
                <label class="control-label col-sm-5 texto-izquierda"><?php _e('Cerramiento ') ?>2</label>
            </div>

            <div class="form-group">
                <label class="control-label col-sm-7"><?php _e('Material cerramiento 2') ?></label>
                <div class="col-sm-5">
                    <select class="form-control" id="material_cerramiento_2" change-hidden='material_cerramiento_2'>
                        <?php
                        foreach ($datos_pared_y_techo as $datos) {
                            if($datos["id"]== 2){
                                echo '<option value="'.$datos["id"].'"
                                selected>'.$datos["nombre"].'</option>';
                            }
                            else{
                                echo '<option value="'.$datos["id"].'">'.$datos["nombre"].'</option>';
                            }
                        }
                        ?>
                    </select>
                </div>
                <input type="hidden" class="material_cerramiento_2" name="material_cerramiento_2">
            </div>

            <div class="form-group oculto">
                <label class="control-label col-sm-7" for=""><?php _e('Conductividad térmica ') ?>(W/mK)</label>
                <div class="col-sm-5">
                    <input type="number" class="form-control dec-4" id="cond_cerr2" placeholder="<?php _e('Conductividad térmica ') ?>(W/mK)" data-validate="validateField" min="0.01" max="500" name="cond_cerr2">
                </div>
            </div>

            <div class="form-group">
                <label class="control-label col-sm-7" for=""><?php _e('Espesor ') ?>(mm)</label>
                <div class="col-sm-5">
                    <input type="number" class="form-control" id="espe_cerr2_calc" placeholder="<?php _e('Espesor ') ?>(mm)" data-validate="validateField" min="1" max="1000" name="espe_cerr2_calc">
                </div>
            </div>
        </div>

        <div class="active-cerr_verticales2 oculto">
            <div class="form-group">
                <label class="control-label col-sm-7"></label>
                <label class="control-label col-sm-5 texto-izquierda"><?php _e('Cerramiento ') ?>3</label>
            </div>

            <div class="form-group">
                <label class="control-label col-sm-7"><?php _e('Material cerramiento 3') ?></label>
                <div class="col-sm-5">
                    <select class="form-control" id="material_cerramiento_3" change-hidden='material_cerramiento_3'>
                        <?php
                        foreach ($datos_pared_y_techo as $datos) {
                            if($datos["id"]== 2){
                                echo '<option value="'.$datos["id"].'"
                                selected>'.$datos["nombre"].'</option>';
                            }
                            else{
                                echo '<option value="'.$datos["id"].'">'.$datos["nombre"].'</option>';
                            }
                        }
                        ?>
                    </select>
                </div>
                <input type="hidden" class="material_cerramiento_3" name="material_cerramiento_3">
            </div>

            <div class="form-group oculto">
                <label class="control-label col-sm-7" for=""><?php _e('Conductividad térmica ') ?>(W/mK)</label>
                <div class="col-sm-5">
                    <input type="number" class="form-control dec-4" id="cond_cerr3" placeholder="<?php _e('Conductividad térmica ') ?>(W/mK)" data-validate="validateField" min="0.01" max="500" name="cond_cerr3">
                </div>
            </div>

            <div class="form-group">
                <label class="control-label col-sm-7" for=""><?php _e('Espesor ') ?>(mm)</label>
                <div class="col-sm-5">
                    <input type="number" class="form-control" id="espe_cerr3_calc" placeholder="<?php _e('Espesor ') ?>(mm)" data-validate="validateField" min="1" max="1000" name="espe_cerr3_calc">
                </div>
            </div>
        </div>

        <div class="active-cerr_verticales2 oculto">
            <div class="form-group">
                <label class="control-label col-sm-7"></label>
                <label class="control-label col-sm-5 texto-izquierda"><?php _e('Cerramiento ') ?>4</label>
            </div>

            <div class="form-group">
                <label class="control-label col-sm-7"><?php _e('Material cerramiento 4') ?></label>
                <div class="col-sm-5">
                    <select class="form-control" id="material_cerramiento_4" change-hidden='material_cerramiento_4'>
                        <?php
                        foreach ($datos_pared_y_techo as $datos) {
                            if($datos["id"]== 2){
                                echo '<option value="'.$datos["id"].'"
                                selected>'.$datos["nombre"].'</option>';
                            }
                            else{
                                echo '<option value="'.$datos["id"].'">'.$datos["nombre"].'</option>';
                            }
                        }
                        ?>
                    </select>
                </div>
                <input type="hidden" class="material_cerramiento_4" name="material_cerramiento_4">
            </div>

            <div class="form-group oculto">
                <label class="control-label col-sm-7" for=""><?php _e('Conductividad térmica ') ?>(W/mK)</label>
                <div class="col-sm-5">
                    <input type="number" class="form-control dec-4" id="cond_cerr4" placeholder="<?php _e('Conductividad térmica ') ?>(W/mK)" data-validate="validateField" min="0.01" max="500" name="cond_cerr4">
                </div>
            </div>

            <div class="form-group">
                <label class="control-label col-sm-7" for=""><?php _e('Espesor ') ?>(mm)</label>
                <div class="col-sm-5">
                    <input type="number" class="form-control" id="espe_cerr4_calc" placeholder="<?php _e('Espesor ') ?>(mm)" data-validate="validateField" min="1" max="1000" name="espe_cerr4_calc">
                </div>
            </div>
        </div>

        <div class="form-group oculto">
            <label class="control-label col-sm-7" for="h_int_pared"><?php _e('Coeficiente convección interior paredes ') ?>(W/m2K)</label>
            <div class="col-sm-5">
                <input type="number" class="form-control" id="h_int_pared" placeholder="<?php _e('Coeficiente convección interior paredes ') ?>(W/m2K)" data-validate="validateField" min="1" max="1000" name="h_int_pared">
            </div>
        </div>

        <div class="form-group oculto">
            <label class="control-label col-sm-7" for="h_ext_pared"><?php _e('Coeficiente convección exterior paredes ') ?>(W/m2K)</label>
            <div class="col-sm-5">
                <input type="number" class="form-control" id="h_ext_pared" placeholder="<?php _e('Coeficiente convección exterior paredes ') ?>(W/m2K)" data-validate="validateField" min="1" max="1000" name="h_ext_pared">
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-7"></label>
            <label class="control-label col-sm-5 texto-izquierda"><?php _e('Techo') ?></label>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-7"><?php _e('Material techo') ?></label>
            <div class="col-sm-5">
                <select class="form-control" id="material_techo" change-hidden='material_techo'>
                    <?php
                    foreach ($datos_pared_y_techo as $datos) {
                        if($datos["id"]== 2){
                            echo '<option value="'.$datos["id"].'"
                            selected>'.$datos["nombre"].'</option>';
                        }
                        else{
                            echo '<option value="'.$datos["id"].'">'.$datos["nombre"].'</option>';
                        }
                    }
                    ?>
                </select>
            </div>
            <input type="hidden" class="material_techo" name="material_techo">
        </div>
        <div class="form-group oculto">
            <label class="control-label col-sm-7" for=""><?php _e('Conductividad térmica ') ?>(W/mK)</label>
            <div class="col-sm-5">
                <input type="number" class="form-control dec-4" id="cond_techo" placeholder="<?php _e('Conductividad térmica ') ?>(W/mK)" data-validate="validateField" min="0.01" max="500" name="cond_techo">
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-7" for=""><?php _e('Espesor ') ?>(mm)</label>
            <div class="col-sm-5">
                <input type="number" class="form-control" id="espe_techo_calc" placeholder="<?php _e('Espesor ') ?>(mm)" data-validate="validateField" min="1" max="1000" name="espe_techo_calc">
            </div>
        </div>
        <div class="form-group oculto">
            <label class="control-label col-sm-7" for="h_int_techo"><?php _e('Coeficiente convección interior ') ?>(W/m2 K)</label>
            <div class="col-sm-5">
                <input type="number" class="form-control" id="h_int_techo" placeholder="<?php _e('Coeficiente convección interior ') ?>(W/m2 K)" data-validate="validateField" min="1" max="1000" name="h_int_techo">
            </div>
        </div>
        <div class="form-group oculto">
            <label class="control-label col-sm-7" for="h_ext_techo"><?php _e('Coeficiente convección exterior ') ?>(W/m2 K)</label>
            <div class="col-sm-5">
                <input type="number" class="form-control" id="h_ext_techo" placeholder="<?php _e('Coeficiente convección exterior ') ?>(W/m2 K)" data-validate="validateField" min="1" max="1000" name="h_ext_techo">
            </div>
        </div>

        <div class="form-group">
            <label class="control-label col-sm-7"></label>
            <label class="control-label col-sm-5 texto-izquierda"><?php _e('Puertas') ?></label>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-7"><?php _e('Material puertas') ?></label>
            <div class="col-sm-5">
                <select class="form-control" id="material_puertas" change-hidden='material_puertas'>
                    <?php
                    foreach ($datos_pared_y_techo as $datos) {
                        if($datos["id"]== 2){
                            echo '<option value="'.$datos["id"].'"
                            selected>'.$datos["nombre"].'</option>';
                        }
                        else{
                            echo '<option value="'.$datos["id"].'">'.$datos["nombre"].'</option>';
                        }
                    }
                    ?>
                </select>
            </div>
            <input type="hidden" class="material_puertas" name="material_puertas">
        </div>
        <div class="form-group oculto">
            <label class="control-label col-sm-7" for=""><?php _e('Conductividad térmica ') ?>(W/mK)</label>
            <div class="col-sm-5">
                <input type="number" class="form-control dec-4" id="cond_puerta" placeholder="<?php _e('Conductividad térmica ') ?>(W/mK)" data-validate="validateField" min="0.01" max="500" name="cond_puerta">
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-7" for=""><?php _e('Espesor ') ?>(mm)</label>
            <div class="col-sm-5">
                <input type="number" class="form-control" id="espe_puerta" placeholder="<?php _e('Espesor ') ?>(mm)" data-validate="validateField" min="1" max="1000" name="espe_puerta">
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-7" for="sup_puerta"><?php _e('Superficie ') ?>(m2)</label>
            <div class="col-sm-5">
                <input type="number" class="form-control" id="sup_puerta" placeholder="<?php _e('Superficie ') ?>(m2)" data-validate="validateField" min="0" max="5000" name="sup_puerta">
            </div>
        </div>

        <div class="form-group">
            <label class="control-label col-sm-7"><?php _e('Ventanas') ?></label>
            <div class="col-sm-5">
                <label class="radio-inline">
                    <input type="radio" name="ventanas" id='ventanas' value="1"><?php _e('Si') ?>
                </label>
                <label class="radio-inline" style="margin-left:0">
                    <input type="radio" name="ventanas" value="2"
                    checked ><?php _e('No') ?>
                </label>

            </div>
        </div>
        <div class="form-group active-ventanas oculto">
            <label class="control-label col-sm-7"><?php _e('Material ventanas') ?></label>
            <div class="col-sm-5">
                <select class="form-control" id="material_ventanas" change-hidden='material_ventanas'>
                    <?php
                    foreach ($datos_ventana as $datos) {
                        if($datos["id"]== 24){
                            echo '<option value="'.$datos["id"].'"
                            selected>'.$datos["nombre"].'</option>';
                        }
                        else{
                            echo '<option value="'.$datos["id"].'">'.$datos["nombre"].'</option>';
                        }
                    }
                    ?>
                </select>
            </div>
            <input type="hidden" class="material_ventanas" name="material_ventanas">
        </div>
        <div class="form-group oculto">
            <label class="control-label col-sm-7" for=""><?php _e('Conductividad térmica ') ?>(W/m K)</label>
            <div class="col-sm-5">
                <input type="number" class="form-control dec-4" id="cond_ventana" placeholder="<?php _e('Conductividad térmica ') ?>(W/m K)" data-validate="validateField" min="0.01" max="500" name="cond_ventana">
            </div>
        </div>
        <div class="form-group active-ventanas oculto">
            <label class="control-label col-sm-7" for=""><?php _e('Espesor ') ?>(mm)</label>
            <div class="col-sm-5">
                <input type="number" class="form-control" id="espe_ventana" placeholder="<?php _e('Espesor ') ?>(mm)" data-validate="validateField" min="1" max="1000" name="espe_ventana">
            </div>
        </div>
        <div class="form-group active-ventanas oculto">
            <label class="control-label col-sm-7" for="sup_ventana"><?php _e('Superficie ') ?>(m2)</label>
            <div class="col-sm-5">
                <input type="number" class="form-control" id="sup_ventana" placeholder="<?php _e('Superficie ') ?>(m2)" data-validate="validateField" min="0" max="5000" name="sup_ventana">
            </div>
        </div>

        <div class="form-group">
            <label class="control-label col-sm-7"></label>
            <label class="control-label col-sm-5 texto-izquierda"><?php _e('Suelo') ?></label>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-7"><?php _e('Material solera') ?></label>
            <div class="col-sm-5">
                <select class="form-control" id="material_solera" change-hidden='material_solera'>
                    <?php
                    foreach ($datos_suelo as $datos) {
                        if($datos["id"]== 21){
                            echo '<option value="'.$datos["id"].'"
                            selected>'.$datos["nombre"].'</option>';
                        }
                        else{
                            echo '<option value="'.$datos["id"].'">'.$datos["nombre"].'</option>';
                        }
                    }
                    ?>
                </select>
            </div>
            <input type="hidden" class="material_solera" name="material_solera">
        </div>
        <div class="form-group oculto">
            <label class="control-label col-sm-7" for=""><?php _e('Conductividad térmica ') ?>(W/mºC)</label>
            <div class="col-sm-5">
                <input type="number" class="form-control dec-4" id="cond_suelo" placeholder="<?php _e('Conductividad térmica ') ?>(W/mºC)" data-validate="validateField" min="0.01" max="500" name="cond_suelo">
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-7" for=""><?php _e('Espesor ') ?>(mm)</label>
            <div class="col-sm-5">
                <input type="number" class="form-control" id="espe_suelo" placeholder="<?php _e('Espesor ') ?>(mm)" data-validate="validateField" min="1" max="1000" name="espe_suelo">
            </div>
        </div>
        <div class="form-group oculto">
            <label class="control-label col-sm-7" for="h_int_suelo"><?php _e('Coeficiente convección interior ') ?>(W/m2 K)</label>
            <div class="col-sm-5">
                <input type="number" class="form-control" id="h_int_suelo" placeholder="<?php _e('Coeficiente convección interior ') ?>(W/m2 K)" data-validate="validateField" min="1" max="1000" name="h_int_suelo">
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-7"><?php _e('Aislamiento') ?></label>
            <div class="col-sm-5">
                <label class="radio-inline">
                    <input type="radio" name="aislamiento" id='aislamiento' value="1"
                    checked><?php _e('Si') ?>
                </label>
                <label class="radio-inline" style="margin-left:0">
                    <input type="radio" name="aislamiento" value="2" ><?php _e('No') ?>
                </label>

            </div>
        </div>
        <div class="form-group active-aislamiento oculto">
            <label class="control-label col-sm-7"><?php _e('Material aislamiento') ?></label>
            <div class="col-sm-5">
                <select class="form-control" id="material_aislamiento" change-hidden='material_aislamiento'>
                    <?php
                    foreach ($datos_suelo as $datos) {
                        if($datos["id"]== 3){
                            echo '<option value="'.$datos["id"].'"
                            selected>'.$datos["nombre"].'</option>';
                        }
                        else{
                            echo '<option value="'.$datos["id"].'">'.$datos["nombre"].'</option>';
                        }
                    }
                    ?>
                </select>
            </div>
            <input type="hidden" class="material_aislamiento" name="material_aislamiento">
        </div>
        <div class="form-group oculto">
            <label class="control-label col-sm-7" for=""><?php _e('Conductividad térmica ') ?>(W/m K)</label>
            <div class="col-sm-5">
                <input type="number" class="form-control dec-4" id="cond_aisla_suelo" placeholder="<?php _e('Conductividad térmica ') ?>(W/m K)" data-validate="validateField" min="0.01" max="500" name="cond_aisla_suelo">
            </div>
        </div>
        <div class="form-group active-aislamiento oculto">
            <label class="control-label col-sm-7" for=""><?php _e('Espesor ') ?>(mm)</label>
            <div class="col-sm-5">
                <input type="number" class="form-control" id="espe_aisla_suelo" placeholder="<?php _e('Espesor ') ?>(mm)" data-validate="validateField" min="1" max="1000" name="espe_aisla_suelo">
            </div>
        </div>

        <div class="form-group">
            <label class="control-label col-sm-7"><?php _e('Cámara aire') ?></label>
            <div class="col-sm-5">
                <label class="radio-inline">
                    <input type="radio" name="camara_de_aire" value="1"><?php _e('Si') ?>
                </label>
                <label class="radio-inline" style="margin-left:0">
                    <input type="radio" name="camara_de_aire" value="2"
                    checked ><?php _e('No') ?>
                </label>

            </div>
        </div>

    </div>
</div>
