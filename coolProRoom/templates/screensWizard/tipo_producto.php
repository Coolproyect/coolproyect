<div class="wizard-card wizard-card-overlay">
    <h3>4. <?php _e('Tipo de producto') ?></h3>
    <div class="col-md-12">
        <div class="wizard-input-section categoriaProducto col-md-3 carnes oculto active-secadero active-congelados active-refrigerados active-sala-trabajo active-tunel-congelacion active-tunel-enfriamiento" id="producto-carnes">
            <p>
                <?php _e('Carnes') ?>
            </p>

            <img class="img-responsive" src="assets/images/carne.png" alt="<?php _e('Cámara de congelados') ?>">
            <input class="oculto" type="radio" name="categoriaProducto" value="1">

        </div>

        <div class="wizard-input-section categoriaProducto col-md-3 pescados oculto active-congelados active-refrigerados active-sala-trabajo active-tunel-congelacion active-tunel-enfriamiento" id="producto-pescados">
            <p>
                <?php _e('Pescados') ?>
            </p>

            <img class="img-responsive" src="assets/images/pescado.png" alt="<?php _e('Cámara de congelados') ?>">
            <input class="oculto" type="radio" name="categoriaProducto" value="2">

        </div>

        <div class="wizard-input-section categoriaProducto col-md-2 frutas oculto active-congelados active-refrigerados active-sala-trabajo active-tunel-congelacion active-tunel-enfriamiento" id="producto-frutas">
            <p>
                <?php _e('Frutas') ?>
            </p>

            <img class="img-responsive" src="assets/images/frutasDos.png" alt="<?php _e('Cámara de congelados') ?>s">
            <input class="oculto" type="radio" name="categoriaProducto" value="3">

        </div>
        <div class="col-md-4 producto_derecha">
            <div class="productos"></div>
            <div class="" id="modal-producto-wrapper"></div>
        </div>
        <input type="hidden" class="productoValue" name="idProducto">

        <!-- container button modal -->

        <?php include('templates/modal-caracteristicas.php') ?>


    </div>

    <div class="col-md-9">
    <div class="wizard-input-section categoriaProducto col-md-4 verduras oculto active-congelados active-refrigerados active-sala-trabajo active-tunel-congelacion active-tunel-enfriamiento" id="producto-verduras">
        <p>
            <?php _e('Verduras') ?>
        </p>

        <img class="img-responsive" src="assets/images/verduras.png" alt="<?php _e('Cámara de congelados') ?>">
        <input class="oculto" type="radio" name="categoriaProducto" value="4">

    </div>

    <div class="wizard-input-section categoriaProducto col-md-4 lacteos oculto active-congelados active-refrigerados active-sala-trabajo active-tunel-congelacion active-tunel-enfriamiento" id="producto-lacteos">
        <p>
            <?php _e('Lácteos') ?>
        </p>

        <img class="img-responsive" src="assets/images/lacteos.png" alt="<?php _e('Cámara de congelados') ?>">
        <input class="oculto" type="radio" name="categoriaProducto" value="5">

    </div>

    <div class="wizard-input-section categoriaProducto col-md-4 otrosProductos oculto active-congelados active-refrigerados active-sala-trabajo active-tunel-congelacion active-tunel-enfriamiento" id="otros-productos">
        <p>
            <?php _e('Otros productos') ?>
        </p>

        <img class="img-responsive" src="assets/images/otros.png">
        <input class="oculto" type="radio" name="categoriaProducto" value="6">

    </div>
        </div>
    <div class="col-md-9">
    <div class="wizard-input-section categoriaProducto col-md-4 productoVariado oculto active-congelados active-refrigerados active-sala-trabajo" id="produc-variado">
        <p>
            <?php _e('Producto variado') ?>
        </p>

        <img class="img-responsive" src="assets/images/producto_variado.png">
        <input class="oculto" type="radio" name="categoriaProducto" value="7"
        checked>

    </div>

    <!--<div class="wizard-input-section crearProducto col-md-4">
        <p>
            Crear producto
        </p>

        <img class="img-responsive" src="assets/images/anadiDos.png" alt="Cámara de congelados">

    </div>-->
        </div>

    <div class="col-md-12 seleccionarCategoriaProducto" data-validate="validarCategoriaProducto">
        <p>
            <?php _e('Por favor, selecciona un tipo de producto') ?>
        </p>
    </div>
</div>
