<div class="wizard-card" data-cardname="location">
    <h3>3. <?php _e('Tipo de cámara') ?></h3>

    <div class="col-md-8">


        <div class="row">

            <div class="wizard-input-section idCamara morad col-md-4" id="congelados">
                <p>
                    <?php _e('Cámara de congelados') ?>
                </p>

                <img class="img-responsive" src="assets/images/cam_congelados.png" alt="Cámara de congelados" width="150px">
                <input class="oculto" type="radio" name="idCamara" value="2"
                checked>

            </div>

            <div class="wizard-input-section idCamara azu col-md-4" id="refrigerados">
                <p>
                    <?php _e('Cámara de refrigerados') ?>
                </p>

                <img class="img-responsive" src="assets/images/cam_refrigerados.png" alt="Cámara de congelados" width="150px">
                <input class="oculto" type="radio" name="idCamara" value="1">

            </div>

            <div class="wizard-input-section idCamara azuDos col-md-4" id="sala-trabajo">
                <p>
                    <?php _e('Sala de trabajo') ?>
                </p>

                <img class="img-responsive" src="assets/images/sala_trabajo.png" alt="Cámara de congelados" width="150px">
                <input class="oculto" type="radio" name="idCamara" value="5">

            </div>

            <div class="wizard-input-section idCamara morad col-md-4" id="tunel-congelacion">
                <p>
                    <?php _e('Túnel de congelación') ?>
                </p>

                <img class="img-responsive" src="assets/images/tunelCongelacion.png" alt="Cámara de congelados" width="150px">
                <input class="oculto" type="radio" name="idCamara" value="4">

            </div>

            <div class="wizard-input-section idCamara azu col-md-4" id="tunel-enfriamiento">
                <p>
                    <?php _e('Túnel de enfriamiento') ?>
                </p>

                <img class="img-responsive" src="assets/images/tunel_enfriamiento.png" alt="Cámara de congelados" width="150px">
                <input class="oculto" type="radio" name="idCamara" value="3">

            </div>

            <div class="wizard-input-section idCamara secad col-md-4 noDisponible" id="secadero">
                <p>
                    <?php _e('Secadero') ?>
                </p>

                <img class="img-responsive" src="assets/images/secaderos.png" alt="Cámara de congelados" width="150px">
                <input class="oculto secaderoValue" type="radio" name="idCamara" value="6">

            </div>

            <div class="col-md-12 seleccionaCamar" data-validate="validarIdCamara">
                <p>
                    <?php _e('Por favor, selecciona un tipo de cámara') ?>
                </p>
            </div>

        </div><!-- .row -->

    </div>

    <div class="col-md-4">

        <select class="form-control active-secadero oculto" name="options_secadero" id="idCamara" change-hidden="secaderoValue">
            <option value="6"><?php _e('Cámara salado') ?></option>
            <option value="7"><?php _e('Cámara post-salado') ?></option>
            <option value="8"><?php _e('Secadero') ?></option>
            <option value="9"><?php _e('Cámara estufaje') ?></option>
            <option value="10"><?php _e('Bodega') ?></option>
        </select>

    </div>

    <div class="clearfix"></div>

</div>
