<div id="modalProducto" class="modal modal-product fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <!--<button type="button" class="close close-modal-product" >&times;</button>-->
        <h4 class="modal-title"><?php echo _e('Características del producto') ?></h4>
      </div>
      <div class="modal-body">

        <div class="row">

          <div class="form-group col-md-6">
              <label class="control-label col-sm-7" for="temp_cam"><?php _e('Tª cámara') ?>(ºC)</label>
              <div class="col-sm-5">
                  <input type="number" class="form-control" name="temp_cam" data-validate="validateField" min="0" max="20" id="" value="1">
              </div>
          </div>

          <div class="form-group col-md-6">
              <label class="control-label col-sm-7" for="hum_cam"><?php _e('H.R cámara') ?>(%)</label>
              <div class="col-sm-5">
                  <input type="number" class="form-control" name="hum_cam" data-validate="validateField" min="0" max="100" id="" value="1">
              </div>
          </div>

          <div class="form-group col-md-6">
              <label class="control-label col-sm-7" for="temp_inicial"><?php echo _e('Tª entrada producto')?> (ºC)</label>
              <div class="col-sm-5">
                  <input type="number" class="form-control" id="temp_inicial" data-validate="validateField" min="0" max="100" placeholder="<?php echo _e('Tª entrada producto')?> (ºC)" name="temp_inicial">
              </div>
          </div>



          <div class="form-group col-md-6">
              <label class="control-label col-sm-7" for="temp_final"><?php echo _e('Tª final producto')?> (ºC)</label>
              <div class="col-sm-5">
                  <input type="number" class="form-control" id="temp_final" data-validate="validateField" min="0" max="20" placeholder="<?php echo _e('Tª final producto')?> (ºC)" name="temp_final">
              </div>
          </div>



          <div class="form-group col-md-6 active-secadero oculto">
              <label class="control-label col-sm-7" for="agua_prod_fres"><?php echo _e('Contenido agua producto fresco') ?> (%)</label>
              <div class="col-sm-5">
                  <input type="number" class="form-control" name="agua_prod_fres" data-validate="validateField" value="0" id="agua_prod_fres" placeholder="<?php echo _e('Contenido agua producto fresco') ?> (%)">
              </div>
          </div>



          <div class="form-group col-md-6 active-secadero oculto">
              <label class="control-label col-sm-7" for="agua_prod_seco"><?php echo _e('Contenido agua producto seco') ?> (%)</label>
              <div class="col-sm-5">
                  <input type="number" class="form-control" name="agua_prod_seco" data-validate="validateField" value="0" id="agua_prod_seco" placeholder="<?php echo _e('Contenido agua producto seco') ?> (%)">
              </div>
          </div>



          <div class="form-group col-md-6 active-secadero oculto">
              <label class="control-label col-sm-7" for="merma_producto"><?php echo _e('Merma producto') ?> (%)</label>
              <div class="col-sm-5">
                  <input type="number" class="form-control" name="merma_producto" data-validate="validateField" value="0" id="merma_producto" placeholder="<?php echo _e('Merma producto') ?> (%)">
              </div>
          </div>



          <div class="form-group col-md-6 active-secadero oculto">
              <label class="control-label col-sm-7" for="duracion_secado"><?php echo _e('Duración ciclo secado (días)') ?></label>
              <div class="col-sm-5">
                  <input type="number" class="form-control" name="duracion_secado" data-validate="validateField" value="0" id="duracion_secado" placeholder="<?php echo _e('Duración ciclo secado (días)') ?> ">
              </div>
          </div>



          <div class="form-group col-md-6 active-refrigerados oculto">
              <label class="control-label col-sm-7" for="renov_aire"><?php echo _e('Nº renov. diarias ventilación') ?></label>
              <div class="col-sm-5">
                  <input type="number" class="form-control" name="renov_aire" data-validate="validateField" min="0" max="100" id="renov_aire" placeholder="<?php echo _e('Nº renov. diarias ventilación') ?> ">
              </div>
          </div>



          <div class="form-group col-md-12 active-tunel-congelacion active-tunel-enfriamiento active-secadero formas oculto">
            <div class="form-group col-md-12 formas">
                <div class="form-group col-md-6">
                    <label class="control-label col-sm-7" for="forma"><?php echo _e('Forma producto') ?></label>

                    <div class="col-sm-5">

                      <label class="radio-inline" style="margin-left:10px">
                          <input type="radio" name="forma" class='forma' id="esfera" value="1">
                          <?php echo _e('Esfera') ?>
                      </label>

                      <label class="radio-inline">
                          <input type="radio" name="forma" class='forma' id="cilindro" value="2">
                          <?php echo _e('Cilindro') ?>
                      </label>

                      <label class="radio-inline">
                          <input type="radio" name="forma" class='forma' id="rodaja" value="3">
                          <?php echo _e('Rodaja') ?>
                      </label>

                      <label class="radio-inline">
                          <input type="radio" name="forma" class='forma' id="filete_fino" value="4"
                          checked>
                          <?php echo _e('Filete Fino') ?>
                      </label>

                      <label class="radio-inline">
                          <input type="radio" name="forma" class='forma' id="prisma" value="5">
                          <?php echo _e('Prisma') ?>
                      </label>

                      <label class="radio-inline">
                          <input type="radio" name="forma" class='forma' id="ovalo" value="6">
                          <?php echo _e('Óvalo') ?>
                      </label>

                    </div>
                </div>
            </div>

              <div class="form-group col-md-6 active-esfera oculto">
                  <label class="control-label col-sm-7" for="diam_esfera"><?php echo _e('Diámetro esfera') ?> (mm)</label>
                  <div class="col-sm-5">
                      <input type="number" class="form-control" name="diam_esfera" data-validate="validateField" id="diam_esfera" placeholder="<?php echo _e('Diámetro esfera') ?> (mm)">
                  </div>
              </div>



              <div class="form-group col-md-6 active-cilindro oculto">
                  <label class="control-label col-sm-7" for="diam_cilindro"><?php echo _e('Diámetro cilindro') ?> (mm)</label>
                  <div class="col-sm-5">
                      <input type="number" class="form-control" name="diam_cilindro" data-validate="validateField" id="diam_cilindro" placeholder="<?php echo _e('Diámetro cilindro') ?> (mm)">
                  </div>
              </div>



              <div class="form-group col-md-6 active-cilindro oculto">
                  <label class="control-label col-sm-7" for="long_cilindro"><?php echo _e('Longitud cilindro') ?> (mm)</label>
                  <div class="col-sm-5">
                      <input type="number" class="form-control" name="long_cilindro" data-validate="validateField" id="long_cilindro" placeholder="<?php echo _e('Longitud cilindro') ?> (mm)">
                  </div>
              </div>



              <div class="form-group col-md-6 active-rodaja oculto">
                  <label class="control-label col-sm-7" for="diam_rodaja"><?php echo _e('Diámetro rodaja') ?> (mm)</label>
                  <div class="col-sm-5">
                      <input type="number" class="form-control" name="diam_rodaja" data-validate="validateField" id="diam_rodaja" placeholder="<?php echo _e('Diámetro rodaja') ?> (mm)">
                  </div>
              </div>



              <div class="form-group col-md-6 active-rodaja oculto">
                  <label class="control-label col-sm-7" for="espe_rodaja"><?php echo _e('Espesor rodaja') ?> (mm)</label>
                  <div class="col-sm-5">
                      <input type="number" class="form-control" name="espe_rodaja" data-validate="validateField" id="espe_rodaja" placeholder="<?php echo _e('Espesor rodaja') ?> (mm)">
                  </div>
              </div>



              <div class="form-group col-md-6 active-filete_fino oculto">
                  <label class="control-label col-sm-7" for="espe_filete"><?php echo _e('Espesor filete') ?> (mm)</label>
                  <div class="col-sm-5">
                      <input type="number" class="form-control" name="espe_filete" data-validate="validateField" id="espe_filete" placeholder="<?php echo _e('Espesor filete') ?> (mm)">
                  </div>
              </div>



              <div class="form-group col-md-6 active-prisma oculto">
                  <label class="control-label col-sm-7" for="long_prisma"><?php echo _e('Longitud prisma') ?> (mm)</label>
                  <div class="col-sm-5">
                      <input type="number" class="form-control" name="long_prisma" data-validate="validateField" id="long_prisma" placeholder="<?php echo _e('Longitud prisma') ?> (mm)">
                  </div>
              </div>



              <div class="form-group col-md-6 active-prisma oculto">
                  <label class="control-label col-sm-7" for="ancho_prisma"><?php echo _e('Ancho prisma') ?> (mm)</label>
                  <div class="col-sm-5">
                      <input type="number" class="form-control" name="ancho_prisma" data-validate="validateField" id="ancho_prisma" placeholder="<?php echo _e('Ancho prisma') ?> (mm)">
                  </div>
              </div>



              <div class="form-group col-md-6 active-prisma oculto">
                  <label class="control-label col-sm-7" for="espe_prisma"><?php echo _e('Espesor prisma') ?> (mm)</label>
                  <div class="col-sm-5">
                      <input type="number" class="form-control" name="espe_prisma" data-validate="validateField" id="espe_prisma" placeholder="<?php echo _e('Espesor prisma') ?> (mm)">
                  </div>
              </div>



              <div class="form-group col-md-6 active-ovalo oculto">
                  <label class="control-label col-sm-7" for="long_oval"><?php echo _e('Longitud óvalo') ?> (mm)</label>
                  <div class="col-sm-5">
                      <input type="number" class="form-control" name="long_oval" data-validate="validateField" id="long_oval" placeholder="<?php echo _e('Longitud óvalo') ?> (mm)">
                  </div>
              </div>



              <div class="form-group col-md-6 active-ovalo oculto">
                  <label class="control-label col-sm-7" for="ancho_oval"><?php echo _e('Ancho óvalo') ?> (mm)</label>
                  <div class="col-sm-5">
                      <input type="number" class="form-control" name="ancho_oval" data-validate="validateField" id="ancho_oval" placeholder="<?php echo _e('Ancho óvalo') ?> (mm)">
                  </div>
              </div>



              <div class="form-group col-md-6 active-ovalo oculto">
                  <label class="control-label col-sm-7" for="espe_oval"><?php echo _e('Espesor óvalo') ?> (mm)</label>
                  <div class="col-sm-5">
                      <input type="number" class="form-control" name="espe_oval" data-validate="validateField" id="espe_oval" placeholder="<?php echo _e('Espesor óvalo') ?> (mm)">
                  </div>
              </div>
            </div>

          <!-- end row -->
        </div>


<!-- CARACTERÍSTICAS AVANZADAS -->
        <div class="row hidden">
            <div class="form-group col-md-6">
                <label class="control-label col-sm-7" for="vel_aire_cam"><?php echo _e('Velocidad aire cámara') ?> (m/s)</label>
                <div class="col-sm-5">
                    <input type="number" class="form-control" name="vel_aire_cam" data-validate="" id="vel_aire_cam" placeholder="<?php echo _e('Velocidad aire cámara') ?> (m/s)">
                </div>
            </div>

            <div class="form-group col-md-6">
                <label class="control-label col-sm-7" for="h_cam"><?php echo _e('Coef. convección cámara') ?> (W/m2K)</label>
                <div class="col-sm-5">
                    <input type="number" class="form-control" name="h_cam" data-validate="" id="h_cam" placeholder="<?php echo _e('Coef. convección cámara') ?> (W/m2K)">
                </div>
            </div>

            <div class="form-group col-md-6">
                <label class="control-label col-sm-7" for="agua"><?php echo _e('Contenido agua') ?> (%)</label>
                <div class="col-sm-5">
                    <input type="number" class="form-control" name="agua" data-validate="" id="agua" placeholder="<?php echo _e('Contenido agua') ?> (%)">
                </div>
            </div>

            <div class="form-group col-md-6">
                <label class="control-label col-sm-7" for="proteinas"><?php echo _e('Proteinas') ?> (%)</label>
                <div class="col-sm-5">
                    <input type="number" class="form-control" name="proteinas" data-validate="" id="proteinas" placeholder="<?php echo _e('Proteinas') ?> (%)">
                </div>
            </div>

            <div class="form-group col-md-6">
                <label class="control-label col-sm-7" for="grasas"><?php echo _e('Grasas') ?> (%)</label>
                <div class="col-sm-5">
                    <input type="number" class="form-control" name="grasas" data-validate="" id="grasas" placeholder="<?php echo _e('Grasas') ?> (%)">
                </div>
            </div>

            <div class="form-group col-md-6">
                <label class="control-label col-sm-7" for="carbohidratos"><?php echo _e('Carbohidratos') ?> (%)</label>
                <div class="col-sm-5">
                    <input type="number" class="form-control" name="carbohidratos" data-validate="" id="carbohidratos" placeholder="<?php echo _e('Carbohidratos') ?> (%)">
                </div>
            </div>

            <div class="form-group col-md-6">
                <label class="control-label col-sm-7" for="fibras"><?php echo _e('Fibras') ?> (%)</label>
                <div class="col-sm-5">
                    <input type="number" class="form-control" name="fibras" data-validate="" id="fibras" placeholder="<?php echo _e('Fibras') ?> (%)">
                </div>
            </div>

            <div class="form-group col-md-6">
                <label class="control-label col-sm-7" for="cenizas"><?php echo _e('Cenizas') ?> (%)</label>
                <div class="col-sm-5">
                    <input type="number" class="form-control" name="cenizas" data-validate="" id="cenizas" placeholder="<?php echo _e('Cenizas') ?> (%)">
                </div>
            </div>

            <div class="form-group col-md-6">
                <label class="control-label col-sm-7" for="calor_esp_fres"><?php echo _e('Calor específico producto fresco') ?> (J/kgK)</label>
                <div class="col-sm-5">
                    <input type="number" class="form-control" name="calor_esp_fres" data-validate="" id="calor_esp_fres" placeholder="<?php echo _e('Calor específico producto fresco') ?> (J/kgK)">
                </div>
            </div>

            <div class="form-group col-md-6">
                <label class="control-label col-sm-7" for="calor_esp_cong_calc"><?php echo _e('Calor específico producto congelado') ?> (J/kgK)</label>
                <div class="col-sm-5">
                    <input type="number" class="form-control" name="calor_esp_cong_calc" data-validate="" id="calor_esp_cong_calc" placeholder="<?php echo _e('Calor específico producto congelado') ?> (J/kgK)">
                </div>
            </div>

            <div class="form-group col-md-6">
                <label class="control-label col-sm-7" for="conduct_fres"><?php echo _e('Conductividad térmica producto fresco') ?> (W/mK)</label>
                <div class="col-sm-5">
                    <input type="number" class="form-control" name="conduct_fres" data-validate="" id="conduct_fres" placeholder="<?php echo _e('Conductividad térmica producto fresco') ?> (W/mK)">
                </div>
            </div>

            <div class="form-group col-md-6">
                <label class="control-label col-sm-7" for="conduct_cong"><?php echo _e('Conductividad térmica producto congelado') ?> (W/mK)</label>
                <div class="col-sm-5">
                    <input type="number" class="form-control" name="conduct_cong" data-validate="" id="conduct_cong" placeholder="<?php echo _e('Conductividad térmica producto congelado') ?> (W/mK)">
                </div>
            </div>

            <div class="form-group col-md-6">
                <label class="control-label col-sm-7" for="densi_fres"><?php echo _e('Densidad producto fresco') ?> (kg/m3)</label>
                <div class="col-sm-5">
                    <input type="number" class="form-control" name="densi_fres" data-validate="" id="densi_fres" placeholder="<?php echo _e('Densidad producto fresco') ?> (kg/m3)">
                </div>
            </div>

            <div class="form-group col-md-6">
                <label class="control-label col-sm-7" for="densi_cong"><?php echo _e('Densidad producto congelado') ?> (kg/m3)</label>
                <div class="col-sm-5">
                    <input type="number" class="form-control" name="densi_cong" data-validate="" id="densi_cong" placeholder="<?php echo _e('Densidad producto congelado') ?> (kg/m3)">
                </div>
            </div>

            <div class="form-group col-md-6">
                <label class="control-label col-sm-7" for="temp_cong"><?php echo _e('Tª congelación') ?> (ºC)</label>
                <div class="col-sm-5">
                    <input type="number" class="form-control" name="temp_cong" data-validate="" id="temp_cong" placeholder="<?php echo _e('Tª congelación') ?> (ºC)">
                </div>
            </div>

            <div class="form-group col-md-6">
                <label class="control-label col-sm-7" for="calor_lat_cong"><?php echo _e('Calor latente congelación') ?> (ºC)</label>
                <div class="col-sm-5">
                    <input type="number" class="form-control" name="calor_lat_cong" data-validate="" id="calor_lat_cong" placeholder="<?php echo _e('Calor latente congelación') ?> (ºC)">
                </div>
            </div>

            <div class="form-group col-md-6">
                <label class="control-label col-sm-7" for="pot_respi_tfinal"><?php echo _e('Carga respiración a Tª final') ?> (W/kg)</label>
                <div class="col-sm-5">
                    <input type="number" class="form-control" name="pot_respi_tfinal" data-validate="" id="pot_respi_tfinal" placeholder="<?php echo _e('Carga respiración a Tª final') ?> (W/kg)">
                </div>
            </div>

            <div class="form-group col-md-6">
                <label class="control-label col-sm-7" for="pot_respi_tmedia"><?php echo _e('Carga respiración a Tª media producto') ?> (W/kg)</label>
                <div class="col-sm-5">
                    <input type="number" class="form-control" name="pot_respi_tmedia" data-validate="" id="pot_respi_tmedia" placeholder="<?php echo _e('Carga respiración a Tª media producto') ?> (W/kg)">
                </div>
            </div>

            <div class="form-group col-md-6">
                <label class="control-label col-sm-7" for="xc"><?php echo _e('Humedad crítica producto') ?> (kg agua/kg sólido seco; kg water/kg dry solid)</label>
                <div class="col-sm-5">
                    <input type="number" class="form-control" name="xc" data-validate="" id="xc" placeholder="<?php echo _e('Humedad crítica producto') ?> (kg agua/kg sólido seco; kg water/kg dry solid)">
                </div>
            </div>

            <div class="form-group col-md-6">
                <label class="control-label col-sm-7" for="xeq"><?php echo _e('Humedad equilibrio producto-ambiente') ?> (kg agua/kg sólido seco; kg water/kg dry solid)</label>
                <div class="col-sm-5">
                    <input type="number" class="form-control" name="xeq" data-validate="" id="xeq" placeholder="<?php echo _e('Humedad equilibrio producto-ambiente') ?> (kg agua/kg sólido seco; kg water/kg dry solid)">
                </div>
            </div>


        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default close-modal-product">Aceptar</button>
      </div>
    </div>

  </div>
</div>