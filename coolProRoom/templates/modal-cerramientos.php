<div id="modalCerramientos" class="modal modal-cerramientos fade" role="dialog">
  <div class="modal-dialog modal-lg">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <!--<button type="button" class="close close-modal-cerramientos" >&times;</button>-->
        <h4 class="modal-title"><?php echo _e('Características de los cerramientos') ?></h4>
      </div>
      <div class="modal-body">

        <div class="container-fluid">


          <div class="row">

            <div class="form-group col-md-3">
                <img class="img-responsive imagenArriba active-tipo_ubicacion2 oculto" src="assets/images/cam_orienta.png" alt="">

            </div>
            <div class="col-md-9">
                <div class="form-group">
                    <label class="control-label col-sm-6" for=""><?php _e('Orientación cerramiento 1') ?></label>
                    <div class="col-sm-6 radiobutton-orientation">
                        <label class="radio-inline">
                            <input type="radio" name="orientacion_cerr1" value="1" checked><?php _e('N') ?>
                        </label>
                        <label class="radio-inline">
                            <input type="radio" name="orientacion_cerr1" value="2" ><?php _e('S') ?>
                        </label>
                        <label class="radio-inline">
                            <input type="radio" name="orientacion_cerr1" value="3" ><?php _e('E') ?>
                        </label>
                        <label class="radio-inline">
                            <input type="radio" name="orientacion_cerr1" value="4" ><?php _e('O') ?>
                        </label>
                        <div class="clearfix"></div>
                        <label class="radio-inline">
                            <input type="radio" name="orientacion_cerr1" value="5" ><?php _e('NE') ?>
                        </label>
                        <label class="radio-inline">
                            <input type="radio" name="orientacion_cerr1" value="6" ><?php _e('NO') ?>
                        </label>
                        <label class="radio-inline">
                            <input type="radio" name="orientacion_cerr1" value="7" ><?php _e('SE') ?>
                        </label>
                        <label class="radio-inline">
                            <input type="radio" name="orientacion_cerr1" value="8" ><?php _e('SO') ?>
                        </label>

                        <input type="number" class="hidden" name="orientacion_cerr2" value="1">
                        <input type="number" class="hidden" name="orientacion_cerr3" value="1">
                        <input type="number" class="hidden" name="orientacion_cerr4" value="1">
                    </div>
                </div>


                <div class="form-group">
                    <label class="control-label col-sm-6" for=""></label>
                    <label class="control-label col-sm-6 texto-izquierda" for=""><?php _e('Ubicación cerramientos') ?></label>
                </div>

                <div class="form-group">
                    <label class="control-label col-sm-6" for=""><?php _e('Cubierta') ?></label>
                    <div class="col-sm-6">
                        <label class="radio-inline">
                            <input type="radio" name="tipo_cubierta" value="1" checked><?php _e('Interior') ?>
                        </label>
                        <label class="radio-inline" style="margin-left:0">
                            <input type="radio" name="tipo_cubierta" id='tipo_cubierta2' value="2"><?php _e('Exterior') ?>
                        </label>
                        <label class="radio-inline" style="margin-left:0">
                            <input type="radio" name="tipo_cubierta" id='tipo_cubierta3' value="3"><?php _e('Adosado a cerramiento exterior aislado') ?>
                        </label>
                        <label class="radio-inline" style="margin-left:0">
                            <input type="radio" name="tipo_cubierta" id='tipo_cubierta4' value="4"><?php _e('Adosado a cerramiento exterior no aislado') ?>
                        </label>

                    </div>
                </div>

                <div class="form-group active-tipo_cubierta2 active-tipo_cubierta3 active-tipo_cubierta4 oculto">
                    <label class="control-label col-sm-6" for=""><?php _e('Color Cubierta') ?></label>
                    <div class="col-sm-6">
                        <label class="radio-inline">
                            <input type="radio" name="color_cubierta" value="1"
                            checked><?php _e('Claro') ?>
                        </label>
                        <label class="radio-inline" style="margin-left:0">
                            <input type="radio" name="color_cubierta" value="2"><?php _e('Medio') ?>
                        </label>
                        <label class="radio-inline" style="margin-left:0">
                            <input type="radio" name="color_cubierta" value="3"><?php _e('Oscuro') ?>
                        </label>
                    </div>
                </div>

                <div class="form-group">
                    <label for="tipo_cerr1" class="control-label col-sm-6"><?php _e('Cerramiento') ?> 1 (<?php _e('inlcuye puerta') ?>)</label>
                    <div class="col-sm-6">
                        <label class="radio-inline">
                            <input type="radio" name="tipo_cerr1" value="1" checked><?php _e('Interior') ?>
                        </label>
                    </div>
                </div>

                 <div class="form-group">
                    <label class="control-label col-sm-6" for="tipo_cerr2"><?php _e('Cerramiento') ?> 2</label>
                    <div class="col-sm-6">
                        <label class="radio-inline">
                            <input type="radio" name="tipo_cerr2" value="1"><?php _e('Interior') ?>
                        </label>
                        <label class="radio-inline" style="margin-left:0">
                            <input type="radio" name="tipo_cerr2" id='tipo_cubierta2_ext' value="2"><?php _e('Exterior') ?>
                        </label>
                        <label class="radio-inline" style="margin-left:0">
                            <input type="radio" name="tipo_cerr2" id='tipo_cubierta2_ado_aisl' value="3"
                            checked><?php _e('Adosado a cerramiento exterior aislado') ?>
                        </label>
                        <label class="radio-inline" style="margin-left:0">
                            <input type="radio" name="tipo_cerr2" id='tipo_cubierta2_ado' value="4"><?php _e('Adosado a cerramiento exterior no aislado') ?>
                        </label>

                    </div>
                </div>

                <div class="form-group active-tipo_cubierta2_ext active-tipo_cubierta2_ado_aisl active-tipo_cubierta2_ado oculto">
                    <label class="control-label col-sm-6" for=""><?php _e('Color Cerramiento') ?> 2</label>
                    <div class="col-sm-6">
                        <label class="radio-inline">
                            <input type="radio" name="color_cerr2" value="1"
                            checked><?php _e('Claro') ?>
                        </label>
                        <label class="radio-inline" style="margin-left:0">
                            <input type="radio" name="color_cerr2" value="2"><?php _e('Medio') ?>
                        </label>
                        <label class="radio-inline" style="margin-left:0">
                            <input type="radio" name="color_cerr2" value="3"><?php _e('Oscuro') ?>
                        </label>
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label col-sm-6" for="tipo_cerr3"><?php _e('Cerramiento') ?> 3</label>
                    <div class="col-sm-6">
                        <label class="radio-inline">
                            <input type="radio" name="tipo_cerr3" value="1"><?php _e('Interior') ?>
                        </label>
                        <label class="radio-inline" style="margin-left:0">
                            <input type="radio" name="tipo_cerr3" id='tipo_cerr3_ext' value="2"><?php _e('Exterior') ?>
                        </label>
                        <label class="radio-inline" style="margin-left:0">
                            <input type="radio" name="tipo_cerr3" id='tipo_cerr3_ado_aisl' value="3"
                            checked><?php _e('Adosado a cerramiento exterior aislado') ?>
                        </label>
                        <label class="radio-inline" style="margin-left:0">
                            <input type="radio" name="tipo_cerr3" id='tipo_cerr3_ado' value="4"><?php _e('Adosado a cerramiento exterior no aislado') ?>
                        </label>
                    </div>
                </div>

                <div class="form-group active-tipo_cerr3_ext active-tipo_cerr3_ado_aisl active-tipo_cerr3_ado oculto">
                    <label class="control-label col-sm-6" for=""><?php _e('Color Cerramiento') ?> 3</label>
                    <div class="col-sm-6">
                        <label class="radio-inline">
                            <input type="radio" name="color_cerr3" value="1"
                            checked><?php _e('Claro') ?>
                        </label>
                        <label class="radio-inline" style="margin-left:0">
                            <input type="radio" name="color_cerr3" value="2"><?php _e('Medio') ?>
                        </label>
                        <label class="radio-inline" style="margin-left:0">
                            <input type="radio" name="color_cerr3" value="3"><?php _e('Oscuro') ?>
                        </label>
                    </div>
                </div>


                <div class="form-group">
                    <label class="control-label col-sm-6" for="tipo_cerr4"><?php _e('Cerramiento') ?> 4</label>
                    <div class="col-sm-6">
                        <label class="radio-inline">
                            <input type="radio" name="tipo_cerr4" value="1"><?php _e('Interior') ?>
                        </label>
                        <label class="radio-inline" style="margin-left:0">
                            <input type="radio" name="tipo_cerr4" id='tipo_cerr4_ext' value="2"><?php _e('Exterior') ?>
                        </label>
                        <label class="radio-inline" style="margin-left:0">
                            <input type="radio" name="tipo_cerr4" id='tipo_cerr4_ado_aisl' value="3"
                            checked><?php _e('Adosado a cerramiento exterior aislado') ?>
                        </label>
                        <label class="radio-inline" style="margin-left:0">
                            <input type="radio" name="tipo_cerr4" id='tipo_cerr4_ado' value="4"><?php _e('Adosado a cerramiento exterior no aislado') ?>
                        </label>

                    </div>
                </div>

                <div class="form-group active-tipo_cerr4_ext active-tipo_cerr4_ado_aisl active-tipo_cerr4_ado oculto">
                    <label class="control-label col-sm-6" for=""><?php _e('Color Cerramiento') ?> 4</label>
                    <div class="col-sm-6">
                        <label class="radio-inline">
                            <input type="radio" name="color_cerr4" value="1"
                            checked><?php _e('Claro') ?>
                        </label>
                        <label class="radio-inline" style="margin-left:0">
                            <input type="radio" name="color_cerr4" value="2"><?php _e('Medio') ?>
                        </label>
                        <label class="radio-inline" style="margin-left:0">
                            <input type="radio" name="color_cerr4" value="3"><?php _e('Oscuro') ?>
                        </label>
                    </div>
                </div>
            </div>


          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default close-modal-cerramientos">Aceptar</button>
      </div>
    </div>

  </div>
</div>