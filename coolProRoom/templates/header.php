<!DOCTYPE html>
<html>
<head>
    <title>CoolProRoom</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <?php include_once('head.php') ?>

    </head>
    <body style="padding:0px;">
    <header class="clearfix">
        <!-- Start  Logo & Naviagtion  -->
        <div class="navbar navbar-default navbar-top">
            <div class="container">
                <div class="navbar-header">
                    <!-- Stat Toggle Nav Link For Mobiles -->
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <i class="fa fa-bars"></i>
                    </button>
                    <!-- End Toggle Nav Link For Mobiles -->
                    <a class="navbar-brand" href="index.php">
                        <img class="img-responsive" alt="" src="assets/images/coolpro_room_text2.png">
                    </a>
                </div>
                <!--<a class="navbar-brand_centro" href="https://coolproyect.es">
                    <img class="img-responsive" alt="" src="assets/images/coolProyect.png">
                </a>-->
                <h4>Cálculo de la potencia frigorífica</h4>
                <div class="navbar-collapse collapse">
                    <!-- Start Navigation List -->
                    <ul class="nav navbar-nav navbar-right logout">
                        <?php
                        if ( !is_page('login.php')){
                            echo $_SESSION['username'].' <a href="loginLogout.php?action=logout">
                                    <i class="fa fa-sign-out white pull-right opacity-70" id="logout" aria-hidden="true" data-toggle="tooltip" data-placement="middle" title="Salir"></i>
                                </a>';
                        }
                        ?>
                        <!--<li><a href='prueba.php?idioma=en'>EN</a></li><li><a href='prueba.php?idioma=es'>ES</a></li>-->
                    </ul>
                    <!-- End Navigation List -->
                </div>
            </div>
        </div>


    </header>
