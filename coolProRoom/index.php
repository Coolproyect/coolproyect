<?php
/**
 * Created by PhpStorm.
 * User: Álvaro
 * Date: 05/06/2016
 * Time: 17:42
 */

require_once("app/core.php");
$bd = new BD(DB_HOST, DB_USER, DB_PASSWORD, DB_NAME);
$idUsuario= $_SESSION['username'];

?>

<?php include_once('templates/header.php') ?>


<nav class="navbar_crear_proyecto col-md-12">
    <div class="container-fluid">
        <div class="navbar-header">
        </div>
        <ul class="nav navbar-nav">
            <li class="active" id="open-wizard"><a href="#"><i class="fa fa-plus-circle" aria-hidden="true"></i><?php _e(' Crear proyecto') ?></a></li>
            <!--<li><a href="#">Base de datos</a></li>
            <li><a href="#">Informes</a></li>
            <li><a href="#">Ayuda</a></li>-->
        </ul>
    </div>
</nav>

<div class="container listaProyectos">
    <?php
    $bd->getColumn('proyecto.`id` , proyecto.`nombre_proyecto` , proyecto.`fecha_proyecto` , proyecto.`nombre_proyectista` , proyecto.`nombre_cliente` , provincia.`nombre` AS nombre, camara.`nombre` AS nombreCamara', '`proyecto` proyecto, `provincia` provincia, `camara` camara', 'idUsuario= "'.$idUsuario.'" AND proyecto.idProvincia = provincia.id AND proyecto.idCamara = camara.id');
    $datos = $bd->getRowsSelect();
    if($datos){
    ?>
        <table class="table table-hover col-md-12">

            <thead>

            <tr>

                <th class="col-md-3"><?php _e('Nombre') ?></th>

                <th class="col-md-1"><?php _e('Fecha') ?></th>

                <th class="col-md-2"><?php _e('Nombre proyectista') ?></th>

                <th class="col-md-2"><?php _e('Nombre cliente') ?></th>

                <th class="col-md-2"><?php _e('Localización') ?></th>

                <th class="col-md-2"><?php _e('Tipo de cámara') ?></th>

            </tr>

            </thead>

            <tbody>
            <?php
            foreach ($datos as $proyecto) {
               echo "<tr id='".$proyecto['id']."'>
                    <td><a href='#' class='editarProyect'><i class=\"fa fa-pencil\" aria-hidden=\"true\"></i></a><a href='#' class='borrarProyect' type=\"button\" class=\"btn\" href=\"#test_modal\" data-toggle=\"modal\"><i class=\"fa fa-trash-o\" aria-hidden=\"true\"></i></a>".$proyecto['nombre_proyecto']."</td>
                    <td>".$proyecto['fecha_proyecto']."</td>
                    <td>".$proyecto['nombre_proyectista']."</td><td>".$proyecto['nombre_cliente']."</td><td>".$proyecto['nombre']."</td><td>".$proyecto['nombreCamara']."</td>
                </tr>";
            } 
            ?>
            </tbody>

        </table>

    <?php } else { ?>
        <p><?php _e('No tiene ningún proyecto. Cree un proyecto nuevo.') ?></p>
    <?php } ?>
</div>

<div class="wizard col-sm-12" id="satellite-wizard" data-title="Crear Proyecto">
    <?php
	//Identificación del proyecto
	include_once('templates/screensWizard/identificacion_proyecto.php');

    //Emplazamiento
    include_once('templates/screensWizard/emplazamiento.php');

    //Tipo de cámara
    include_once('templates/screensWizard/tipo_camara.php');

    //Tipo de producto
    include_once('templates/screensWizard/tipo_producto.php');

    //Dimensiones de la cámara
    include_once('templates/screensWizard/dimensiones_camara.php');

    //Ubicacion y orientación
    include_once('templates/screensWizard/ubicacion_orientacion.php');

    //Cerramientos
    include_once('templates/screensWizard/cerramientos.php');

    //Ocupación
    include_once('templates/screensWizard/ocupacion.php');

    //Iluminación
    include_once('templates/screensWizard/iluminacion.php');

    //Evaporador
    include_once('templates/screensWizard/evaporador.php');

    //Otras cargas térmicas
    include_once('templates/screensWizard/otras_cargas_termicas.php');

    //Factores de mayoración
    include_once('templates/screensWizard/factores_de_mayoracion.php');

    //Parametros ocultos
    include_once('templates/screensWizard/parametros_ocultos.php');

    //Resultados
    include_once('templates/screensWizard/resultados.php');
	?>
    <div class="success">
        <?php _e('Guardado') ?>
    </div>
</div>

<?php include_once('templates/footer.php') ?>