<?php

require_once("app/core.php");
include('templates/header.php'); ?>

<section class="bg-image">

    <div class="container pb-lg">
        <div class="row justify-content-center no-gutters row-login">
            <form class="" id="login" name="login" method="post">
                <h4 class="fw-bold gray">Inicio de sesión</h4>
                <div class="alert alert-danger" role="alert" id="error" style="display: none;">...</div>
                <div class="form-group">

                    <input type="text" name="username" value="" class="form-control" placeholder="Dirección de correo electrónico">

                    <input type="password" name="password" value="" class="form-control" placeholder="Contraseña">

                    <div class="fila">
                        <label class="pull-left" for="remember">
                            <input class="form-check-input" type="checkbox" name="remember" id="remember" value="" checked>
                        Recuérdame
                        </label>

                        <div class="pull-right forgottenPass">
                            <a href="https://cursos.coolproyect.com/recuperar-contrasena">¿Olvidaste tu contraseña?</a>
                        </div>
                        <div class="clearfix"></div>
                    </div>

                    <button id="login_button" type="submit" class="btn btn-info submit">
                        ENTRAR
                    </button>
                </div>
            </form>

        </div>

    </div>

</section>

<?php include('templates/footer.php'); ?>
